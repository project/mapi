<?php

/**
 * @file
 * Class for reading SWF file information.
 */

/**
 * For getting the details from a SWF file.
 */
class mapi_video_swf {
  var $handle;      // the open file handle for the SWF file.
  var $header;      // the header for the SWF, read once.
  var $data;        // the last chunk of data read. in uncompressed form
  var $filesize;    // the actual filesize of the SWF

  /**
   * Open a SWF filename
   */
  function open($filename) {
    // close any previously opened file
    if ($this->handle) {
      fclose($this->handle);
    }

    // attempt to open handle
    if (!($this->handle = @fopen($filename, 'rb'))) {
      return false;
    }

    // get the filesize
    $this->filesize = filesize($filename);

    // Read SWF header block
    if (($block = @fread($this->handle, 8)) === FALSE) {
      return $this->close();
    }

    // process the header block
    $this->header = unpack('a3signature/Cversion/Vlength', $block);
    $this->data = '';

    // check the header that it's a proper SWF file.
    if ($this->header['signature'] != 'FWS' && $this->header['signature'] != 'CWS') {
      return $this->close();
    }

    return true;
  }

  /**
   * Close the SWF file handle
   */
  function close() {
    if ($this->handle) {
      fclose($this->handle);
    }
    $this->handle = NULL;
    $this->data = NULL;
    $this->filesize = NULL;
    $this->header = NULL;
    return false;
  }

  /**
   * Is a compressed SWF file?
   */
  function compressed() {
    return $this->header['signature'] == 'CWS';
  }

  /**
   * Gets the dimensions from the file.
   */
  function dimensions() {
    // returns nothing if no handle.
    if (!$this->handle) {
      return array();
    }

    // this means that no dimensions are going to be returned.
    if (!$this->get_contents()) {
      return false;
    }

    // unpack these next important bytes
    $block = unpack('a9frame/Crate_major/Crate_minor/vcount', $this->data);

    // get the frame details
    $frame = preg_split('//', $block['frame']);

    // how many bits per index
    $byte = ord($frame[1]);

    // bits per part
    $bpp = ($byte >> 3) & 0x1F;

    $values = array();

    // get the values as a series of bits
    $byte = ($byte << 5) & 0xFF;
    $value = 0;
    $left = 3;
    $index = 1;
    for ($i = 0; $i < 4; $i++) {
      $nbl = $bpp;
      while ($nbl > 0) {
        $bit = ($byte & 0x80) >> 7;
        $byte = ($byte << 1) & 0xFF;
        $left -= 1;

        if ($left <= 0) {
          $index = $index + 1;
          $byte = ord($frame[$index]);
          $left = 8;
        }
        $value = ($value << 1) | $bit;
        $nbl--;
      }
      $values[] = $value;
      $value = 0;
    }

    // automatically convert to pixels (not the twips for swf layouts).
    $dimensions['width'] = $values[1] / 20;
    $dimensions['height'] = $values[3] / 20;

    // calculate duration from frame rate, and frame count
    $dimensions['frame_rate'] = ($block['rate_major'] + ($block['rate_minor'] / 100));

    return $dimensions;
  }

  /**
   * Currently there is no support for metadata within SWF
   */
  function metadata($filename) {
    return array();
  }

  /**
   * Gets the contents of the SWF into memory.
   */
  function get_contents() {
    if ($this->compressed()) {
      // use built-in if possible
      if (function_exists('gzuncompress')) {
        $this->data = gzuncompress(fread($this->handle, $this->filesize - 8));
      }
      // we don't support the decompression yet
      else {
        return FALSE;
      }
    }
    // simple read of all the contents
    else {
      $this->data = fread($this->handle, $this->filesize - 8);
    }

    return $this->data;
  }

}
