<?php

/**
 * @file
 * Class for reading MP4 file information.
 */

/**
 * For getting the details from a MP4 file.
 */
class mapi_video_mp4 {
  var $handle;      // the open file handle for the MP4 file.
  var $atoms;       // the atom structure for the MP4 file
  var $width;
  var $height;
  var $duration;

  /**
   * Open an MP4 filename
   */
  function open($filename) {
    // close any previously opened file
    if ($this->handle) {
      fclose($this->handle);
    }

    // attempt to open handle
    if (!($this->handle = @fopen($filename, 'rb'))) {
      return false;
    }

    $this->width = $this->height = $this->duration = 0;

    return true;
  }

  /**
   * Close the SWF file handle
   */
  function close() {
    if ($this->handle) {
      fclose($this->handle);
    }
    $this->handle = null;
    $this->atoms = null;
    $this->width = $this->height = $this->duration = 0;
    return false;
  }

  /**
   * Gets the dimensions from the file.
   */
  function dimensions() {
    // returns nothing if no handle.
    if (!$this->handle || !$this->parse_file()) {
      return array();
    }

    return array('width' => $this->width, 'height' => $this->height, 'duration' => $this->duration);
  }

  /**
   * MP4 metadata
   */
  function metadata() {
    // returns nothing if no handle.
    if (!$this->handle || !$this->parse_file()) {
      return array();
    }

    // currently don't have the ability to do metadata from MP4.
    return array();
  }

  /**
   * Parse the file
   */
  function parse_file() {
    // check handle
    if (!$this->handle) {
      return false;
    }

    $count = 0;
    do {
      if (($block = @fread($this->handle, 8)) === FALSE) {
        return false;
      }
      // probably at the end of file
      if (!$block) {
        break;
      }
      $size = array_shift(unpack('N', substr($block, 0, 4)));
      $tag = substr($block, 4, 4);
      if ($size == 0 || $size == 1)  {
        // size == 1 requires 64-bit size.
        // skips to end of file
        break;
      }
      // adjust for size including the size and tag details
      $size -= 8;
      
      $func = trim('handle_'. $tag);
      // handle contents of the atom
      if (method_exists($this, $func)) {
        if (($content = @fread($this->handle, $size)) === FALSE) {
          return false;
        }
        if ($this->$func($content)) {
        	break;
        }
      }
      // otherwise skip the atom itself
      elseif ($size) {
        @fseek($this->handle, $size, SEEK_CUR);
      }
      
      // safety against infinite looping
      if ($count++ > 20) {
        break;
      }
    } while (!feof($this->handle));

    return true;
  }
  
  /**
   *
   */
  function parse_version(&$block, &$version, &$flags) {
    $version = array_shift(unpack('N', substr($block, 0, 4)));
    $flags = $version & 0xFFF;
    $version >>= 24;
    $block = substr($block, 4);
  }
   
  /**
   * Handle child blocks
   */
  function child_blocks($block) {
    do {
      $size = array_shift(unpack('N', substr($block, 0, 4)));
      $tag = substr($block, 4, 4);
      // handle contents of the atom
      if (method_exists($this, $func = trim('handle_'. $tag))) {
        $this->$func(substr($block, 8, $size - 8));
      }
      if (!$size) {
        break;
      }
      $block = substr($block, $size);
    } while ($block);
  }

  /**
   * Handles the FTYP atom
   */
  function handle_ftyp($block) {
    $ftyp = new stdClass;
    $ftyp->major = substr($block, 0, 4);
    $block = substr($block, 4);
    $ftyp->minor = substr($block, 0, 4);
    $ftyp->compatible = array();
    while ($block = substr($block, 4)) {
     $ftyp->compatible[] = substr($block, 0, 4);
    }
    $this->atoms['ftyp'] = $ftyp;
  }
  
  /**
   * Handle the MOOV atom
   */  
  function handle_moov($block) {
    $this->child_blocks($block);
    return TRUE;
  }
  
  /**
   * Handle the movie header information
   */
  function handle_mvhd($block) {
    $this->parse_version($block, $version, $flags);
    if (!$version) {
      $obj = new stdClass;
      $obj->create = array_shift(unpack('N', substr($block, 0, 4)));
      $obj->modify = array_shift(unpack('N', substr($block, 0, 4)));
      $obj->timescale = array_shift(unpack('N', substr($block, 8, 4)));
      $obj->duration = array_shift(unpack('N', substr($block, 12, 4)));
      $this->atoms['moov.mvhd'] = $obj;
      $this->duration = ($obj->duration / $obj->timescale);
    }
  }
  
  /**
   * Handle TRAK info.
   */
  function handle_trak($block) {
    $this->child_blocks($block);
  }
  
  /**
   * Handle TKHD track header box.
   */
  function handle_tkhd($block) {
    $this->parse_version($block, $version, $flag);
    if (!$version && ($flag & 0x1)) {
      $obj = new stdClass;
      $obj->create = array_shift(unpack('N', substr($block, 0, 4)));
      $obj->modify = array_shift(unpack('N', substr($block, 0, 4)));
      $obj->track_id = array_shift(unpack('N', substr($block, 8, 4)));
      $obj->duration = array_shift(unpack('N', substr($block, 16, 4)));
      $obj->width = array_shift(unpack('N', substr($block, 72, 4))) >> 16; // 16.16 floating point size
      $obj->height = array_shift(unpack('N', substr($block, 76, 4))) >> 16; // 16.16 floating point size
      $this->atoms['moov.mvhd.trak.tkhd'] = $obj;

      $this->width = $obj->width ? $obj->width : $this->width;
      $this->height = $obj->height ? $obj->height : $this->height;
    }
  }
    
  // TODO: handle the META box (8.44 or page 60 of the ISO file).
}
