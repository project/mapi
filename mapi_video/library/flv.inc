<?php

// need this for doing floating point numbers correctly.
define('BIG_ENDIAN', pack('n', 0x6273) == pack('I', 0x6273));

/**
 * For getting the details from a FLV file.
 */
class mapi_video_flv {
  var $handle;      // the open file handle for the SWF file.
  var $header;      // the header for the SWF, read once.
  var $data;        // the last chunk of data read. in uncompressed form
  var $filesize;    // the actual filesize of the SWF

  /**
   * Open a SWF filename
   */
  function open($filename) {
    // close any previously opened file
    if ($this->handle) {
      fclose($this->handle);
    }

    // attempt to open handle
    if (!($this->handle = @fopen($filename, 'rb'))) {
      return false;
    }

    // get the filesize
    $this->filesize = filesize($filename);

    // Read FLV header block
    if (($block = @fread($this->handle, 9)) === FALSE) {
      return $this->close();
    }

    // process the header block
    $this->header = unpack('a3magic/Cversion/Cflags/Noffset', $block);
    $this->header['audio'] = ($this->header['flags'] & 0x4 == 0x4);
    $this->header['video'] = ($this->header['flags'] & 0x1 == 0x1);
    $this->data = null;

    // can only handle FLV version 1 (offset == 9)
    if ($this->header['magic'] != 'FLV' || $this->header['version'] != 1 || $this->header['offset'] != 9) {
      return $this->close();
    }

    return true;
  }

  /**
   * Close the SWF file handle
   */
  function close() {
    if ($this->handle) {
      fclose($this->handle);
    }
    $this->handle = null;
    $this->data = null;
    $this->filesize = null;
    $this->header = null;
    return false;
  }

  /**
   * Is a compressed SWF file?
   */
  function compressed() {
    return $this->header['signature'] == 'CWS';
  }

  /**
   * Gets the dimensions from the file.
   */
  function dimensions() {
    // returns nothing if no handle.
    if (!$this->handle || !$this->parse_file()) {
      return array();
    }

    return $this->data['dimensions'];
  }

  /**
   * Currently there is no support for metadata within SWF
   */
  function metadata() {
    // returns nothing if no handle.
    if (!$this->handle || !$this->parse_file()) {
      return array();
    }

    return $this->data['metadata'];
  }

  /**
   * Parse the file
   */
  function parse_file() {
    // check handle
    if (!$this->handle) {
      return false;
    }

    // return the data already
    if ($this->data) {
      return $this->data;
    }

    // goto the end of the header
    fseek($this->handle, $this->header['offset'], SEEK_SET);

    // get a tag block
    $header = $this->header;
    $offset = $this->header['offset'];
    $length = 0;
    $metadata = null;
    while (!feof($this->handle)) {
      // read beginning tag block
      if (($block = @fread($this->handle, 15)) === FALSE) {
        return false;
      }
      // last tag
      if (strlen($block) == 4) {
        break;
      }

      // do check for actual size of block
      if (strlen($block) < 15) {
        return false;
      }

      $previous = array_shift(unpack('N', substr($block, 0, 4)));
      $type = array_shift(unpack('C', substr($block, 4, 1)));
      $datasize = unpack('C3', substr($block, 5, 3));
      $timestamp = unpack('C3', substr($block, 8, 3));
      $extra = array_shift(unpack('C', substr($block, 11, 1)));
      $stream = unpack('C', substr($block, 12, 3));

      $datasize = $datasize[3] | ($datasize[2] << 8) | ($datasize[1] << 16);
      $timestamp = $timestamp[3] | ($timestamp[2] << 8) | ($timestamp[1] << 16);

      $offset += 15 + $datasize;

      switch ($type) {
        // audio details
        case 8:
          if (!$audio) {
            $audio['codec']  = $extra & 0x7;
            $audio['rate']   = ($extra >> 4) & 0x3;
            $audio['size']   = ($extra >> 6) & 0x1;
            $audio['stereo'] = ($extra >> 7) & 0x1;
          }
          break;

        // video details
        case 9:
          if (!$video) {
            // read video header
            if (($block = @fread($this->handle, 12)) === FALSE) {
              return false;
            }
            $extra = array_shift(unpack('C', $block));
            $video['codec'] = $extra & 0x7;

            switch ($video['codec']) {
              case 2: // H263
                $picsize = (array_shift(unpack('n', substr($block, 4, 2))) >> 7) & 0x07;
                switch ($picsize) {
                  case 0: // custom 1 byte
                    $video['width']  = (array_shift(unpack('n', substr($block, 5, 2))) >> 7) & 0xFF;
                    $video['height'] = (array_shift(unpack('n', substr($block, 6, 2))) >> 7) & 0xFF;
                    break;
                  case 1: // custom 2 byte
                    $video['width']  = (array_shift(unpack('N', substr($block, 5, 4))) >> 15) & 0xFFFF;
                    $video['height'] = (array_shift(unpack('N', substr($block, 7, 4))) >> 15) & 0xFFFF;
                    break;
                  case 2: $video['width'] = 352; $video['height'] = 288; break; // CIF
                  case 3: $video['width'] = 176; $video['height'] = 144; break; // QCIF
                  case 4: $video['width'] = 128; $video['height'] = 96;  break; // SQCIF
                  case 5: $video['width'] = 320; $video['height'] = 240; break; // 320x240
                  case 6: $video['width'] = 160; $video['height'] = 120; break; // 160x120
                }
                break;
              case 3: // Screen
              case 6: // Screen V2
                $video['width']  = array_shift(unpack('n', substr($block, 1, 2))) & 0x7FF;
                $video['height'] = array_shift(unpack('n', substr($block, 3, 2))) & 0x7FF;
                break;
              case 4: // VP6
              case 5: // VP6-alpha
              case 7: // AVC
                // unsupported currently.
                break;
            }
          }
          break;

        // metadata details
        case 18:
          // skip first bytes (which is 2)
          if (($length = fread($this->handle, 1)) === FALSE) {
            return false;
          }
          $metadata = $this->amf_object();
          if ($metadata['key'] == 'onMetaData') {
            $metadata = $metadata['value'];
            if (isset($metadata['height'])) {
              $video['height'] = intval($metadata['height']);
            }
            if (isset($metadata['width'])) {
              $video['width'] = intval($metadata['width']);
            }
            if (isset($metadata['framerate'])) {
              $video['framerate'] = $metadata['framerate'];
            }
            if (isset($metadata['duration'])) {
              $length = $metadata['duration'] * 1000;
            }
          }
          // metadata that we don't seek.
          else {
            $metadata = null;
          }
          break;
      }

      // check if all the data has be grabbed where necessary (assume that duration is also correctly set)
      if ($video['height'] && $video['width'] && $metadata) {
        // handles when video data exists, but header incorrectly set.
        $header['video'] = true;
        break;
      }
      if ($timestamp > $length) {
        $length = $timestamp;
      }

      fseek($this->handle, $offset, SEEK_SET);
    }

    $this->data = array(
      'metadata' => $metadata ? $metadata : array(),
      'dimensions' => array('length' => $length / 1000
      )
    );
    if ($header['video']) {
      $this->data['metadata']['video'] = true;
      $this->data['dimensions']['height'] = $video['height'];
      $this->data['dimensions']['width'] = $video['width'];
    }
    if ($header['audio']) {
      $this->data['metadata']['audio'] = true;
    }

    return true;
  }

  /**
   * Handle an AMF object
   */
  function amf_object($no_key = false) {
    $string = '';
    if (!$no_key) {
      // read SCRIPTDATASTRING
      if (($length = fread($this->handle, 2)) === FALSE) {
        return false;
      }
      $length = array_shift(unpack('n', $length));

      // get the actual string data
      if ($length && ($string = fread($this->handle, $length)) === FALSE) {
        return false;
      }
      // trims the string
      $string = rtrim($string, "\0");
    }

    // read SCRIPTDATAVALUE
    if (($type = fread($this->handle, 1)) === FALSE) {
      return false;
    }
    $type = array_shift(unpack('C', $type));

    switch ($type) {
      // SCRIPTDATAOBJECTEND
      case 9:
        return TRUE;

      // number type;
      case 0:
        if (($double = fread($this->handle, 8)) === FALSE) {
          return false;
        }
        $value = array_shift(unpack('d', BIG_ENDIAN ? $double : strrev($double)));
        break;

      // boolean type;
      case 1:
        if (($bool = fread($this->handle, 1)) === FALSE) {
          return false;
        }
        $value = array_shift(unpack('C', $bool));
        break;

      // movieclip type
      case 4:
      // string type
      case 2:
        if (($length = fread($this->handle, 2)) === FALSE) {
          return false;
        }
        $length = array_shift(unpack('n', $length));

        // get the actual string data
        if (($value = fread($this->handle, $length)) === FALSE) {
          return false;
        }
        $value = rtrim($value, "\0");
        break;

      // reference type
      case 7:
        if (($length = fread($this->handle, 2)) === FALSE) {
          return false;
        }
        $value = array_shift(unpack('n', $length));
        break;

      // ECMA array type
      case 8:
        if (($count = fread($this->handle, 4)) === FALSE) {
          return false;
        }
        $count = array_shift(unpack('N', $count));

      // data object
      case 3:
        $list = array();
        while ($object = $this->amf_object()) {
          if ($object === true) {
            return array('key' => $string, 'value' => $list);
          }
          $list[$object['key']] = $object['value'];
        }
        return false;
        break;

      // strict array type
      case 10:
        if (($count = fread($this->handle, 4)) === FALSE) {
          return false;
        }
        $count = array_shift(unpack('N', $count));

        $value = array();
        for ($i = 0; $i < $count; $i++) {
          if (!($object = $this->amf_object(true))) {
            return false;
          }
          //
          if ($object === true && $type == 3) {
            return array('key' => $string, 'value' => $value);
          }
          $value[$i] = $object['value'];
        }
        break;

      // date type
      case 11:
        if (($double = fread($this->handle, 8)) === FALSE) {
          return false;
        }
        $datetime = array_shift(unpack('d', BIG_ENDIAN ? $double : strrev($double)));

        if (($double = fread($this->handle, 2)) === FALSE) {
          return false;
        }
        $local = array_shift(unpack('s', BIG_ENDIAN ? $double : strrev($double)));

        $value = array('datetime' => $datetime, 'localoffset' => $local);
        break;

      // long string type.
      case 12:
        if (($length = fread($this->handle, 4)) === FALSE) {
          return false;
        }
        $length = array_shift(unpack('N', $length));

        // get the actual string data
        if (($value = fread($this->handle, $length)) === FALSE) {
          return false;
        }
        break;
      case 5: // null type
      case 6: // undefined type
        break;
    }

    return array('key' => $string, 'value' => $value);
  }
}
