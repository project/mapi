<?php

/**
 * Administration Settings Form
 */
function mapi_admin_settings_form(&$form_state) {
  drupal_set_title(t('Media Settings'));

  $form = mapi_admin_invoke('settings');

  if (!count($form)) {
    $form[] = array('#value' => '<div>'. t('Currently, no settings are supplied for the media API administration.') .'</div>');
  }
  else {
    $form = system_settings_form($form);
  }

  return $form;
}

/**
 * Administration Extensions Form
 */
function mapi_admin_extensions_form(&$form_state, $ext = null) {
  $exts = mapi_extension_list();
  // specific to the extension which we're selecting
  if ($ext && in_array($ext, $exts)) {
    drupal_set_title(t('Media Extension %ext', array('%ext' => $ext)));

    // set breadcrumbs
    $breadcrumbs = drupal_get_breadcrumb();
    $breadcrumbs[] = l(t('Extensions'), 'admin/mapi/extensions');
    drupal_set_breadcrumb($breadcrumbs);

    // create presenter list
    $plist = array();
    $plist[false] = t('none');
    foreach (mapi_presenter_list_by_extension($ext) as $presenter) {
      $plist[$presenter] = $presenter;
    }

    $form['header'] = array('#value' => t('Please choose the default presenter for each extension type.') .'<p />');

    // select presenter for the extension
    $form['presenter'] = array('#tree' => true);
    $form['presenter']["{$ext}"] = array(
      '#type' => 'select',
      '#title' => $ext,
      '#options' => $plist,
      '#default_value' => mapi_presenter_default($ext),
    );

    $form['footer'] = array(
      '#prefix' => '<div>',
      '#value' => l(t('presenters'), 'admin/mapi/presenters') .' | '. l(t('extensions'), 'admin/mapi/extensions'),
      '#suffix' => '</div><p />',
    );

    $form['submit'] = array('#type' => 'submit', '#value' => t('Submit'));
  }
  else {
    drupal_set_title(t('Media Extensions'));

    $form['header'] = array('#value' => t('The list of extensions with default presenter.') .'<p />');

    $rows = array();
    // Theme the presenters in a way that allows it to handle multiple levels
    foreach ($exts as $ext) {
      $default = mapi_presenter_default($ext);
      $row = array(l($ext, 'admin/mapi/extensions/'. $ext), ($default ? $default : t('none')));
      $rows[] = $row;
    }
    if (!count($rows)) {
      $rows[] = array(array('data' => t('There are currently no extensions enabled.'), 'colspan' => 3));
    }

    $form['table'] = array('#value' => theme('table', array(t('Extensions'), t('Default Presenter')), $rows));
  }

  return $form;
}

/**
 * Submission of mapi_admin_extensions_form.
 */
function mapi_admin_extensions_form_submit($form_state, &$form_state) {
  $values = $form_state['values'];
  if ($values['op'] == t('Submit')) {
    $exts = $values['presenter'];

    $list = variable_get('mapi_presenter_defaults', array());
    foreach (array_keys($exts) as $ext) {
      $list[$ext] = $values['presenter'][$ext];
      $list[$ext] = $list[$ext] ? $list[$ext] : false;
    }

    variable_set('mapi_presenter_defaults', $list);
  }
}

/**
 * Administrative Form for MAPI types.
 */
function mapi_admin_types_form(&$form_state, $type = null) {
  // List all the types, and the extensions that exist, as well as the
  // presenter for each of the types. Provide links to all of them.
  if ($type && in_array($type, mapi_type_list())) {
    return mapi_admin_types_info_form($type);
  }

  drupal_set_title(t('Media Types'));

  $types = mapi_type_list();

  $form['header'] = array('#value' => t('These are the types which are associated with each of the different extensions.') .'<p />');

  $rows = array();
  foreach ($types as $type) {
    $details = mapi_type_info($type);
    $rows[] = array($details['name'], $details['title'], $details['description'], l(t('extensions'), 'admin/mapi/types/'. $type));
  }

  if (!count($rows)) {
    $rows[] = array(array('data' => t('There are currently no types enabled.'), 'colspan' => 4));
  }
  $form['table'] = array('#value' => theme('table', array(t('Name'), t('Title'), t('Description'), ''), $rows));

  return $form;
}

/**
 * Administration Types Information Form
 */
function mapi_admin_types_info_form($type) {
  drupal_set_title(t('Media type %type', array('%type' => $type)));

  // set breadcrumbs
  $breadcrumbs = drupal_get_breadcrumb();
  $breadcrumbs[] = l(t('Types'), 'admin/mapi/types');
  drupal_set_breadcrumb($breadcrumbs);

  $form['header'] = array('#value' => t('Settings for the media type %type.', array('%type' => $type)) .'<p />');

  $exts = array();
  $list = mapi_type_extensions($type);
  foreach ($list as $ext) {
    $exts[] = l($ext, 'admin/mapi/extensions/'. $ext);
  }

  $form['exts'] = array('#value' => theme('item_list', $exts, t('Extensions')));

  return $form;
}


/**
 * Administration Presenters Information Form
 */
function mapi_admin_presenters_form(&$form_state, $presenter = null) {
  if ($presenter && in_array($presenter, mapi_presenter_list())) {
    drupal_set_title(t('Media Presenter %presenter', array('%presenter' => $ext)));

    // set breadcrumbs
    $breadcrumbs = drupal_get_breadcrumb();
    $breadcrumbs[] = l(t('Presenters'), 'admin/mapi/presenters');
    drupal_set_breadcrumb($breadcrumbs);

    $form['presenter'] = array('#type' => 'value', '#value' => $presenter);
    $form['defaults'] = array(
      '#type' => 'fieldset',
      '#title' => t('Defaults'),
      '#description' => t('Select checkbox to make this presenter the default.'),
      '#tree' => true
    );
    $settings = variable_get('mapi_presenter_defaults_'. $presenter, array());
    foreach (mapi_extension_list_by_presenter($presenter) as $ext) {
      $form['defaults'][$ext] = array(
        '#type' => 'fieldset',
      );
      $form['defaults'][$ext]['default'] = array(
        '#type' => 'checkbox',
        '#title' => $ext,
        '#default_value' => mapi_presenter_default($ext) === $presenter,
      );
      $form['defaults'][$ext]['settings'] = mapi_presenter_configure($presenter, $settings[$ext], $ext);
      if (count($form['defaults'][$ext]['settings'])) {
        $form['defaults'][$ext]['settings']['#type'] = 'fieldset';
        $form['defaults'][$ext]['settings']['#title'] = t('Settings');
        $form['defaults'][$ext]['settings']['#collapsible'] = true;
        $form['defaults'][$ext]['settings']['#collapsed'] = true;
      }
    }

    $form['submit'] = array('#type' => 'submit', '#value' => t('Submit'));
  }
  else {
    drupal_set_title(t('Media Presenters'));
    $form['header'] = array('#value' => t('The listing of all presenters, and the extensions handled by them.'));

    $rows = array();

    $presenters = mapi_presenter_list();
    foreach ($presenters as $presenter) {
      $exts = mapi_extension_list_by_presenter($presenter);
      $count = count($exts);
      foreach ($exts as $key => $ext) {
        if ($key == 0) {
          $row = array(
            array('data' => l($presenter, 'admin/mapi/presenters/'. $presenter), 'rowspan' => $count),
            array('data' => l($ext, 'admin/mapi/extensions/'. $ext)),
            array('data' => (mapi_presenter_default($ext) === $presenter ? t('yes') : '')),
          );
        }
        else {
          $row = array(
            array('data' => l($ext, 'admin/mapi/extensions/'. $ext)),
            array('data' => (mapi_presenter_default($ext) === $presenter ? t('yes') : '')),
          );
        }
        $rows[]  = $row;
      }
    }

    if (!count($rows)) {
      $rows[] = array(array('data' => t('There are currently no presenters enabled.'), 'colspan' => 3));
    }
    $form['table'] = array('#value' => theme('table', array(t('Presenter'), t('Extension'), t('Default')), $rows));
  }

  return $form;
}

/**
 * Submission for mapi_admin_presenters_form.
 */
function mapi_admin_presenters_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  if ($values['op'] == t('Submit')) {
    // load the details for the specific presenter
    $list = variable_get('mapi_presenter_defaults', array());

    // process the settings
    $settings = array();
    foreach ($values['defaults'] as $ext => $value) {
      if ($value['default']) {
        unset($value['ext']);
        $settings[$ext] = $value['settings'];
        $list[$ext] = $values['presenter'];
      }
    }
    variable_set('mapi_presenter_defaults', $list);
    variable_set('mapi_presenter_defaults_'. $values['presenter'], $settings);
  }
}

/**
 * Administrative Form for MAPI Profiles.
 */
function mapi_admin_profiles_form(&$form_state, $profile = null) {
  // handle the profile details
  if ($profile && in_array($profile->name, mapi_profile_list())) {
    // set breadcrumbs
    $breadcrumbs = drupal_get_breadcrumb();
    $breadcrumbs[] = l(t('Profiles'), 'admin/mapi/profiles');
    drupal_set_breadcrumb($breadcrumbs);

    $form = mapi_admin_profile_edit_form($profile);
  }
  // handle the profile listing
  else {
    $rows = array();
    foreach (mapi_profile_list() as $pid => $profile) {
      $rows[] = array(l($profile, 'admin/mapi/profiles/'. $pid));
    }
    if (!count($rows)) {
      $rows[] = array(t('No profiles are currently defined.'));
    }
    $form[] = array('#value' => '<div>'. t('Profiles provide a consistant presentation of media files, regardless of extension.'). '</div>');
    $form[] = array('#value' => theme('table', array(), $rows));
  }

  return $form;
}

/**
 * Administration Form for Profile Addition.
 */
function mapi_admin_profiles_add_form() {
  $form['profile'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Name of the description, use only alphanumerics and hyphen.'),
  );

  $form['create'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 *  Form for adding profiles.
 */
function mapi_admin_profiles_add_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  if (preg_match('/[^\w\-]/', $values['profile'])) {
    form_set_error('profile', t('Name must only contain alphanumeric or hyphen characters.'));
  }
  if ($values['op'] == t('Submit')) {
    if (in_array($values['profile'], mapi_profile_list())) {
      form_set_error('profile', t('Profile with that name already exists.'));
    }
  }
}

/**
 *  Submission for mapi_admin_profiles_add_form
 */
function mapi_admin_profiles_add_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $form_state['redirect'] = 'admin/mapi/profiles';

  if ($values['op'] == t('Submit')) {
    if ($profile = mapi_profile_create($values['profile'], array())) {
      $form_state['redirect'] = 'admin/mapi/profiles/'. $profile->pid;
      $form_state['pid'] = $profile->pid;
    }
  }
}

/**
 * Administrative Form for Profile Editing.
 */
function mapi_admin_profiles_edit_form(&$form_state, $profile) {
  if (isset($form_state['storage']['profile'])) {
    $profile->settings = $form_state['storage']['profile'];
  }

  // set breadcrumbs
  $breadcrumbs[] = l(t('Home'), null);
  $breadcrumbs[] = l(t('Administer'), 'admin');
  $breadcrumbs[] = l(t('Media management'), 'admin/mapi');
  $breadcrumbs[] = l(t('Profiles'), 'admin/mapi/profiles');
  drupal_set_breadcrumb($breadcrumbs);

  $form['pid'] = array('#type' => 'value', '#value' => $profile->pid);

  $rows = array();
  // do settings per extension
  foreach (mapi_extension_list() as $ext) {
    $settings = isset($profile->settings[$ext]) ? (object)$profile->settings[$ext] : array();

    $rows[] = array(
      l($ext, 'admin/mapi/profiles/'. $profile->pid .'/'. $ext),
      $settings->presenter === '' ? t('none') : $settings->presenter,
      mapi_presenter_display_settings($settings->presenter, $settings->settings, $ext),
    );
  }
  $form['table'] = array('#value' => theme('table', array(t('Extension'), t('Settings')), $rows));

  $form['delete'] = array('#type' => 'submit', '#value' => t('Delete Profile'), '#weight' => 50);

  return $form;
}

/**
 * Validation of mapi_admin_profiles_edit_form.
 */
function mapi_admin_profiles_edit_form_validate($form, &$form_state) {
}

/**
 * Submission for mapi_admin_profiles_edit_form.
 */
function mapi_admin_profiles_edit_form_submit($form, &$form_state) {
  $values = $form_state['values'];

  // process the results
  switch ($values['op']) {
    case t('Delete Profile'):
      $form_state['redirect'] = 'admin/mapi/profiles/'. $values['pid'] .'/delete';
      break;
  }
}

/**
 * Administrative form for Profile Extension Overrides.
 */
function mapi_admin_profiles_ext_form(&$form_state, $profile, $ext) {
  if (!in_array($ext, mapi_extension_list())) {
    drupal_not_found();
    exit();
  }

  // set breadcrumbs
  $breadcrumbs[] = l(t('Home'), null);
  $breadcrumbs[] = l(t('Administer'), 'admin');
  $breadcrumbs[] = l(t('Media management'), 'admin/mapi');
  $breadcrumbs[] = l(t('Profiles'), 'admin/mapi/profiles');
  $breadcrumbs[] = l(_mapi_admin_profile_title($profile), 'admin/mapi/profiles/'. $profile->pid);
  drupal_set_breadcrumb($breadcrumbs);

  drupal_set_title(t('Profile Extension Settings %profile - %ext', array('%profile' => $profile->name, '%ext' => $ext)));

  $settings = isset($profile->settings[$ext]) ? (object)$profile->settings[$ext] : null;

  $form['pid'] = array('#type' => 'value', '#value' => $profile->pid);
  $form['ext'] = array('#type' => 'value', '#value' => $ext);

  $form['settings'] = array('#tree' => true);

  // define whether to override the defaults already set.
  $form['settings']['override'] = array(
    '#type' => 'checkbox',
    '#title' => t('Override'),
    '#default_value' => $settings,
    '#weight' => -20,
  );

  // allow selectable presenters for extension
  $form['settings']['presenter'] = array(
    '#type' => 'select',
    '#title' => t('Presenter'),
    '#options' => array_merge(array('' => t('none')), drupal_map_assoc(mapi_presenter_list_by_extension($ext))),
    '#default_value' => isset($settings->presenter) ? $settings->presenter : array_shift(mapi_presenter_list_by_extension($ext)),
    '#weight' => -18,
  );

  // presenter settings for the extension
  if ($settings->override && $settings->presenter) {
    $item = mapi_presenter_configure($settings->presenter, $settings->settings, $ext);
    // add the presenter form configure settings.
    if (is_array($item) && count($item)) {
      $item += array(
        '#type' => 'fieldset',
        '#title' => t('Presenter Settings'),
        '#collapsible' => true,
        '#collapsed' => false,
        '#weight' => -15,
      );
      $form['settings']['settings'] = $item;

    }
  }
  // provides a nothing option.
  if (empty($form['settings']['settings'])) {
    $form['settings']['settings'] = array('#type' => 'value', '#value' => array());
  }

  // any extra configuration options
  $extras = mapi_invoke_profiles('configure', $profile, $ext);
  if (is_array($extras) && count($extras)) {
    $form['settings'] = array_merge($extras, $form['settings']);
  }

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save Changes'));

  return $form;
}

/**
 * Validation for mapi_admin_profiles_ext_form.
 */
function mapi_admin_profiles_ext_form_validate($form, &$form_state) {
  $values = $form_state['values'];

  $profile = mapi_profile_load($values['pid']);

  // only check things if we have override.
  if ($values['settings']['override']) {
    $ext = $values['ext'];
    $details = $values['settings'];
    if (count(mapi_invoke_profiles('validate', $details, $ext))) {
      form_set_error('', t('Settings are invalid.'));
      return;
    }
    if ($details['presenter'] && !empty($details['settings']) && (empty($profile->settings[$ext]) || $profile->settings[$ext]['presenter'] == $details['presenter'])) {
      mapi_presenter_validate($value['settings']['presenter'], $values['settings']['settings'], $values['ext']);
    }
  }
}

/**
 * Submission for mapi_admin_profiles_ext_form.
 */
function mapi_admin_profiles_ext_form_submit($form, &$form_state) {
  $values = $form_state['values'];

  // process the results
  switch ($values['op']) {
    case t('Save Changes'):
      $profile = mapi_profile_load($values['pid']);
      $settings = $values['settings'];
      if ($settings['override']) {
        if ($values['presenter'] != $profile->presenter) {
          $settings['settings'] = $settings['presenter'] !== '' ? mapi_presenter_defaults($settings['presenter'], $values['ext']) : array();
        }
        $profile->settings[$values['ext']] = $settings;
      }
      else {
        unset($profile->settings[$values['ext']]);
      }

      // update the profile
      if (mapi_profile_update($profile)) {
        drupal_set_message(t('Profile Settings were saved.'));

        // save the settings for configurable inserts
        mapi_invoke_profiles('save', $profile, null);
      }
      break;
  }
}

/**
 * Administrative Form for Profile Deletion.
 */
function mapi_admin_profiles_delete_form(&$form_state, $profile) {
  $form['profile'] = array('#type' => 'value', '#value' => $profile->pid);

  // provide user interaction
  $notice = t('This action cannot be undone.');

  $form = confirm_form($form,
    t('Are you sure you want to delete profile %profile?', array('%profile' => $profile->name)),
    isset($_GET['destination']) ? $_GET['destination'] : 'admin/mapi/profiles/'. $profile->pid,
    $notice,
    t('Delete'), t('Cancel'));

  return $form;
}

/**
 *  Submission for delete form
 */
function mapi_admin_profiles_delete_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  if ($values['op'] == t('Delete')) {
    mapi_profile_delete($values['profile']);
  }

  $form_state['redirect'] = 'admin/mapi/profiles';
}

/**
 * Administration Form for Programs.
 */
function mapi_admin_programs_form(&$form_state, $program = null) {
  // accessing particular program settings
  if ($program && in_array($program, mapi_program_list())) {
    // set breadcrumbs
    $breadcrumbs = drupal_get_breadcrumb();
    $breadcrumbs[] = l(t('Programs'), 'admin/mapi/programs');
    drupal_set_breadcrumb($breadcrumbs);
    drupal_set_title($program);

    $details = mapi_program_load($program);

    $path = $details->path;

    $form['program'] = array('#type' => 'value', '#value' => $program);

    $form['header'] = array('#prefix' => '<div>', '#value' => t('Change the settings for the program.'), '#suffix' => '</div>');
    $form['path'] = array(
      '#type' => 'textfield',
      '#title' => t('Path'),
      '#description' => t('Please provide the full path to the program.'),
      '#default_value' => $path,
    );
    $form['settings'] = mapi_program_configure($program, unserialize($details->settings));
    $form['settings']['#tree'] = true;

    $form['submit'] = array('#type' => 'submit', '#value' => t('Submit'));
  }
  // otherwise, we list all the programs that exist
  else {
    $rows = array();
    foreach (mapi_program_list() as $name) {
      $rows[] = array(l($name, 'admin/mapi/programs/'. $name));
    }
    // no programs specified
    if (!count($rows)) {
      $rows[] = array(t('There are no programs enabled.'));
    }

    $form['header'] = array('#value' => t('Settings for programs which can be used by transforms, or other modules.'));
    $form['table'] = array('#value' => theme('table', array(t('Programs')), $rows));
  }

  return $form;
}

/**
 * Validation for mapi_admin_programs_form.
 */
function mapi_admin_programs_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  $program = $values['program'];
  if (!is_file($values['path'])) {
    form_set_error('path', t('There is no file located at the specified path.'));
    return;
  }
  mapi_program_validate($program, $values['settings']);
}

/**
 * Submission for mapi_admin_programs_form.
 */
function mapi_admin_programs_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $program = $values['program'];

  $settings = mapi_program_submit($program, $values['settings']);
  $settings = (isset($settings) ? $settings : $values['settings']);

  mapi_program_update($program, $values['path'], $values['settings']);
}

