INSTALLATION
============

swfobject.js
------------

* Download swfobject 1.5 from http://blog.deconcept.com/swfobject/swfobject.zip.
* Unzip swfobject.zip and copy the swfobject1-5/swfobject.js to misc/swfobject.js

jw_player
---------

* Download jw_flv_player 4.1 from http://www.jeroenwijering.com/?item=JW_FLV_Player.
* Unzip mediaplayer.zip and copy the player.swf into mapi_jwplayer/player.swf

1pixelout
---------

* Download Audio Player Wordpress plugin from http://www.1pixelout.net/download/audio-player.zip
* Unzip audio-player.zip and copy the audio-player/player.swf into mapi_onepixelout/player.swf

