<?php

/**
 * @file
 * MAPI Presenter handling functions
 *
 * A presenter is a method to present files to a user through a multiplicity
 * of ways. This could include a Flash/Silverlight plugin, or some standardized
 * way of presenting a file as HTML.
 *
 */

/**
 * Retrieve the list of available presenters.
 *
 * @return
 *   An array of the presenters made available through the hook mapi_presenters.
 */
function mapi_presenter_list() {
  return mapi_invoke_presenters('list', NULL, NULL, NULL, NULL, $null);
}

/**
 * Retrieve the list of available presenters for a given extension.
 *
 * @param $ext
 *   The extension.
 * @return
 *   An array of presenters that work with a given extension.
 */
function mapi_presenter_list_by_extension($ext) {
  static $cache = array();

  if (!array_key_exists($ext, $cache)) {
    $presenters = array();
    foreach (mapi_presenter_list() as $presenter) {
      $null = NULL;
      $exts = mapi_invoke_presenters('extensions', $presenter, $ext, NULL, NULL, $null);
      if (is_array($exts) && in_array($ext, $exts)) {
        $presenters[] = $presenter;
      }
    }
    $cache[$ext] = $presenters;
  }
  return $cache[$ext];
}

/**
 * Display a filename via a presenter.
 *
 * @param $presenter
 *   The presenter.
 * @param $filename
 *   The filename.
 * @param $settings
 *   The settings for display, based generally off the defaults, and possibly
 *   either configured or overriden in some way.
 * @param $details
 *   Non-modifiable options for the presenter
 * @param $options
 *   Modifiable options for the presenter.
 * @return
 *   The display HTML for a presented filename.
 */
function mapi_presenter_display($presenter, $filename, $settings, $details, &$options) {
  $output = mapi_invoke_presenters('display', $presenter, $filename, $settings, $details, $options);
  return is_string($output) ? $output : '';
}

/**
 * Display a series of filenames via a presenter.
 *
 * @param $presenter
 *   The presenter
 * @param $playlist
 *   The list of filenames to be presented.
 * @param $settings
 *   The settings for display, based generally off the defaults, and possibly
 *   either configured or overriden in some way.
 * @param $details
 *   Non-modifiable options for the presenter
 * @return
 *   The display HTML for a presented filename.
 */
function mapi_presenter_playlist($presenter, $playlist, $settings, $details) {
  $results = mapi_invoke_presenters('playlist', $presenter, $playlist, $settings, $details, $null);

  $html = '';
  if (!empty($results) && is_array($results)) {
    $html = $results['html'];
    // cache only things which need caching.
    if ($results['cache']) {
      cache_set($details['playlist_url'], $results['cache'], 'cache_page', CACHE_TEMPORARY, implode("\n\r", $results['headers']));
    }
  }

  return $html;
}

/**
 * Get the list of extensions for a given presenter.
 *
 * @param $presenter
 *   The presenter.
 * @return
 *   An array of extensions.
 */
function mapi_presenter_extension_list($presenter) {
  $exts = mapi_invoke_presenters('extensions', $presenter, NULL, NULL, NULL, $null);
  return is_array($exts) ? $exts : array();
}

/**
 * Retrieve the default settings for a presenter.
 *
 * @param $presenter
 *   The presenter.
 * @param $ext
 *   The extension for the presenter to handle.
 * @return
 *   An array of values.
 */
function mapi_presenter_defaults($presenter, $ext) {
  $defaults = mapi_invoke_presenters('defaults', $presenter, $ext, NULL, NULL, $null);
  return is_array($defaults) ? $defaults : array();
}

/**
 * Provides a display of the settings for the presenter.
 *
 * @param $presenter
 *   The presenter
 * @param $settings
 *   The settings for a presenter.
 * @return
 *   Returns the HTML for displaying the settings for the presenter.
 *   This is generally shorter than the entire configuration options.
 */
function mapi_presenter_display_settings($presenter, $settings) {
  $output = mapi_invoke_presenters('settings', $presenter, array(), $settings, NULL, $null);
  return is_string($output) ? $output : '';
}

/**
 * Retrieve the detail configuration form for a presenter.
 *
 * @param $presenter
 *   The presenter.
 * @param $edit
 *   The form edit values.
 * @param $ext
 *   The extension for which the presenter form applies to.
 * @return
 *   A form array.
 */
function mapi_presenter_configure($presenter, $edit = array(), $ext = '') {
  $form = mapi_invoke_presenters('configure', $presenter, $ext, $edit, NULL, $null);
  return is_array($form) ? $form : array();
}

/**
 * Validation the detail configuration form for a presenter.
 *
 * @param $presenter
 *   The presenter.
 * @param $values
 *   The values based on the configuration form.
 */
function mapi_presenter_validate($presenter, $values) {
  $errors = mapi_invoke_presenters('validate', $presenter, NULL, $values, NULL, $null);
  return is_array($errors) ? $errors : array();
}

/**
 * Submit the detail configuration form for a presenter.
 *
 * @param $presenter
 *   The presenter.
 * @param $values
 *   The values for a configuration form.
 * @return
 *   The modified values of the submission.
 */
function mapi_presenter_submit($presenter, $values) {
  $items = mapi_invoke_presenters('submit', $presenter, NULL, $values, NULL, $null);
  return is_array($items) ? array_merge($values, $items) : $values;
}

/**
 * Get default presenter for a given extension.
 *
 * @param $extension
 *   The file extension.
 * @return
 *   The name of the presenter that handles the extension by default.
 */
function mapi_presenter_default($extension) {
  $list = variable_get("mapi_presenter_defaults", array());

  // defaults to inbuilt generic presenter
  $default = FALSE;
  if (in_array($extension, mapi_extension_list())) {
    $presenters = mapi_presenter_list_by_extension($extension);

    // automatically select generic for all extensions which don't already have something defined.
    if (!array_key_exists($extension, $list)) {
      $list[$extension] = 'generic';
      variable_set('mapi_presenter_defaults', $list);
    }

    // now attempt to select the presenter
    if (array_key_exists($extension, $list) && in_array($list[$extension], $presenters)) {
      $default = $list[$extension];
    }
  }

  return $default;
}
