<?php

/**
 * @file
 * MAPI extension handling functions.
 *
 */

/**
 * Retrieve the list of available extensions.
 *
 * @return
 *   Returns an array of extensions defined by modules that hook mapi_extensions.
 */
function mapi_extension_list() {
  return mapi_invoke_extensions('list');
}

/**
 * Get the type of a given extension.
 *
 * @param $ext
 *   The file extension.
 * @return
 *   Returns the type defined by the module which hooked through mapi_types, to handle
 *   that the extension, or FALSE nothing currently handling it.
 */
function mapi_extension_type($ext) {
  // no extension means no type
  if ($ext) {
    // cycle through all the types and their extensions
    foreach (mapi_type_list() as $type) {
      if (in_array($ext, mapi_type_extensions($type))) {
        return $type;
      }
    }
  }

  return FALSE;
}

/**
 * Get the Mime type of a given extension.
 *
 * @param $ext
 *   The file extension.
 * @return
 *   Returns the mime type as defined through the hook mapi_extensions, or
 *   defaults to 'application/octet-stream'.
 */
function mapi_extension_mime($ext) {
  if ($ext) {
    if ($mime = mapi_invoke_extensions('mime', $ext)) {
      return $mime;
    }
  }
  return 'application/octet-stream';
}

/**
 * Determine extension from a mime type.
 *
 * @param $mime
 *   The mime type.
 * @return
 *   The appropriate extension, if defined through hook mapi_extensions, or
 *   a simple blank string on failure.
 */
function mapi_extension_by_mime($mime) {
  if ($mime) {
    // cycle through the extension list, looking for matching mime type
    foreach (mapi_extension_list() as $ext) {
      if (mapi_extension_mime($ext) == $mime) {
        return $ext;
      }
    }
  }

  return '';
}

/**
 * Retrieve a list of extensions a given transform can handle.
 *
 * @param $transform
 *   The transform name.
 * @return
 *   An array of extensions that the transform can handle, as defined through
 *   the module hook mapi_transforms.
 */
function mapi_extension_list_by_transform($transform) {
  $exts = mapi_invoke_transforms('extensions', $transform);
  return is_array($exts) ? array_intersect($exts, mapi_extension_list()) : array();
}

/**
 * Retrieve a list of extensions a given presenter can handle.
 *
 * @param $presenter
 *   The presenter name.
 * @return
 *   An array of extensions that a presenter can handle, as defined through
 *   the module hook mapi_presenters.
 */
function mapi_extension_list_by_presenter($presenter) {
  $exts = mapi_presenter_extension_list($presenter);
  return array_intersect($exts, mapi_extension_list());
}

/**
 * Get dimensions for a given filename.
 *
 * @param $filename
 *   A filename.
 * @return
 *   Returns dimensional properties of the filename, as returned by the
 *   handling routines of modules hooking mapi_extensions.
 */
function mapi_extension_dimensions($filename) {
  static $cache = array();
  if (!is_string($filename)) {
    return array();
  }
  if (!array_key_exists($filename, $cache)) {
    $dimensions = mapi_invoke_extensions('filename dimensions', $filename);
    if (empty($dimensions)) {
      $dimensions = mapi_invoke_extensions('dimensions', $filename);
    }
    $cache[$filename] = $dimensions;
  }
  return $cache[$filename];
}

/**
 * Get metadata for a given filename.
 *
 * @param $filename
 *   A filename.
 * @return
 *   Returns metadata information of a filename, as returned by the
 *   handling routines of modules hooking mapi_extensions.
 */
function mapi_extension_metadata($filename) {
  static $cache = array();
  if (!is_string($filename)) {
    return array();
  }
  if (!array_key_exists($filename, $cache)) {
    $metadata = mapi_invoke_extensions('filename metadata', $filename);
    if (empty($metadata)) {
      $metadata = mapi_invoke_extensions('metadata', $filename);
    }
    $cache[$filename] = $metadata;
  }
  return $cache[$filename];
}
