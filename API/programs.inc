<?php

/**
 * @file
 * MAPI Program handling functions
 *
 * A program is an external program used by the system to perform transforms
 * or allow other programs to perform an action, such as script.
 */

/**
 * Retrieve the list of available programs.
 *
 * @return
 *   An array of the available programs.
 */
function mapi_program_list() {
  return mapi_invoke_programs('list');
}

/**
 * Retrieve the configuration form of a program.
 *
 * @param $program
 *   The program name.
 * @param $edit
 *   The values entered into the configuration form.
 * @return
 *   A form array.
 */
function mapi_program_configure($program, $edit) {
  $form = mapi_invoke_programs('configure', $program, $edit);
  return is_array($form) ? $form : array();
}

/**
 * Validate the configuration form of a program.
 *
 * @param $program
 *   The program name.
 * @param $values
 *   The values from the configuration form.
 * @return
 *   An array of errors that occurred.
 */
function mapi_program_validate($program, $values) {
  $errors = mapi_invoke_programs('validate', $program, $values);
  return is_array($errors) ? $errors : array();
}

/**
 * Submit the configuration form of a program.
 *
 * @param $program
 *   The program name.
 * @param $values
 *   The values entered into the configuration form.
 * @return
 *   An array with all the modified configuration values.
 */
function mapi_program_submit($program, $values) {
  $items = mapi_invoke_programs('submit', $program, $values);
  return is_array($items) ? array_merge($values, $items) : $values;
}

/**
 * Retrieve a list of extensions the program can handle.
 *
 * @param $program
 *   The program name
 * @return
 *   A list of extensions that the program can handle.
 */
function mapi_program_extensions($program) {
  $exts = mapi_invoke_programs('extensions', $program);
  return is_array($exts) ? $exts : array();
}

/**
 * Retrieve a valid path for the program, as suggested by the hook_mapi_programs.
 *
 * @param $program
 *   The program name.
 * @return
 *   A path for the program on success, FALSE on failure.
 */
function mapi_program_path($program) {
  $paths = mapi_invoke_programs('path', $program);
  // possibility of searching paths for valid path
  if (is_array($paths)) {
    foreach ($paths as $path) {
      if (is_string($path) && is_file($path)) {
        return $path;
      }
    }
  }
  // otherwise a string could suffix for possible location
  elseif (is_string($paths) && is_file($paths)) {
    return $paths;
  }
  return FALSE;
}

/**
 * Execute the program with a commandline.
 *
 * @param $program
 *   The program name
 * @param $cmdline
 *   An array of options to be outputted onto the command line.
 * @param $options
 *   Options for how to handle various outputs.
 *   TODO: to include 'wait' to wait for a program to finish executing
 *         as well as 'outfile' to allow for recovery of program output.
 */
function mapi_program_execute($program, $cmdline, $options = array()) {
  // get options with defaults
  $wait = isset($options['wait']) ? $options['wait'] : TRUE;

  // check that program is available
  $programs = mapi_program_list();
  if (!in_array($program, $programs)) {
    return FALSE;
  }

  // load the details from the database, including possible suggestion of path
  $details = mapi_program_load($program);

  // file has to exist
  if (!is_file($details->path)) {
    return FALSE;
  }

  // TODO: Integrate settings into here. These will automatically always be executed, with cmdline

  // attempts to put the path in an executable state for the operating system.
  $path = escapeshellarg($details->path);

  // can pass a simple string cmdline
  if (is_string($cmdline)) {
    $cmdline = escapeshellcmd($cmdline);
  }
  // or a nicely created array
  elseif (is_array($cmdline)) {
    foreach ($cmdline as $key => $value) {
      // has to be string or number
      if (!is_string($value) && !is_numeric($value)) {
        return FALSE;
      }
      // only place it in string if there exist spaces in the option
      if (strpos($value, ' ') !== FALSE) {
        $value = '"'. str_replace('"', '\\"', $value) .'"';
      }
      $cmdline[$key] = $value;
    }
    $cmdline = implode(' ', $cmdline);
  }
  // otherwise totally ignore the whole process
  else {
    return FALSE;
  }

  // final creation of the cmdline
  $cmdline = $path . ($cmdline ? ' '. $cmdline : '');

  // this waits for the result to finish
  // and diverts the information into a NULL device.
  // TODO: Could allow for the file contents to be retrieved
  // by simply writing to a temporary file, getting those details,
  // and providing them for the process to use.

  $result = shell_exec($cmdline .' 2>&1');

  // return the result of the operation.
  return $result;
}

/**
 * Check to see if a program is installed.
 *
 * @param $program
 *   The program name.
 * @return
 *   TRUE on success, FALSE on failure.
 */
function mapi_program_installed($program) {
  // check that it is a program that exists
  $programs = mapi_program_list();
  if (!in_array($program, $programs)) {
    return FALSE;
  }

  // load database details, include possible of auto-seek path.
  $details = mapi_program_load($program);

  // must be a file in order to be executable.
  if (is_file($details->path)) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Retrieve the details for a given program.
 *
 * @param $program
 *   The program name.
 * @return
 *   An program object, with the fields 'name', 'path', 'settings'.
 */
function mapi_program_load($program) {
  if ($details = db_fetch_object(db_query("SELECT name, path, settings FROM {mapi_programs} WHERE name='%s'", $program))) {
    $details->settings = unserialize($details->settings);
  }

  // if nothing already set, attempt to locate
  if (!$details->path) {
    $path = mapi_program_path($program);
    // only change it when there is something to change to.
    if ($path) {
      $details->path = $path;
      mapi_program_update($program, $path, $details->settings);
    }
  }
  return $details;
}

/**
 * Update the details for a given program.
 *
 * @param $program
 *   The program name
 * @param $path
 *   The path location for the program.
 * @param $settings
 *   An array of settings for the program.
 */
function mapi_program_update($program, $path, $settings) {
  // update
  if (db_result(db_query("SELECT name FROM {mapi_programs} WHERE name='%s'", $program)) == $program) {
    db_query("UPDATE {mapi_programs} SET path='%s', settings='%s' WHERE name='%s'", $path, serialize($settings), $program);
  }
  // save settings
  else {
    db_query("INSERT {mapi_programs} (name, path, settings) VALUES ('%s', '%s', '%s')", $program, $path, serialize($settings));
  }
}
