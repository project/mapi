<?php

/**
 * @file
 * MAPI transform handling functions.
 *
 * A transform is a process that allows a file to be converted in some way
 * to different settings, extension, media type, or other results.
 */

/**
 * Retrieve the list of available transforms.
 *
 * @return
 *   An array of transform listed by name.
 */
function mapi_transform_list() {
  return mapi_invoke_transforms('list');
}

/**
 * Process a filename through a transformation.
 *
 * @param $transform
 *   The transform name.
 * @param $filename
 *   The filename.
 * @param $settings
 *   The settings for transformation
 * @return
 *   The new filename for the transformed file, or empty string on failure.
 */
function mapi_transform_process($transform, $filename, $settings) {
  $filename = mapi_invoke_transforms('process', $transform, $settings, $filename);
  return is_string($filename) ? $filename : '';
}

/**
 * Test an extension change through a transformation via settings.
 *
 * @param $transform
 *   The transform name.
 * @param $filename
 *   A fake filename to do the test transform on.
 * @param $settings
 *   The settings values for the transform
 * @return
 *   The new fake filename on success, or empty string on failure.
 */
function mapi_transform_test($transform, $filename, $settings) {
  $filename = mapi_invoke_transforms('test', $transform, $settings, $filename);
  return is_string($filename) ? $filename : '';
}

/**
 * Retrieve information about a given transform.
 *
 * @param $transform
 *   The transform name.
 * @return
 *   A string with the information, or blank string on failure.
 */
function mapi_transform_info($transform) {
  $info = mapi_invoke_transforms('info', $transform);
  return is_string($info) ? $info : '';
}

/**
 * Retrieve the defaults for a given transform.
 *
 * @param $transform
 *   The transform name.
 * @param $ext
 *   The extension.
 * @return
 *   The default values for a transform on the extension.
 */
function mapi_transform_defaults($transform, $ext) {
  $defaults = mapi_invoke_transforms('defaults', $transform, array(), $ext);
  return is_array($defaults) ? $defaults : array();
}

/**
 * Retrieve the form elements for configuring a transform.
 *
 * @param $transform
 *   The transform name.
 */
function mapi_transform_configure($transform, $edit) {
  $form = mapi_invoke_transforms('configure', $transform, $edit);
  return is_array($form) ? $form : array();
}

/**
 * Validate the form for configuration of a transform.
 *
 * @param $transform
 *   The transform name.
 * @param $values
 *   The values from the configuration form.
 * @return
 *   An array of errors that have occurred.
 */
function mapi_transform_validate($transform, $values) {
  $errors = mapi_invoke_transforms('validate', $transform, $values);
  return is_array($errors) ? $errors : array();
}

/**
 * Submit the form for configuration of a transform.
 *
 * @param $transform
 *   The transform name.
 * @param $values
 *   The values from the configuration form.
 * @return
 *   An array of the modified configuration values.
 */
function mapi_transform_submit($transform, $values) {
  $items = mapi_invoke_transforms('submit', $transform, $values);
  return is_array($items) ? array_merge($values, $items) : $values;
}

/**
 * Flush a transform.
 *
 * Allow a transform to be flushed, wiping out everything that depends on that transform.
 *
 * @param $transform
 *   The transform name.
 */
function mapi_transform_flush($transform) {
  mapi_invoke_transforms('flush', $transform);
}

/**
 * Retrieve a list of transforms which are valid for a given extension.
 *
 * @param $ext
 *   The extension.
 * @return
 *   An array of the transforms that can apply to the extension.
 */
function mapi_transform_list_by_extension($ext) {
  $transforms = array();
  foreach (mapi_transform_list() as $transform) {
    $exts = mapi_extension_list_by_transform($transform);
    if (in_array($ext, $exts)) {
      $transforms[] = $transform;
    }
  }
  return $transforms;
}

/**
 * Apply a transform to a given file.
 *
 * @param $filename
 *   The filename.
 * @param $transform
 *   The transform name.
 * @param $settings
 *   The settings for doing the transformation.
 * @return
 *   A different filename for a transformed file, or the original if unsuccessful.
 */
function mapi_transform($filename, $transform, $settings) {
  // transform must exist to do transform
  if (in_array($transform, mapi_transform_list())) {
    // ok so perform the transform on the filename
    $defaults = mapi_transform_defaults($transform, mapi_file_ext($filename));
    $settings = array_merge($defaults, $settings);
    $filename = mapi_transform_process($transform, $filename, $settings);
  }
  // always return the filename
  return $filename;
}
