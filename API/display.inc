<?php

/**
 * @file
 * MAPI functions to display a media file.
 */

/**
 * Display a filename with the given options.
 *
 * @param $filename
 *   The filename to be displayed.
 * @param $options
 *   Options available for modifying the presentation process. These include:
 *     'override'  - Turn on override of presenter and settings.
 *     'profile'   - Override the profile used for display purposes.
 *     'presenter' - The presenter to be used for override display.
 *     'settings'  - The settings to be used for override display.
 *     'metadata'  - The metadata to be used for presentation.
 * @return
 *   The HTML to be displayed.
 */
function mapi_display($filename, $options = array()) {
  // must be close to being a filename
  if (!$filename || !is_string($filename)) {
    return '';
  }

  // profile and layout
  $override = array_key_exists('override', $options) && $options['override'];
  $profile = array_key_exists('profile', $options) ? $options['profile'] : variable_get('mapi_profile_default', NULL);

  // handle via profile
  $details = mapi_profile_details($profile, $filename);

  // get presenter, settings and metadata for the display of an image.
  $presenter = isset($options['presenter']) ? $options['presenter'] : '';
  $settings = isset($options['settings']) ? $options['settings'] : array();
  $metadata = isset($options['metadata']) ? $options['metadata'] : array();

  // have at least a presenter listed
  if (!$override) {
    $presenter = $presenter && in_array($presenter, mapi_presenter_list()) ? $presenter : $details['presenter'];
    $settings = array_merge($details['settings'], $settings);
  }

  mapi_invoke_display('preprocess', $details, $filename, $settings, $metadata, $options);

  $content = '';
  if (mapi_file_exists($filename)) {
    $settings = array_merge(mapi_extension_dimensions($filename), $settings);
    $content = mapi_presenter_display($presenter, $filename, $settings, $details, $options);
  }

  // save content so that it can be modified or extended, as well
  $details['content'] = $content ? $content : '';
  $options['content'] = $details['content'];

  // add a download option for the file
  if (isset($options['download']) && $options['download']) {
    $options['content'] .= theme('mapi_download', $filename, $settings, $metadata, $options);
  }

  mapi_invoke_display('postprocess', $details, $filename, $settings, $metadata, $options);

  return $options['content'] ? $options['content'] : '';
}

/**
 * Displays a playlist of items, filename, caption, and associated image.

 * @param $filenames
 *   An array of filenames to be displayed.
 * @param $options
 *   Options available for modifying the presentation process. These include:
 *     'override'  - Turn on override of presenter and settings.
 *     'profile'   - Override the profile used for display purposes.
 *     'presenter' - The presenter to be used for override display.
 *     'settings'  - The settings to be used for override display.
 *     'metadata'  - The metadata to be used for presentation.
 * @return
 *   The HTML to be displayed.
 */
function mapi_playlist($filenames, $options = array()) {
  // check that it is an array
  if (!is_array($filenames)) {
    return '';
  }

  // profile
  $override = array_key_exists('override', $options) && $options['override'];
  $profile = array_key_exists('profile', $options) ? $options['profile'] : variable_get('mapi_profile_default', NULL);

  // get settings, check for total override
  $presenter = isset($options['presenter']) ? $options['presenter'] : '';
  $settings = isset($options['settings']) ? $options['settings'] : array();
  $metadata = isset($options['metadata']) ? $options['metadata'] : array();

  if ($override) {
    $presenter = $presenter && in_array($presenter, mapi_presenter_list()) ? $presenter : $details['presenter'];
  }

  $list = array();
  foreach ($filenames as $item) {
    if ($item['filename']) {
      $filename = $item['filename'];

      // get details for specific filename, noting that it is part of a playlist.
      $details = mapi_profile_details($profile, $filename);
      $details['playlist'] = TRUE;

      // always have the settings updated to meet profile standards.
      $settings  = array_merge($details['settings'], $settings);

      $fsettings = $item['settings'] ? $item['settings'] : array();
      $fmetadata = $item['metadata'] ? $item['metadata'] : array();
      if ($override) {
        $fsettings = array_merge($details['settings'], $settings);
        $fmetadata = array_merge($details['metadata'], $metadata);
      }
      // make sure that SOME presenter is available.
      if (!$presenter) {
        $presenter = $details['presenter'];
      }

      // preprocess the filename before display
      mapi_invoke_display('preprocess', $details, $filename, $fsettings, $fmetadata, $options);
      if (mapi_file_exists($filename)) {
        $list[] = array(
          'filename' => $filename,
          'settings' => $fsettings,
          'metadata' => $fmetadata,
          'details' => $details,
        );
      }
    }
  }

  $content = '';
  if (count($list)) {
    $details['cache_id'] = md5(serialize($list) . serialize($details));
    $details['playlist_url'] = 'mapi/playlist/'. $details['cache_id'];
    $content = mapi_presenter_playlist($presenter, $list, $settings, $details);
  }

  $details['content'] = $content ? $content : '';

  mapi_invoke_display('postprocess', $details, $filename, $settings, $metadata, $options);

  return $details['content'] ? $details['content'] : '';
}

/**
 * Outputs whatever the cached playlist, outside files of these are cached as pages (since they are effectively pages to be served).
 */
function _mapi_playlist_cached($identifier) {
  // load the playlist from the identifier from in the cache.
  // otherwise just simply exit.
  $details = cache_get('mapi/playlist/'. mapi_file_name($identifier), 'cache_page');

  if ($details) {
    header($details->headers);
    print $details->data;
  }
  exit();
}

/**
 * This creates the HTML for an XHTML-valid object.
 *
 * @param $url
 *   A full URL
 * @param $width
 *   The width to be displayed within the HTML.
 * @param $height
 *   The height to be displayed within the HTML.
 * @param $general
 *   An array containing 'param' and 'var' wich themselves are associative arrays,
 *   which provides the generally used parameters and variables for an object.
 * @param $microsoft
 *   Similar to general but providing the details to the Microsoft aware object.
 * @return
 *   HTML code representing the media object.
 */
function mapi_render_object($url, $width, $height, $general = array(), $microsoft = array()) {
  $output = '';

  // define the main ones
  $param = (!empty($general['param']) ? $general['param'] : array());
  $var = (!empty($general['var']) ? $general['var'] : array());

  $var['width'] = $width;
  $var['height'] = $height;

  // process microsoft problematic param and variables
  $ms_param = (!empty($microsoft['param']) ? $microsoft['param'] : array());
  $ms_var = (!empty($microsoft['var']) ? $microsoft['var'] : array());

  // add class for object-replacement for IE click-passed
  $id = 'player-'. md5($url) .'-'. ++$sobject;

   // begin the object Microsoft sees
  $output  = '<div class="object-replace">';
  $output .= '<!--[if IE]>';
  $output .= '<object width="'. $width .'" height="'. $height .'" '. drupal_attributes($ms_var) .'>';
  foreach ($ms_param as $k => $v) {
    $output .= '<param name="'. $k .'" value="'. $v .'" />';
  }
  $output .= '</object>';
  $output .= '<![endif]-->';

  // object for everybody else
  $output .= '<!--[if !IE]>-->';
  $output .= '<object data="'. $url .'" '. drupal_attributes($var) .'>';
  foreach ($param as $k => $v) {
    $output .= '<param name="'. $k .'" value="'. $v .'" />';
  }
  $output .= '</object>';
  $output .= '<!--<![endif]-->';
  $output .= '</object>';
  $output .= '</div>';

  return $output;
}

