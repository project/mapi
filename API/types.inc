<?php

/**
 * @file
 * MAPI Type handling functions.
 *
 * A type is a list of extensions that have the same styles of properties.
 * Audio, Video, Document files, for example.
 */

/**
 * Retrieve the list of avaiable types.
 *
 * @return
 *   An array of types the system can handle.
 */
function mapi_type_list() {
  return mapi_invoke_types('list');
}

/**
 * Retrieve the list of avaiable extensions for a given type.
 *
 * @param $type
 *   The type name.
 * @return
 *   An array of extensions that the type can handle.
 */
function mapi_type_extensions($type) {
  return mapi_invoke_types('extensions', $type);
}

/**
 * Retrieve information for a type.
 *
 * @param $type
 *   The type name
 * @return
 *   An array containing the information containing information
 *   expected by node_type_info().
 */
function mapi_type_info($type) {
  $info = mapi_invoke_types('info', $type);

  // do standard error checking for details
  $info                = is_array($info) ? $info : array();
  $info['name']        = $type;
  $info['title']       = isset($info['title']) ? $info['title'] : $type;
  $info['description'] = isset($info['description']) ? $info['description'] : '';

  return $info;
}

