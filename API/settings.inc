<?php

/**
 * @file
 * MAPI handling of persistant session information.
 */

/**
 * Retrieve the details of a persistant media variable.
 *
 * @param $key
 *   The reference key.
 * @return
 *   Returns FALSE on failure, value on success.
 */
function mapi_persist_get($key) {
  if (!isset($_SESSION['mapi'][$key])) {
    return FALSE;
  }

  return $_SESSION['mapi'][$key];
}

/**
 * Set the details of a persistant media variable.
 *
 * @param $key
 *   The reference key.
 * @param $value
 *   The value to be placed in the session variable.
 */
function mapi_persist_set($key, $value) {
  if ($key === NULL) {
    unset($_SESSION['mapi']);
  }
  // wipe variable completely from memory if NULL
  elseif ($value === NULL && isset($_SESSION['mapi'][$key])) {
    unset($_SESSION['mapi'][$key]);
  }
  // otherwise set it to the value
  else {
    $_SESSION['mapi'][$key] = $value;
  }
}
