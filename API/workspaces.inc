<?php

/**
 * @file
 * MAPI Workspace handling functions.
 */

/**
 * Retrieve the list of available workspaces.
 *
 * @param $workspace
 *   The workspace for which the paths are listed. Lists workspaces if path is NULL.
 * @return
 *   And array of paths, key => path, or an unindexed array of workspaces.
 */
function mapi_workspace_list($workspace = NULL) {
  return mapi_invoke_workspaces('list', $workspace);
}

/**
 * Provides the reference key path for a given workspace.
 * Always returns an allowed path, defaults to file_directory_temp() if
 * workspace or key does not exist.
 *
 * @param $workspace
 *   The workspace (module name) for which the path is being requested.
 * @param $path
 *   The key to which kind of path is required (i.e. public, private, or if the module
 *   defines the hook, paths that could represent users, or other relative paths).
 * @return
 *   Returns an always valid path. Defaults to file_directory_temp() if any parameter not
 *   met.
 */
function mapi_workspace_path($workspace, $path) {
  return mapi_invoke_workspaces('path', ($workspace ? $workspace : 'mapi'), $path);
}
