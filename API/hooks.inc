<?php

/**
 * @file
 * Functions for handling MAPI hooks.
 */

/**
 * MAPI Invoke Display Hook
 *
 * @param $op
 *   The operation to be performed for display. These are:
 *     'preprocess'  - This preprocesses a mapi_display request, allowing for
 *                     modification in any of the parameters.
 *     'postprocess' - This postprocesses a mapi_display request, allowing for
 *                     the presenter HTML to be then modified after the fact.
 * @param $details
 *   The non modifiable options for display.
 * @param &$filename
 *   The filename to be displayed.
 * @param &$settings
 *   Settings for the display.
 * @param &$metadata
 *   Metadata given for the display of a filename.
 * @param &$options
 *   The modifiable options for display.
 */
function mapi_invoke_display($op, $details, &$filename, &$settings, &$metadata, &$options) {
  // simply allow every module that implements this to do modifications.
  foreach (module_implements('mapi_display') as $module) {
    $func = $module .'_mapi_display';
    $func($op, $details, $filename, $settings, $metadata, $options);
  }
}

/**
 * MAPI Invoke Extensions Hook
 *
 * @param $op
 *   The operations that exist for extension handling. These are:
 *    'list'
 *       List the extensions the system can currently handle.
 *    'mime'
 *       Get the mime type for a filename extension.
 *    'filename dimensions'
 *       Allow for individual filenames to have different dimension
 *       information to be returned.
 *    'filename metadata'
 *       As above, except regarding metadata information.
 *    'dimensions'
 *       Get the standard dimension for a filename from the modules
 *       that handle the particular filename extension.
 *    'metadata'
 *       As above, except regarding metadata information.
 * @param $filename
 *   The filename as required by various of the above operations.
 * @return
 *   Dependent on the operation being performed.
 */
function mapi_invoke_extensions($op, $filename = NULL) {
  static $cache;
  static $modules;

  // cache the extensions
  if (!isset($cache)) {
    $cache = array();
    $modules = array();

    foreach (module_implements('mapi_extensions') as $module) {
      $func = $module .'_mapi_extensions';
      $list = $func('list', NULL);
      if (is_array($list)) {
        // store cached extensions and mime types
        $cache = array_merge($cache, $list);

        // store which module is handling the extension
        foreach (array_keys($list) as $ext) {
          $modules[$ext] = $module;
        }
      }
    }
    ksort($modules);
  }

  // perform operation
  switch ($op) {
    case 'list':
      return array_keys($modules);

    case 'mime':
      $ext = mapi_file_ext($filename);
      if (array_key_exists($ext, $cache)) {
        return $cache[$ext];
      }
      return 'application/octet-stream';

    // check to see if anything already stored for filename (used for special times when we
    // don't want to always call the dimensions section
    case 'filename dimensions':
    case 'filename metadata':
      $details = array();

      $ext = mapi_file_ext($filename);
      foreach (module_implements('mapi_extensions') as $module) {
        $func = $module .'_mapi_extensions';
        $list = $func($op, $filename);
        if (is_array($list)) {
          $details = array_merge($details, $list);
        }
      }
      if (is_array($details)) {
        return $details;
      }
      break;

    // has similar calling convention
    case 'dimensions':
    case 'metadata':
      $details = array();

      $ext = mapi_file_ext($filename);
      if (array_key_exists($ext, $modules)) {
        $func = $modules[$ext] .'_mapi_extensions';
        $details = $func($op, $filename);
      }
      if (is_array($details)) {
        return $details;
      }
      return array();
  }
}

/**
 * MAPI Invoke Types Hook
 *
 * @param $op
 *   Operations that exist for type handling. These are:
 *     'list'
 *        List types being handled by this system.
 *     'extensions'
 *        List the extensions a given type relates to.
 *     'info'
 *        Provide the information regarding the type which is
 *       used as an example for creating nodes of that type.
 * @param $type
 *   The type being reference. Modified by approach to
 *   return values.
 * @return
 *   Dependent on operation.
 */
function mapi_invoke_types($op, $type = NULL) {
  static $cache;

  // cache types
  if (!isset($cache)) {
    $cache = array();
    foreach (module_implements('mapi_types') as $module) {
      $func = $module .'_mapi_types';
      $list = $func('list', NULL);
      if (is_array($list)) {
        // store the type -> module
        foreach ($list as $t) {
          $cache[$t] = $module;
        }
      }
    }
  }

  // perform operation
  switch ($op) {
    case 'list':
      return array_keys($cache);

    case 'extensions':
    case 'info':
      $list = NULL;
      if (array_key_exists($type, $cache)) {
        $func = $cache[$type] .'_mapi_types';
        $list = $func($op, $type);
        if ($op == 'info') {
          if (is_array($list) && array_key_exists($type, $list)) {
            $list = $list[$type];
          }
          else {
            $list = NULL;
          }
        }
      }

      return is_array($list) ? $list : array();
  }
}

/**
 * MAPI Invoke Presenters Hook
 *
 * @param $op
 *   The operation to be performed for a presenter. These are:
 *     'list'
 *        List the available presenters for the system.
 *     'extensions'
 *        List the extensions a presenter can handle.
 *     'display'
 *        Get the HTML to be displayed for presenting a filename
 *        with all the given settings.
 *     'playlist'
 *        Get the HTML for presenting a list of filenames. Allows for
 *        cached data to be returned as a "playlist", i.e. an XML playlist
 *        for a Flash presenter.
 *     'defaults'
 *        Return the default settings for a given presenter, with a given
 *        filename.
 *     'settings'
 *        Return the HTML for the settings for a presenter. This is generally
 *        a shorter version of the configure form information.
 *     'configure'
 *        Return a form array for a given presenter.
 *     'validate'
 *        Validate a form array for a given presenter.
 *     'submit'
 *        Allow submission of a form array for a given presenter to be modified.
 *        An example is to allow changes in settings from strings to numbers.
 * @param $presenter
 *   The presenter being referenced by the operation
 * @param $filename
 *   The filename being presented, reliant on operation.
 * @param $settings
 *   The settings for a presenting a filename.
 * @param $details
 *   Non-modifiable options
 * @param &$options
 *   Modifiable options.
 */
function mapi_invoke_presenters($op, $presenter, $filename, $settings, $details, &$options) {
  static $cache;

  // cache presenters
  if (!isset($cache)) {
    $cache = array();
    foreach (module_implements('mapi_presenters') as $module) {
      $func = $module .'_mapi_presenters';
      $null = array();
      $list = $func('list', NULL, NULL, NULL, NULL, $null);
      if (is_array($list)) {
        // store the presenter -> module
        foreach ($list as $p) {
          $cache[$p] = $module;
        }
      }
    }
  }

  // perform operation
  switch ($op) {
    case 'list':
      return array_keys($cache);
    default:
      if (array_key_exists($presenter, $cache)) {
        $func = $cache[$presenter] .'_mapi_presenters';
        return $func($op, $presenter, $filename, $settings, $details, $options);
      }
      return;
  }
}

/**
 * MAPI Invoke Workspaces Hook
 *
 * @param $op
 *   One of the operations for managing workspaces. These are:
 *    'list' - List the paths a workspace can handle
 *    'path' - Get the path for a given workspace.
 * @param $workspace
 *   The workspace (module name) needing to be referenced.
 * @param $path
 *   The key for a path being handled by a workspace.
 * @return
 *   Dependent on the operation.
 */
function mapi_invoke_workspaces($op, $workspace = NULL, $path = NULL) {
  static $cache;

  // cache the workpaths
  if (!isset($cache)) {
    $cache = array();
    foreach (module_list() as $module) {
      $func = $module .'_mapi_workspaces';
      if (function_exists($func)) {
        $list = $func();
        if (is_array($list)) {
          // check that each path is a local directory
          foreach ($list as $key => $item) {
            if (!is_dir($item)) {
              unset($list[$key]);
            }
          }
          // add the base file and temporary directories, since everything should have access to these anyway
          $list[] = file_directory_path();
          $list[] = file_directory_temp();
          $cache[$module] = $list;
        }
      }
    }
  }

  // perform operation
  switch ($op) {
    case 'list':
      if (!is_null($workspace)) {
        if (array_key_exists($workspace, $cache)) {
          return $cache[$workspace];
        }
      }
      return array_keys($cache);

    case 'path':
      $list = NULL;
      if (array_key_exists($workspace, $cache)) {
        // returns the names of the paths available for the module
        if (is_null($path)) {
          return array_keys($cache[$workspace]);
        }
        elseif (array_key_exists($path, $cache[$workspace])) {
          return $cache[$workspace][$path];
        }
      }

      return file_directory_temp();
  }
}

/**
 * MAPI Invoke Programs Hook
 *
 * @param $op
 *   One of the following operations used for programs:
 *    'list'
 *      List the programs that can be handled by this system.
 *    'path'
 *      Returns an array of paths path to be auto-searched for the program.
 *    'extensions'
 *      Return the extensions currently handled by the program.
 *    'configure'
 *      A configure form for settings to be passed to the program.
 *    'validate'
 *      Validate the form given by the 'configure' operation.
 *    'submit'
 *      Allow program to handle changes in configuration.
 * @param $program
 *   The program to be referenced.
 * @param $data
 *   Data for a given operation.
 * @return
 *   Dependent on the operation.
 */
function mapi_invoke_programs($op, $program = NULL, $data = NULL) {
  static $cache;

  // cache the extensions
  if (!isset($cache)) {
    $cache = array();
    foreach (module_implements('mapi_programs') as $module) {
      $list = module_invoke($module, 'mapi_programs', 'list', NULL, NULL);
      if (is_array($list)) {
        foreach ($list as $program) {
          $cache[$program] = $module;
        }
      }
    }
  }

  // perform operation
  switch ($op) {
    case 'list':
      return array_keys($cache);
    default:
      if (array_key_exists($program, $cache)) {
        $func = $cache[$program] .'_mapi_programs';
        return $func($op, $program, $data);
      }
      break;
  }
}

/**
 * MAPI Invoke Transforms Hook
 *
 * @param $op
 *   One of the operations that can be performed on transforms. These are:
 *     'list'       - List the transforms available on the system.
 *     'extensions' - List the extensions a given transform can be applied to.
 *     'process'    - Processes a transform on a filename, returning the new file name
 *                    or FALSE on error.
 *     'test'       - Tests for changes in extension given the settings to be applied.
 *     'info'       - Retrive human readable information regarding the transform.
 *     'defaults'   - Get the default settings for a transform.
 *     'configure'  - Get the configuration form for a transform
 *     'validate'   - Validate the configuration form
 *     'submit'     - Allow modifications to take place after validation.
 *     'flush'      - Notify modules that filename has been asked to be flushed from the
 *                    system.
 * @param $transform
 *   Name of the transform.
 * @param $data
 *   The data required for the transform operation.
 * @param $filename
 *   The filename on which a transform may take place.
 * @return
 *   Dependent on the operation.
 */
function mapi_invoke_transforms($op, $transform = NULL, $data = NULL, $filename = NULL) {
  static $cache;
  static $modules;

  // cache the transform details
  if (!isset($cache)) {
    $cache = array();
    $modules = array();
    foreach (module_implements('mapi_transforms') as $module) {
      $func = $module .'_mapi_transforms';
      $list = $func('list', NULL, NULL, NULL, NULL);
      if (is_array($list)) {
        // store cached transforms and extensions they handle
        $cache = array_merge($cache, $list);

        // store which module is handling the extension
        foreach ($list as $t => $exts) {
          foreach ($exts as $ext) {
            $modules[$ext][$t] = $module;
          }
        }
      }
    }
  }

  // perform operation
  switch ($op) {
    // list all of the transforms available on this system.
    case 'list':
      return array_keys($cache);

    // list the transforms that apply for an extension
    case 'transforms':
      if (array_key_exists($filename, $modules)) {
        return array_keys($modules[$filename]);
      }
      return array();

    // list the extensions used by the transform?
    case 'extensions':
      if (array_key_exists($transform, $cache)) {
        return $cache[$transform];
      }
      return array();

    // execution and testing of the transform on a filename
    case 'process':
    case 'test':
      $item = NULL;
      $ext = mapi_file_ext($filename);
      if (array_key_exists($ext, $modules) && array_key_exists($transform, $modules[$ext])) {
        $func = $modules[$ext][$transform] .'_mapi_transforms';
        $item = $func($op, $transform, $data, $filename);
      }
      return $item;
    default:
      if (array_key_exists($transform, $cache)) {
        // get all modules which match the extension defined for transform
        $list = $cache[$transform];

        // since all of these don't require a particular extension just get the top one
        $ext = array_shift($list);
        $module = $modules[$ext][$transform];
        $func = $module .'_mapi_transforms';
        return $func($op, $transform, $data, $filename);
      }
      break;
  }
}

/**
 * MAPI Invoke Profiles Hook
 *
 * @param $op
 *   One of the operations to be performed on a profile. These are:
 *     'configure' - Retrieve a configuration form for the profile.
 *     'validate'  - Validates the added form items for the profile.
 *     'save'      - Allow additional profile data to be saved.
 *     'load'      - Allow additional profile data to be loaded.
 *     'create'    - Notify of profile creation.
 *     'delete'    - Notify of profile deletion.
 *     'update'    - Notify of profile update.
 * @param &$profile
 *   The profile to be modified.
 * @param $data
 *   Data required to perform the operation.
 * @return
 *   Dependent on the operation.
 */
function mapi_invoke_profiles($op, &$profile, $data = NULL) {
  $items = array();
  foreach (module_implements('mapi_profiles') as $module) {
    $func = $module .'_mapi_profiles';
    $list = $func($op, $profile, $data);
    if (is_array($list)) {
      $items = array_merge($items, $list);
    }
    elseif ($list) {
      $items[] = $list;
    }
  }
  return $items;
}

