<?php

/**
 * @file
 * MAPI Profile handling functions.
 *
 * A profile represents a common medium of presentation for any given file extension.
 */

/**
 * Retrieve the list of available profiles.
 *
 * @return
 *   An array of available profiles, with the profile ID as the key.
 */
function mapi_profile_list() {
  static $cache;

  if (!isset($cache)) {
    $cache = array();
    $results = db_query("SELECT pid, name FROM {mapi_profiles}");
    while ($object = db_fetch_object($results)) {
      $cache[$object->pid] = $object->name;
    }
  }

  return $cache;
}

/**
 * Load a profile.
 *
 * @param $pid
 *   The profile ID.
 * @param $refresh
 *   Boolean to determine whether to refresh the cache for this $pid.
 * @return
 *   A profile object, including addition profile data.
 */
function mapi_profile_load($pid, $refresh = FALSE) {
  static $cache = array();

  // basic checking of the pid
  if (!is_numeric($pid)) {
    return FALSE;
  }

  // send cached version
  if (!$refresh && array_key_exists($pid, $cache)) {
    return $cache[$pid];
  }

  // load from database
  $object = db_fetch_object(db_query("SELECT * FROM {mapi_profiles} WHERE pid = %d", $pid));

  // only send through the load function if an object
  if ($object) {
    $object->settings = unserialize($object->settings);
    mapi_invoke_profiles('load', $object);
  }

  // save in cache
  $cache[$pid] = $object;

  return $object;
}

/**
 * Create a profile.
 *
 * @param $name
 *   A name for the profile. Checked for whether it contains only alphanumerics and hyphen.
 * @param $settings
 *   The settings for the profile.
 * @return
 *   A profile object on success, or FALSE on failure.
 */
function mapi_profile_create($name, $settings) {
  // check if valid name
  if (!preg_match('/^[\w\d-]{1,255}$/', $name)) {
    return FALSE;
  }

  // check if settings correctly layed out
  if (!_mapi_profile_validate_settings($settings)) {
    $values = array('name' => $name, 'settings' => $settings);

    // create profile
    if (drupal_write_record('mapi_profiles', $values)) {
      $profile = (object)$values;

      // notify that a profile was created
      mapi_invoke_profiles('create', $profile);

      return mapi_profile_load($profile->pid);
    }
  }
  return FALSE;
}

/**
 * Delete a profile.
 *
 * @param $pid
 *   The profile ID.
 */
function mapi_profile_delete($pid) {
  // only valid profiles can be deleted
  if (!($profile = mapi_profile_load($pid))) {
    return;
  }

  // delete from database
  db_query("DELETE FROM {mapi_profiles} WHERE pid = %d", $profile->pid);

  // notify of changes taking place
  mapi_invoke_profiles('delete', $profile);

  // clear cache
  mapi_profile_load($profile->pid, TRUE);
}

/**
 * Update a profile.
 *
 * @param $profile
 *   A profile object in full.
 * @return
 *   The updated profile on success, or FALSE on failure.
 */
function mapi_profile_update($profile) {
  // must be replacing a previous profile
  if (!($old_profile = mapi_profile_load($profile->pid))) {
    return FALSE;
  }

  // check if valid name, if change occurred
  if ($old_profile->name != $profile->name) {
    if (!preg_match('/^[\w\d-]{1,255}$/', $profile->name)) {
      return FALSE;
    }
  }

  // check if settings correctly layed out
  if (!_mapi_profile_validate_settings($profile->settings)) {
    $values = array(
      'pid' => $profile->pid,
      'name' => $profile->name,
      'settings' => $profile->settings,
    );

    // create profile
    if (drupal_write_record('mapi_profiles', $values, array('pid'))) {
      $profile = (object)$values;

      // notify that a profile was created
      mapi_invoke_profiles('update', $profile);

      // clear cache and return the newly loaded profile
      return mapi_profile_load($profile->pid, TRUE);
    }
  }
  return FALSE;
}

/**
 * Validate profile settings.
 *
 * Very strict checker of profile settings.
 *
 * @param $settings
 *   Settings for the profile.
 * @return
 *   A string is the extension it failed on, or FALSE when correct.
 */
function _mapi_profile_validate_settings($settings) {
  if (is_array($settings)) {
    foreach ($settings as $ext => $item) {
      // check that the presenter exists
      if (!isset($item['presenter']) && !is_string($item['presenter'])) {
        return $ext;
      }
      // check that the settings exists
      if (!isset($item['settings']) && !is_array($item['settings'])) {
        return $ext;
      }

      // do validate for individual extension settings for profile.
      if (count(mapi_invoke_profiles('validate', $item, $ext))) {
        return $ext;
      }
    }
    // without some more serious checking, this function passes ok.
    return FALSE;
  }

  return FALSE;
}

/**
 * Retrieve the details of a given profile for a given filename.
 *
 * @param $profile
 *   The profile as a string, or full profile object, can also be
 *   NULL.
 * @return
 *   An array with all the necessary details need for display.
 */
function mapi_profile_details($profile, $filename) {
  // so that a simply profile name can be passed
  if (is_string($profile)) {
    $profile = mapi_profile_load(array_search($profile, mapi_profile_list()));
  }

  // figure out which part of the profile settings apply
  $object = NULL;
  $ext = mapi_file_ext($filename);

  if (!empty($profile->settings)) {
    if (array_key_exists($ext, $profile->settings)) {
      $object = (object)$profile->settings[$ext];
    }
  }

  // look up default presenter for the filename (defaulting to generic)
  $list = variable_get("mapi_presenter_defaults", array());
  $default = array_key_exists($ext, $list) && in_array($list[$ext], mapi_presenter_list()) ? $list[$ext] : 'generic';
  $default_settings = variable_get('mapi_presenter_defaults_'. $default, array());
  $default_settings = array_key_exists($ext, $default_settings) ? array_filter($default_settings[$ext]) : array();

  $fname = !empty($profile->filename) ? $profile->filename : $filename;
  $settings = !empty($object->settings) ? $object->settings : $default_settings;
  $presenter = !empty($object->presenter) && in_array($object->presenter, mapi_presenter_list()) ? $object->presenter : $default;

  // allow extra things from the saved profile settings to permeate through.
  $details = (array)$object;
  $details += array(
    'filename' => $fname,
    'presenter' => $presenter,
    'settings' => $settings,
  );

  return $details;
}
