<?php

/**
 * @file
 *
 * File and directory handling functions, including outside the drupal "safety" zone.
 *
 * These are crucial for providing access as well as checking directory existance.
 * Some of these are customized functions of the drupal equivalents, given that some
 * of the file handling is outside the drupal install directory.
 */

/**
 * Checks if an URI passed a local file.
 *
 * @param $uri
 *   The URI.
 * @return
 *   TRUE if file does not contain URI schema reference, or FALSE if does not.
 */
function mapi_file_is_local($uri) {
  return strpos($uri, '://') === FALSE;
}

/**
 * Create a URL for a file based on settings.
 * Always returns an absolute URL.
 *
 * @param $path
 *   Either a relative path, or an external one.
 * @return
 *   A full absolute url for the given file path.
 */
function mapi_url($path) {
  // already an external path
  if (!mapi_file_is_local($path)) {
    return $path;
  }

  if (is_file($path)) {
    global $base_url;

    return $base_url .'/'. ltrim($path, '/');
  }
  return url($path, array('absolute' => TRUE));
}

/**
 * Get a simple URI, without the addition query parameters or fragments.
 *
 * @param $uri
 *   The URI.
 * @return
 *   The resulting URI.
 */
function mapi_file_uri($uri) {
  // strip off any query
  $hash = strpos($uri, '#');
  $query = strpos($uri, '?');
  if ($hash !== FALSE || $query !== FALSE) {
    // if hash comes first
    if ($hash !== FALSE && $hash < $query) {
      $length = $hash;
    }
    // otherwise query comes first
    else {
      $length = $query;
    }
    $uri = substr($uri, 0, $length);
  }

  return $uri;
}

/**
 * Check to see if a URI exists.
 *
 * @param $uri
 *   The URI.
 * @param $refresh
 *   Refreshs the offsite URI cache.
 * @return
 *   TRUE if file, or existing URI.
 */
function mapi_file_exists($uri, $refresh = FALSE) {
  // automatic disqualification for non-existant or invalid entities
  if (!$uri || !is_string($uri)) {
    return FALSE;
  }

  // check local file
  if (mapi_file_is_local($uri)) {
    return is_file($uri);
  }
  // check foreign file
  else {
    return _mapi_file_external($uri) !== FALSE;
  }
}

/**
 * Retrieve the basename for a filename, without query or anchors of a URI.
 *
 * @param $filename
 *   The filename.
 * @return
 *   The basename.
 */
function mapi_file_basename($filename) {
  $filename = mapi_file_uri($filename);
  // make sure we deal with potential Windows file systems
  $filename = str_replace('\\', '/', $filename);
  // get the exploded list of parts
  $parts = array_filter(explode('/', $filename));
  return array_pop($parts);
}

/**
 * Get the base file name without extension or path.
 * This includes external URLs.
 *
 * @param $filename
 *   The filename.
 * @return
 *   Returns the filename minus the extension.
 */
function mapi_file_name($filename) {
  $fname = mapi_file_basename($filename);

  if (!$fname || !($pos = strrpos($fname, '.'))) {
    return $filename;
  }
  return substr($fname, 0, $pos);
}

/**
 * Get the extension of a filename.
 *
 * @param $filename
 *   The filename.
 * @return
 *   The extension
 */
function mapi_file_ext($filename) {
  $fname = mapi_file_basename($filename);

  if (!$fname || !($pos = strrpos($fname, '.'))) {
    return '';
  }
  return strtolower(substr($fname, $pos + 1));
}

/**
 * Get the path of a given filename.
 *
 * @param $filename
 *   The filename.
 * @return
 *   The extension
 */
function mapi_file_path($filename) {
  $pos = strpos($filename, '://');
  $schema = substr($filename, 0, $pos);
  if ($pos) {
    $filename = substr($filename, $pos + 3);
  }
  $filename = mapi_file_uri($filename);
  // make sure we deal with potential Windows file systems
  $filename = str_replace('\\', '/', $filename);
  // get the exploded list of parts
  $parts = array_filter(explode('/', $filename));
  array_pop($parts);
  return ($schema ? $schema .'://' : '') . implode('/', $parts) .'/';
}

/**
 *  The following provides manipulation of the file system.
 */

/**
 * Check for the existance of the directory, within a given workspace.
 *
 * @param $directory
 *   The directory to be checked
 * @param $options
 *   Contains the options for workspace. The definition for a workspace option is
 *   array('workspace' => 'mapi').
 * @return
 *   Returns FALSE on failure, TRUE on success
 */
function mapi_directory_check_location($directory, $options = array()) {
  $workspace = _mapi_file_workspace($options);
  $directory = realpath($directory);
  $paths = mapi_workspace_list($workspace);
  foreach ($paths as $type => $path) {
    if (strpos($directory, realpath($path)) !== 0) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Checks for the existance of the directory, within a given workspace.
 *
 * @param $directory
 *   The directory to be checked
 * @param $options
 *   Contains an array of options. Valid ones are:
 *     'workspace' - defines the workspace paths to check for validity
 *     'create'    - TRUE to create the directory, FALSE if only to check.
 *     'recursive' - TRUE to recurse through the directory path. Useful for creating a
 *                   non-existant path that is 2-3 levels deep.
 * @return
 *   Returns FALSE on failure, otherwise the real path name.
 */
function mapi_directory_check($directory, $options = array()) {
  // get options
  $create    = isset($options['create']) && $options['create'];
  $recursive = isset($options['recursive']) && $options['recursive'];
  $workspace = _mapi_file_workspace($options);

  // setup the path, normalized to unix pathway.
  $path = str_replace('\\', '/', $directory);

  // check if located in valid directories
  if ($path && !mapi_directory_check_location($path, $workspace)) {
    return FALSE;
  }

  // check and create if necessary
  if (!is_dir($path) && $create) {
    if ($recursive) {
      // get all of the parts of the path
      $path_parts = explode('/', $path);

      // remove Windows drive specifier for those directorys which are realpath()
      if (count($path_parts) && preg_match('/^[\w]:$/', $path_parts[0])) {
        array_shift($part_parts);
      }

      // now attempt to construct from bottom up.
      while (count($path_parts)) {
        if (!is_dir($new_path)) {
          if (!@mkdir($new_path)) {
            return FALSE;
          }
          @chmod($path, 0775);
        }
        $new_path .= '/'. array_shift($path_parts);
      }
    }

    // make directory
    if (!@mkdir($path)) {
      return FALSE;
    }

    // make directory useable, if possible
    @chmod($path, 0775);
  }

  // check for directory existance
  if (!is_dir($path)) {
    return FALSE;
  }

  return $path;
}

/**
 * Move a directory to the appropriate location, within a given workspace.
 *
 * @param $source
 *   The source directory.
 * @param $dest
 *   The destination directory.
 * @param $options
 *   An array of options. Valid values are:
 *     'workspace' - The workspace in which this function works.
 * @return
 *   Returns FALSE on failure, TRUE on success.
 */
function mapi_directory_move($source, $dest, $options = array()) {
  $workspace = _mapi_file_workspace($options);

  // safety check
  if (!mapi_directory_check_location($source, $workspace)) {
    return FALSE;
  }

  // safety check
  if (!mapi_directory_check_location($dest, $workspace)) {
    return FALSE;
  }

  // check that dest is not inside source
  if (strpos(realpath(rtrim($dest, '/')), realpath(rtrim($source, '/'))) === 0) {
    return FALSE;
  }

  // make sure that nothing exists at destination
  if (is_file($dest) || is_dir($dest)) {
    return FALSE;
  }

  return @rename($source, $dest);
}

/**
 * Delete a directory, from within a given workspace.
 *
 * @param $directory
 *   The directory to be deleted.
 * @param $options
 *   An array of options. Valid values are:
 *     'workspace' - The workspace in which this function works.
 *     'files'     - Remove all the files in the directory as well.
 *     'recursive' - Recurse any lower level directories.
 * @return
 *   Returns FALSE on failure, TRUE on success.
 */
function mapi_directory_delete($directory, $options = array()) {
  $files     = isset($options['files']) && $options['files'];
  $recursive = isset($options['recursive']) && $options['recursive'];
  $workspace = _mapi_file_workspace($options);

  // safety check
  if (!$directory = mapi_directory_check($directory, $workspace)) {
    return FALSE;
  }

  // make sure that something exists at destination
  if (!is_dir($directory)) {
    return FALSE;
  }

  $dirs = array();

  // get all the files
  if ($files) {
    if ($dh = opendir($directory)) {
      while (($file = readdir($dh)) !== FALSE) {
        // ignore the listings of '.', and '..'
        if (in_array($file, array('.', '..'))) {
          continue;
        }

        $path = $directory .'/'. $file;
        // delete file
        if (is_file($path)) {
          @unlink($path);
        }
        // otherwise list the sub-directories for later removal
        elseif (is_dir($path)) {
          $dirs[] = $file;
        }
      }
      closedir($dh);
    }
  }

  // do we have sub-directories to consider?
  if (count($dirs)) {
    if ($recursive) {
      foreach ($dirs as $dir) {
        mapi_directory_delete($directory .'/'. $dir, $options);
      }
    }
    // not recursive
    else {
      return FALSE;
    }
  }

  return @rmdir($directory);
}

/**
 * Creates a directory, within a given workspace.
 *
 * @param $directory
 *   The directory to be created.
 * @param $options
 *   An array of options. Valid values are:
 *     'workspace' - The workspace in which this function works.
 *     'recursive' - Allows recursive creation of a directory.
 * @return
 *   Returns FALSE on failure, TRUE on success.
 */
function mapi_directory_create($directory, $options = array()) {
  $options['create'] = TRUE;
  return mapi_directory_check($directory, $options);
}

/**
 * Check whether a file is located in the given workspace directories.
 *
 * @param $filename
 *   The filename to delete.
 * @param $options
 *   An array of options. Valid values are:
 *     'workspace' - The workspace in which this function works.
 * @return
 *   Returns FALSE on failure, returns the filename.
 */
function mapi_file_check_location($filename, $options = array()) {
  $workspace = _mapi_file_workspace($options);

  // list of directories that a file can reside in.
  $dirs = mapi_workspace_list($workspace);

  // check all the directories
  foreach ($dirs as $dir) {
    if ($fname = file_check_location($filename, $dir)) {
      return $filename;
    }
  }

  // check offsite locations as a filename
  if (!mapi_file_is_local($filename)) {
    $uri_base = mapi_file_uri($filename);

    // checking for allowed external files
    // make sure Windows and MAC line returns function.
    $paths = str_replace(array("\r", "\r\n"), "\n", variable_get('mapi_allowed_external', ''));


    // perform checks
    $checks = explode("\n", $paths);
    foreach ($checks as $path) {
      $search = str_replace(array('@', '\*'), array('\\@', '.*'), preg_quote($path));

      // checked for allowed website
      if (preg_match('@^http(s|)://'. $search .'@', $uri_base)) {
        return $filename;
      }
    }
  }

  // otherwise failed
  return FALSE;
}

/**
 * Copy a file from one place to another.
 *
 * @param $source
 *   The source filename.
 * @param $dest
 *   The destination filename.
 * @param $options
 *   An array of options. Valid values are:
 *     'workspace' - The workspace in which this function works.
 *     'replace'   - Replace behavior when the destination file already exists.
 *        FILE_EXISTS_REPLACE - Replace the existing file
 *        FILE_EXISTS_RENAME - Append _{incrementing number} until the filename is unique
 *        FILE_EXISTS_ERROR - Do nothing and return FALSE.
 * @return
 *   Returns FALSE on failure, and the copied filename on success.
 */
function mapi_file_copy($source, $dest, $options = array()) {
  $replace = isset($options['replace']) && $options['replace'] ? $options['replace'] : FILE_EXISTS_RENAME;
  $workspace = _mapi_file_workspace($options);

  // do check for source
  if (!$source = mapi_file_check_location($source, $options)) {
    return FALSE;
  }

  // do check for destination
  if (!$dest = mapi_file_check_location($dest, $options)) {
    return FALSE;
  }

  // only move the files if the are not the same
  if ($source != $dest) {
    if (is_file($dest)) {
      switch ($replace) {
        case FILE_EXISTS_RENAME:
          $dest = mapi_file_freename($dest);
          break;
        case FILE_EXISTS_ERROR:
          return FALSE;
      }
    }

    // now simply rename file, this should preserve the file settings, etc.
    if (!@copy($source, $dest)) {
      return FALSE;
    }

    // Give everyone read access so that FTP'd users or
    // non-webserver users can see/read these files.
    @chmod($dest, 0664);

    module_invoke_all('mapi_extensions', 'copy', array('source' => $source, 'dest' => $dest));
  }

  return $dest;
}

/**
 * Move a given filename to another location, within a given workspace.
 *
 * @param $source
 *   The source filename.
 * @param $dest
 *   The destination filename.
 * @param $options
 *   An array of options. Valid values are:
 *     'workspace' - The workspace in which this function works.
 *     'replace'   - Replace behavior when the destination file already exists.
 *        FILE_EXISTS_REPLACE - Replace the existing file
 *        FILE_EXISTS_RENAME - Append _{incrementing number} until the filename is unique
 *        FILE_EXISTS_ERROR - Do nothing and return FALSE.
 * @return
 *   Returns FALSE on failure, and the moved filename on success.
 */
function mapi_file_move($source, $dest, $options = array()) {
  $replace = isset($options['replace']) && $options['replace'] ? $options['replace'] : FILE_EXIST_RENAME;
  $workspace = _mapi_file_workspace($options);

  $source = realpath($source);
  if ($dest = mapi_file_copy($source, $dest, array('replace' => $replace, 'workspace' => $workspace))) {
    module_invoke_all('mapi_extensions', 'move', array('source' => $source, 'dest' => $dest));
    if ($source != $dest && file_delete($source)) {
      return $dest;
    }
  }

  return FALSE;
}

/**
 * Delete only valid filenames, with a given workspace.
 *
 * @param $filename
 *   The filename to delete.
 * @param $options
 *   An array of options. Valid values are:
 *     'workspace' - The workspace in which this function works.
 * @return
 *   Returns FALSE on failure, TRUE on success.
 */
function mapi_file_delete($filename, $options = array()) {
  $workspace = _mapi_file_workspace($options);
  if ($path = mapi_file_check_location($filename, $workspace)) {
    module_invoke_all('mapi_extensions', 'delete', $filename);
    return file_delete($path);
  }
}

/**
 * Transfer file using http to client. Pipes a file through Drupal to the
 * client.
 *
 * @param $source
 *   File to transfer.
 * @param $headers
 *   An array of http headers to send along with file.
 */
function mapi_file_transfer($source, $headers) {
  ob_end_clean();

  foreach ($headers as $header) {
    // To prevent HTTP header injection, we delete new lines that are
    // not followed by a space or a tab.
    // See http://www.w3.org/Protocols/rfc2616/rfc2616-sec4.html#sec4.2
    $header = preg_replace('/\r?\n(?!\t| )/', '', $header);
    drupal_set_header($header);
  }

  $source = realpath($source);

  // Transfer file in 1024 byte chunks to save memory usage.
  if (is_file($source) && ($fd = fopen($source, 'rb'))) {
    while (!feof($fd)) {
      print fread($fd, 1024);
    }
    fclose($fd);
  }
  else {
    drupal_not_found();
  }
  exit();
}

/**
 * Get the mime type for a filename, including URLs with http:// or https://
 * format. Will attempt to use the extension determination process first.
 *
 * @param $filename
 *   Name of file to check for mimetype
 * @return
 *   The mime type of a file, 'application/octet-stream' if unknown
 */
function mapi_file_mime($filename) {
  // attempt to find mime by extension first
  if ($ext = mapi_file_ext($filename)) {
    return mapi_extension_mime($ext);
  }

  $mime = FALSE;
  if (!mapi_file_is_local($filename)) {
    if ($mime = _mapi_file_external($filename)) {
      return $mime;
    }
  }

  return 'application/octet-stream';
}

/**
 * Create a filename which will not overwrite ones already existing.
 *
 * @param $filename
 *   Name of filename
 * @return
 *   A unique filename which preserves the extension and general details of the filename.
 */
function mapi_file_freename($filename) {
  $dir = dirname($filename);
  $name = mapi_file_name($filename);
  $ext = mapi_file_ext($filename);

  // no need to generate a new name, since the filename already doesn't exist.
  if (!is_file($filename)) {
    return $filename;
  }

  // check for the last _d on name (so we avoid _0_0_0 scenarios
  if ($pos = strrpos($name, '_')) {
    if (is_numeric(substr($name, $pos+1))) {
      $name = substr($name, 0, $pos);
    }
  }

  $count = 1;
  $filename = $dir .'/'. $name .'.'. $ext;
  while (is_file($filename)) {
    $filename = $dir .'/'. $name .'_'. $count++ .'.'. $ext;
  }

  return $filename;
}

/**
 * Provide a free filename, in a temporary directory, that has the appropriate extension.
 * Although this could potentially be a race condition, we're running it through mapi_file_freename
 * to get one we can write to.
 *
 * @param $ext
 *   The filename extension.
 * @param $workspace
 *   The given workspace.
 * @return
 *   The temporary filename matching this extension.
 */
function mapi_file_temp($ext, $workspace = '') {
  // get filename
  $file = mapi_file_freename(mapi_workspace_path('temp', $workspace) .'/mapi'. time() .'.'. $ext);

  // make sure that filename is touched, making sure it is then "in use".
  touch($file);

  // return file
  return $file;
}

/**
 * For opening files that are not located within the current server file system.
 * Validates the URI exists and is allowed to be accessed.
 * Returns the mime type of the URI, for ease of locating actual extension later.
 *
 * @param $uri
 *   An external URI
 * @return
 *   A mime-type for the URI if successful, FALSE if not.
 */
function _mapi_file_external($uri) {
  static $uris = array();

  // stored result of offsite existance
  if (array_key_exists($uri, $uris)) {
    return $uris[$uri];
  }

  // get the base URI.
  $uri_base = mapi_file_uri($uri);

  // checking for allowed external files
  // make sure Windows and MAC line returns function.
  $paths = str_replace(array("\r", "\r\n"), "\n", variable_get('mapi_allowed_external', ''));


  // perform checks
  $checks = array_filter(explode("\n", $paths));
  foreach ($checks as $path) {
    $search = str_replace(array('@', '\*'), array('\\@', '.*'), preg_quote($path));

    // checked for allowed website
    if (preg_match('@^http(s|)://'. $search .'@', $uri_base)) {
      // create stream context for testing the HTTP result, since we're only dealing with HTTP
      $options = array(
        'http' => array(
          'user_agent'    => 'spider',
          'max_redirects' => 10,
          'timeout'       => 120,
        )
      );

      // open file and get the details for the looking at the mimetype
      $context = stream_context_create($options);
      if ($handle = @fopen($uri_base, 'r', FALSE, $context)) {
        $results = array();
        if (!empty($http_response_header)) {
          $results = $http_response_header;
        }
        fclose($handle);

        foreach (array_reverse($results) as $line) {
          if (preg_match('@Content-Type:\s+([\w\-\./+]+)(;\s+charset=(\S+))?@i', $line, $matches)) {
            // check mime type
            if (!isset($matches[1])) {
              continue;
            }

            // now we know that it exists, and what it's mime type is
            $uris[$uri] = $matches[1];

            // return the appropriate details
            return $uris[$uri];
          }
        }
      }
    }
  }

  // save cached version of the URI
  if (!isset($uris[$uri])) {
    $uris[$uri] = FALSE;
  }

  // return the value, since this indicators existance, and mime-type
  return $uris[$uri];
}

/**
 * Helper function for workspace option.
 */
function _mapi_file_workspace($options) {
  return isset($options['workspace']) && in_array($options['workspace'], mapi_workspace_list()) ? $options['workspace'] : 'mapi';
}

