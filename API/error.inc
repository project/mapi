<?php

/**
 * @file
 * MAPI error handling.
 */

/**
 * Makes sure that the correct routines are called for media errors
 *
 * @param $message
 *   The message to record for the watchdog and drupal_set_message
 * @param $variables
 *   Array of variables to replace in the message on display or NULL if message is already translated or not possible to translate.
 * @param $watchdog_severity
 *   The severity of the message (see watchdog_severity_levels())
 */
function mapi_error($message, $variables = array(), $watchdog_severity = WATCHDOG_ERROR) {
  watchdog('mapi', $message, $variables, $watchdog_severity);
  drupal_set_message(t($message, $variables), 'error');
}
