<?php

/**
 *  This is executed to process an image into a scaled version
 *
 *  Parameters passed are:
 *  $filename - filename of file to load and resave to
 *  $width - width of transformation
 *  $height - height of transformation
 *  $options - contains everything
 *
 *
 *  required to return $result = true for success
 */

// now check for the fit
if ($options['fit'] == 'outside') {
  $ratio = $original['width'] / $original['height'];
  $new_ratio = $width / $height;
  $width = ($ratio > $new_ratio ? 0 : $width);
  $height = ($ratio < $new_ratio ? 0 : $height);
}

// set impossibly large values if the width and height aren't set.
$width = $width ? $width : 9999999;
$height = $height ? $height : 9999999;

$result = true;
if (($original['height'] >= $height) || ($original['width'] >= $width)) {
  // only when it's not both equal perform the transformation
  if (!($original['height'] == $height && $original['width'] == $width)) {
    $temp = mapi_file_temp(mapi_file_ext($filename));
    $result = image_scale($filename, $temp, $width, $height);
    $filename = $temp;
  }
}
