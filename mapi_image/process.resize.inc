<?php

/**
 *  This is executed to process an image into a resized version
 *
 *  Parameters passed are:
 *  $filename - filename of file to load and resave to
 *  $width - width of transformation
 *  $height - height of transformation
 *  $options - contains everything
 *
 *
 *  required to return $result = true for success
 */

$temp = mapi_file_temp(mapi_file_ext($filename));
$result = image_resize($filename, $temp, $width, $height);
$filename = $temp;
