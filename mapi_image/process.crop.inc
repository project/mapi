<?php

/**
 *  This is executed to process an image into a cropped version
 *
 *  Parameters passed are:
 *  $filename - filename of file to load and resave to
 *  $width - width of transformation
 *  $height - height of transformation
 *  $options - contains everything
 *  $original - contains the dimensions of the original filename
 *
 *
 *  required to return $result = true on success
 */

$ow = $original['width'];
$oh = $original['height'];

// check that we crop only what is possible
$width = $width > $ow ? $ow : $width;
$height = $height > $oh ? $oh : $height;

// handle standard options
$xoffset = $options['xoffset'];
switch ($xoffset) {
  case 'left':   $xoffset = 0; break;
  case 'right':  $xoffset = $ow - $width; break;
  case 'center': $xoffset = (int)(($ow) - ($width)) / 2; break;
  default:       $xoffset = (int)$xoffset; break;
}

$yoffset = $options['yoffset'];
switch ($yoffset) {
  case 'top':    $yoffset = 0; break;
  case 'bottom': $yoffset = $oh - $height; break;
  case 'center': $yoffset = (int)(($oh) - ($height)) / 2; break;
  default:       $yoffset = (int)$yoffset; break;
}

// do boundary checks
if ($xoffset < 0) {
  $width += $xoffset;
  $xoffset = 0;
}
if ($yoffset < 0) {
  $height += $yoffset;
  $yoffset = 0;
}
if (($xoffset + $width) > $ow) {
  $width = $ow - $xoffset;
}
if (($yoffset + $height) > $oh) {
  $height = $oh - $yoffset;
}

$temp = mapi_file_temp(mapi_file_ext($filename));
$result = image_crop($filename, $temp, $xoffset, $yoffset, $width, $height);
$filename = $temp;