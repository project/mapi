<?php

/**
 *  This is executed to process an image into a scaled version
 *
 *  Parameters passed are:
 *  $filename - filename of file to load and resave to
 *  $width - width of transformation
 *  $height - height of transformation
 *  $options - contains everything
 *
 *
 *  required to return $result = true for success
 */

// get color coordinates
if (strlen($options['padding-color']) == 3) {
  preg_match('/([a-fA-F\d])([a-fA-F\d])([a-fA-F\d])/', $options['padding-color'], $padding_color);
  $padding_color[1] = $padding_color[1] . $padding_color[1];
  $padding_color[2] = $padding_color[2] . $padding_color[2];
  $padding_color[3] = $padding_color[3] . $padding_color[3];
}
else {
  preg_match('/([a-fA-F\d][a-fA-F\d])([a-fA-F\d][a-fA-F\d])([a-fA-F\d][a-fA-F\d])/', $options['padding-color'], $padding_color);
}
if (strlen($options['border-color']) == 3) {
  preg_match('/([a-fA-F\d])([a-fA-F\d])([a-fA-F\d])/', $options['border-color'], $border_color);
  $border_color[1] = $border_color[1] . $border_color[1];
  $border_color[2] = $border_color[2] . $border_color[2];
  $border_color[3] = $border_color[3] . $border_color[3];
}
else {
  preg_match('/([a-fA-F\d][a-fA-F\d])([a-fA-F\d][a-fA-F\d])([a-fA-F\d][a-fA-F\d])/', $options['border-color'], $border_color);
}

$border = $options['border-width'];
$padding = $options['padding-width'];

$resize_width = $width + ($border * 2) + ($padding * 2);
$resize_height = $height + ($border * 2) + ($padding * 2);

$ext = mapi_file_ext($filename);
switch ($ext) {
  case 'png': $original = @imagecreatefrompng($filename); break;
  case 'jpg': $original = @imagecreatefromjpeg($filename); break;
  case 'gif': $original = @imagecreatefromgif($filename); break;
}
if (!$original) {
  $result = false;
  return;
}

// Now begin processing
$image = imagecreatetruecolor($resize_width, $resize_height);

$bcolor = imagecolorallocate($image, hexdec($border_color[1]), hexdec($border_color[2]), hexdec($border_color[3]));
$pcolor = imagecolorallocate($image, hexdec($padding_color[1]), hexdec($padding_color[2]), hexdec($padding_color[3]));

// Generate border
imagefilledrectangle($image, 0, 0, $resize_width, $resize_height, $bcolor);

// Generate padding
imagefilledrectangle($image, $border, $border, (int)($border + ($padding * 2) + $width - 1), (int)($border + ($padding * 2) + $height - 1), $pcolor);

// copy original image over the newly created image
if (imagecopymerge($image, $original, ($border + $padding), ($border + $padding), 0, 0, $width, $height, 100)) {
  $temp = mapi_file_temp($ext);
  switch ($ext) {
    case 'png': $result = imagepng($image, $temp); break;
    case 'jpg': $result = imagejpeg($image, $temp); break;
    case 'gif': $result = imagepng($image, $temp); break;
  }
  $filename = $temp;
}

// do cleanup
imagedestroy($image);
imagedestroy($original);
