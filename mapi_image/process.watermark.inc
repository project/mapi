<?php

/**
 * This is executed to process an image with a watermarked version
 *
 * Parameters passed are:
 *  $filename - filename of file to load and resave to
 *  $width - width of transformation
 *  $height - height of transformation
 *  $options - contains everything
 *
 *
 * required to return $result = TRUE for success
 */

// we must have a valid watermark file for this to function
if (!is_file($options['watermark']) || !($water = image_get_info($options['watermark']))) {
  // it's fine, we don't do anything to "$filename"
  $result = true;
  return;
}

// get extensions for the filename and watermark
$ext_main = mapi_file_ext($filename);
$ext_water = mapi_file_ext($options['watermark']);

$ow = $width > $water['width'] ? $water['width'] : $width;
$oh = $height > $water['height'] ? $water['height'] : $height;

// handle opacity (0 - 100 percent).
$opacity = rtrim($options['opacity'], '%');
$opacity = max(min($opacity, 100), 0);

// handle standard options
$xoffset = $options['xoffset'];
switch ($xoffset) {
  case 'left':   $xoffset = 0; break;
  case 'right':  $xoffset = $width - $ow; break;
  case 'center': $xoffset = (int)(($width) - ($ow)) / 2; break;
  default:       $xoffset = (int)$xoffset; break;
}

$yoffset = $options['yoffset'];
switch ($yoffset) {
  case 'top':    $yoffset = 0; break;
  case 'bottom': $yoffset = $height - $oh; break;
  case 'center': $yoffset = (int)(($height) - ($oh)) / 2; break;
  default:       $yoffset = (int)$yoffset; break;
}

// create the images
switch ($ext_main) {
  case 'png': $main_image = @imagecreatefrompng($filename); break;
  case 'jpg': $main_image = @imagecreatefromjpeg($filename); break;
  case 'gif': $main_image = @imagecreatefromgif($filename); break;
}

switch ($ext_water) {
  case 'png': $water_image = @imagecreatefrompng($options['watermark']); break;
  case 'gif': $water_image = @imagecreatefromgif($options['watermark']); break;
}

if (!$main_image || !$water_image) {
  $result = FALSE;
  return;
}
// TODO: possibilty of resizing watermark.

// if successfully copied then save to filename.
$result = false;
if (imagecopyresampled($main_image, $water_image, $xoffset, $yoffset, 0, 0, $ow, $oh, $ow, $oh)) {
  $temp = mapi_file_temp($ext_main);
  switch ($ext_main) {
    case 'png': $result = imagepng($main_image, $temp); break;
    case 'jpg': $result = imagejpeg($main_image, $temp); break;
    case 'gif': $result = imagepng($main_image, $temp); break;
  }
  if (!empty($result) && $result) {
    $filename = $temp;
  }
  else {
    file_delete($temp);
  }
}

// clean up the generated images.
imagedestroy($main_image);
imagedestroy($water_image);