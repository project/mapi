<?php

/**
 * Implementation of hook_mapi_transforms().
 */
function mapi_image_mapi_transforms($op, $transform, $data, $filename) {
  switch ($op) {
    // provide list of extensions for transforms
    case 'list':
      return array(
        'crop' => array('png', 'gif', 'jpg'),
        'scale' => array('png', 'gif', 'jpg'),
        'resize' => array('png', 'gif', 'jpg'),
        'watermark' => array('png', 'gif', 'jpg'),
        'border' => array('png', 'gif', 'jpg'),
      );
      break;

    case 'test':
      if (!in_array(mapi_file_ext($filename), array('png', 'gif', 'jpg'))) {
        return false;
      }
      return $filename;

    case 'info':
      $details = array(
        'crop'   => t('Allows cropping of images to specific boundaries.'),
        'scale'  => t('Resizes a picture, keeping the original aspect ratio.'),
        'resize' => t('Resizes the picture to a specified setting.'),
        'watermark' => t('Allows a watermark to be applied to an image.'),
        'border' => t('Applies a border to an image.'),
      );
      if (array_key_exists($transform, $details)) {
        return $details[$transform];
      }
      break;

    case 'defaults':
      switch ($transform) {
        case 'border':
          return array('border-width' => 1, 'padding-width' => 0, 'border-color' => 'fff', 'padding-color' => 'fff');
        case 'scale':
          return array('width' => '100%', 'height' => '100%', 'fit' => 'inside');
        case 'resize':
          return array('width' => '100%', 'height' => '100%');
        case 'crop':
          return array('width' => '100%', 'height' => '100%', 'xoffset' => 'left', 'yoffset');
        case 'watermark':
          return array('xoffset' => 'left', 'yoffset', 'watermark' => '');
      }
      break;

    case 'configure':
      switch ($transform) {
        case 'border':
            $form['border-width'] = array(
              '#type' => 'textfield',
              '#title' => t('Border Width'),
              '#default_value' => (isset($data['border-width']) ? $data['border-width'] : 1),
              '#description' => t('Enter a width in pixels.'),
            );

            $form['padding-width'] = array(
              '#type' => 'textfield',
              '#title' => t('Padding Width'),
              '#default_value' => (isset($data['padding-width']) ? $data['padding-width'] : 0),
              '#description' => t('Enter a width in pixels.'),
            );

            $form['border-color'] = array(
              '#type' => 'textfield',
              '#title' => t('Border Color'),
              '#default_value' => (isset($data['border-color']) ? $data['border-color'] : 'fff'),
              '#description' => t('Enter a color as CSS hex color (i.e. fff, f0f0f0).'),
            );

            $form['padding-color'] = array(
              '#type' => 'textfield',
              '#title' => t('Padding Color'),
              '#default_value' => (isset($data['padding-color']) ? $data['padding-color'] : 'fff'),
              '#description' => t('Enter a color as CSS hex color (i.e. fff, f0f0f0).'),
            );
          break;
        case 'scale':
          $helptext = array();
          $helptext['inside'] = t('<strong>Inside dimensions</strong>: Final dimensions will be less than or equal to the entered width and height. Useful for ensuring a maximum height and/or width.');
          $helptext['outside'] = t('<strong>Outside dimensions</strong>: Final dimensions will be greater than or equal to the entered width and height. Ideal for cropping the result to a square.');
          $description = '<ul><li>'. implode('</li><li>', $helptext) .'</li><ul>';

          $form['fit'] = array(
            '#type' => 'select',
            '#title' => t('Scale to fit'),
            '#options' => array('inside' => t('Inside dimensions'), 'outside' => t('Outside dimensions')),
            '#default_value' => (isset($data['fit']) ? $data['fit'] : 'inside'),
            '#weight' => 1,
            '#description' => $description,
          );
        case 'resize':
          $form['width'] = array(
            '#type' => 'textfield',
            '#title' => t('Width'),
            '#default_value' => (isset($data['width']) ? $data['width'] : '100%'),
            '#description' => t('Enter a width in pixels or as a percentage. i.e. 500 or 80%.'),
          );

          $form['height'] = array(
            '#type' => 'textfield',
            '#title' => t('Height'),
            '#default_value' => (isset($data['height']) ? $data['height'] : '100%'),
            '#description' => t('Enter a height in pixels or as a percentage. i.e. 500 or 80%.'),
          );
          break;
        case 'crop':
          $form['width'] = array(
            '#type' => 'textfield',
            '#title' => t('Width'),
            '#default_value' => (isset($data['width']) ? $data['width'] : '100%'),
            '#description' => t('Enter a width in pixels or as a percentage. i.e. 500 or 80%.'),
          );

          $form['height'] = array(
            '#type' => 'textfield',
            '#title' => t('Height'),
            '#default_value' => (isset($data['height']) ? $data['height'] : '100%'),
            '#description' => t('Enter a height in pixels or as a percentage. i.e. 500 or 80%.'),
          );

          $form['xoffset'] = array(
            '#type' => 'textfield',
            '#title' => t('X Offset'),
            '#default_value' => (isset($data['yoffset']) ? $data['xoffset'] : 'left'),
            '#description' => t('Enter an offset in pixels or use a keyword: <em>left</em>, <em>center</em>, or <em>right</em>.'),
          );

          $form['yoffset'] = array(
            '#type' => 'textfield',
            '#title' => t('Y Offset'),
            '#default_value' => (isset($data['yoffset']) ? $data['yoffset'] : 'top'),
            '#description' => t('Enter an offset in pixels or use a keyword: <em>top</em>, <em>center</em>, or <em>bottom</em>.'),
          );
          break;
        case 'watermark':
            $form['xoffset'] = array(
              '#type' => 'textfield',
              '#title' => t('X Offset'),
              '#default_value' => (isset($data['yoffset']) ? $data['xoffset'] : 'left'),
              '#description' => t('Enter an offset in pixels or use a keyword: <em>left</em>, <em>center</em>, or <em>right</em>.'),
            );

            $form['yoffset'] = array(
              '#type' => 'textfield',
              '#title' => t('Y Offset'),
              '#default_value' => (isset($data['yoffset']) ? $data['yoffset'] : 'top'),
              '#description' => t('Enter an offset in pixels or use a keyword: <em>top</em>, <em>center</em>, or <em>bottom</em>.'),
            );

            $form['watermark'] = array(
              '#type' => 'textfield',
              '#title' => t('Watermark Image'),
              '#default_value' => (isset($data['watermark']) ? $data['watermark'] : ''),
              '#description' => t('This must be a valid PNG or GIF image file, located on the server. Retains watermark transparency.'),
            );
          break;
      }
      return $form;
      break;

    case 'validate':
      if (!empty($data['border-width']) && !preg_match("/^\d+$/", $data['border-width'])) {
        form_set_error('border-width', t('Width must be a positive number.'));
      }
      if (!empty($data['padding-width']) && !preg_match("/^\d+$/", $data['padding-width'])) {
        form_set_error('padding-width', t('Width must be a positive number.'));
      }
      if (!empty($data['border-color']) && !preg_match("/^([a-fA-F\d]{3}|[a-fA-F\d]{6})$/", $data['border-color'])) {
        form_set_error('border-color', t('Color must be valid CSS hex color of either 3 or 6 hexadecimal characters.'));
      }
      if (!empty($data['padding-color']) && !preg_match("/^([a-fA-F\d]{3}|[a-fA-F\d]{6})$/", $data['padding-color'])) {
        form_set_error('padding-color', t('Color must be valid CSS hex color of either 3 or 6 hexadecimal characters.'));
      }
      if (!empty($data['height']) && !preg_match("/^\d+(%|)$/", $data['height'])) {
        form_set_error('height', t('Height must be a number, or a percentage.'));
      }
      // check if number or percentage
      if (!empty($data['width']) && !preg_match("/^\d+(%|)$/", $data['width'])) {
        form_set_error('width', t('Width must be a number, or a percentage.'));
      }
      //
      if (!empty($data['xoffset']) && !preg_match("/^(\d+|left|center|right)$/i", $data['xoffset'])) {
        form_set_error('xoffset', t('XOffset must be either a number, or left, center, or right.'));
      }
      //
      if (!empty($data['yoffset']) && !preg_match("/^(\d+|top|center|bottom)$/i", $data['yoffset'])) {
        form_set_error('yoffset', t('YOffset must be either a number, or top, center, or bottom.'));
      }
      if (!empty($data['watermark']) && (!is_file($data['watermark']) || !in_array(mapi_file_ext($data['watermark']), array('gif', 'png')))) {
        form_set_error('watermark', t('Watermark is not a file, or is not a valid GIF or PNG image.'));
      }
      break;

    // process via the transforms
    case 'process':
      // get the details, since we may need to change things
      $details = image_get_info($filename);

      $options = array();
      if (!empty($data['width'])) {
        $options['width'] = ($data['width'] ? $data['width'] : $details['width']);
        $options['width'] = (int)(substr($options['width'], -1) == '%' ? ($details['width'] / 100) * trim($options['width'], '%') : $options['width']);
      }
      else {
        $options['width'] = $details['width'];
      }
      if (!empty($data['height'])) {
        $options['height'] = ($data['height'] ? $data['height'] : $details['height']);
        $options['height'] = (int)(substr($options['height'], -1) == '%' ? ($details['height'] / 100) * trim($options['height'], '%') : $options['height']);
      }
      else {
        $options['height'] = $details['height'];
      }
      // allowable options to pass through
      foreach (array('xoffset', 'yoffset', 'fit', 'border-color', 'padding-color', 'border-width', 'padding-width', 'watermark') as $key) {
        if (!empty($data[$key])) {
          $options[$key] = $data[$key];
        }
      }

      $result = _mapi_image_transform($filename, $transform, $options, $details);

      // failure should be reported
      if (!$result) {
        mapi_error('Transform failed [%transform] on [%file]', array('%transform' => $transform, '%file' => $filename));
      }

      // always return generated filename
      return $filename;
      break;
  }
}

/**
 *  Processes files using externally stored include files.
 */
function _mapi_image_transform(&$filename, $transform, $options, $original) {
  $result = false;
  if (in_array($transform, array('crop', 'scale', 'resize', 'watermark', 'border'))) {
    $width = trim($options['width']);
    $height = trim($options['height']);

    include drupal_get_path('module', 'mapi_image') ."/process.{$transform}.inc";
  }
  return $result;
}

