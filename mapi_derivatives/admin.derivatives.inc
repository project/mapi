<?php

/**
 *  Complete list of all the derivatives that exist within a system.
 */
function mapi_derivatives_admin_page() {
  drupal_add_css(drupal_get_path('module', 'mapi_derivatives') .'/mapi_derivatives.css');

  $rows = array();
  foreach (mapi_derivatives_list_by_parentage() as $did => $parentage) {
    $derivative = mapi_derivative_load($did);
    $transforms = array();
    foreach (mapi_transform_list_by_derivative($derivative->did) as $tid => $transform) {
      $transforms[] = l($transform->name, 'admin/mapi/derivatives/'. $did .'/transforms/'. $tid);
    }
    $transforms[] = l(t('Add'), 'admin/mapi/derivatives/'. $did .'/transforms/add');

    $rows[] = array(
      theme('indentation', count(array_filter(explode('/', $parentage))) - 1) .
        l($derivative->name, 'admin/mapi/derivatives/'. $did) . ' - (' . $derivative->ext . ' -- ' . $derivative->output . ')',
      l(t('Add'), 'admin/mapi/derivatives/'. $did .'/add'),
      implode('<br />', $transforms),
    );
  }
  $rows[] = array(array('data' => l('Add base derivative', 'admin/mapi/derivatives/add'), 'colspan' => 3));

  $output = '<div>'. t('Derivatives allow for automated generation of items of similar size, resolution, or length.') .'</div>';
  $output .= theme('table', array(t('Derivative'), t('Subderivative'), t('Transforms')), $rows);

  return $output;
}

/**
 *  Lists the details about the derivatives
 */
function mapi_derivatives_derivative_page($derivative) {
  // set breadcrumbs to the appropriate hierarchy
  $breadcrumbs = _mapi_derivatives_breadcrumb($derivative->did);
  drupal_set_breadcrumb($breadcrumbs);

  // List the transforms this derivative applies to
  $rows = array();
  foreach (mapi_transform_list_by_derivative($derivative->did) as $tid => $transform) {
    $rows[] = array(
      l($transform->name, 'admin/mapi/derivatives/'. $derivative->did .'/transforms/'. $tid),
      _mapi_derivatives_transform_print_settings($transform->settings)
    );
  }
  $rows[] = array(array('data' => l(t('Add'), 'admin/mapi/derivatives/'. $derivative->did .'/transforms/add'), 'colspan' => 2));

  $output = '<div>'. t('Transforms allow for the derivative to provide some action of standardization. A blank derivative does nothing.') .'</div>';
  $output .= theme('table', array(t('Transform'), t('Settings')), $rows);

  return $output;
}

/**
 * Add/Edit form for a derivative
 */
function mapi_derivatives_edit_form(&$form_state, $derivative, $parent) {
  // handle the addition differently
  if (!$derivative) {
    $breadcrumbs = _mapi_derivatives_breadcrumb($parent->did, true);
    drupal_set_title($parent ? t('Create derivative of @title', array('@title' => _mapi_derivatives_title($parent))) : t('Create base derivative'));
  }
  else {
    $breadcrumbs = _mapi_derivatives_breadcrumb($derivative->did, true);
  }
  drupal_set_breadcrumb($breadcrumbs);

  // this is what edits the derivative
  $form['did'] = array('#type' => 'value', '#value' => $derivative->did);
  $form['parent'] = array('#type' => 'value', '#value' => $parent->did);

  // we're creating if derivative is null
  // otherwise we're editing.
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('The name of the derivative, must only include alphanumeric, hyphen, underscore or space characters.'),
    '#default_value' => $derivative->name,
  );

  // only select something if we don't already have a base extension
  if (empty($derivative->ext) && empty($parent->output)) {
    $extensions = mapi_extension_list();
    if (count($extensions) > 0) {
      $form['ext'] = array(
        '#type' => 'radios',
        '#title' => t('Extensions'),
        '#options' => drupal_map_assoc($extensions),
        '#default_value' => $extensions[0],
        '#description' => t('Select the extension for which this derivative will apply.'),
      );
    }
    else {
      return array(array('#value' => t('Unable to add a derivative since no extensions are currently defined.')));
    }
  }
  // use the derivative extension
  elseif (!empty($derivative->ext)) {
    $form['ext'] = array('#type' => 'value', '#value' => $derivative->ext);
    $form['ext_msg'] = array('#value' => '<div>'. t('The extension for this derivative is (%ext)', array('%ext' => $derivative->ext)) .'</div>');
  }
  // otherwise use the parent extension
  else {
    $form['ext'] = array('#type' => 'value', '#value' => $parent->output);
    $form['ext_msg'] = array('#value' => '<div>'. t('The extension for this derivative is (%ext)', array('%ext' => $parent->output)) .'</div>');
  }

  $form['submit'] = array('#type' => 'submit', '#value' => $derivative->did ? t('Update') : t('Add'));
  if ($derivative->did) {
    $form['delete'] = array('#type' => 'submit', '#value' => t('Delete'));
  }

  return $form;
}

/**
 * Validation of the mapi_derivatives_edit_form
 */
function mapi_derivatives_edit_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  if ($values['op'] == t('Update') || $values['op'] == t('Add')) {
    if (preg_match('/[^\w\d\-\_ ]/', $values['name'])) {
      form_set_error('name', t('The name of the derivative, must only include alphanumeric, hyphen, underscore or space characters.'));
      return;
    }

    // check that extension is possible, for the given name
    if ($values['op'] == t('Add')) {
      $taken = mapi_extension_list_by_derivative($values['name']);
      $ext = $values['ext'];
      if (array_key_exists($ext, $taken)) {
        form_set_error('ext]['. $ext, t('This extension is already being used by a derivative of the same name.'));
      }
    }
  }
}

/**
 * Submission for derivative_edit form
 */
function mapi_derivatives_edit_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  switch ($values['op']) {
    // redirect to the delete page.
    case t('Delete'):
      drupal_goto('admin/mapi/derivatives/'. $values['did'] .'/delete');
      break;

    case t('Add'):
      if ($derivative = mapi_derivative_create($values['name'], $values['ext'], (int)$values['parent'])) {
        $form_state['did'] = $derivative->did;
        $form_state['redirect'] = 'admin/mapi/derivatives/'. $derivative->did;
      }
      break;

    case t('Update'):
      if ($values['did']) {
        mapi_derivative_update($values['did'], $values['name']);
        $form_state['redirect'] = 'admin/mapi/derivatives/'. $values['did'];
      }
      break;
  }
}

/**
 * Deletion of a derivative.
 */
function mapi_derivatives_delete_form(&$form_state, $derivative) {
  $breadcrumbs = _mapi_derivatives_breadcrumb($derivative->did, true);
  drupal_set_breadcrumb($breadcrumbs);

  $form['derivative'] = array('#type' => 'value', '#value' => $derivative->did);
  $form['parent'] = array('#type' => 'value', '#value' => $derivative->parent);

  // provide user interaction
  $notice = t('This action cannot be undone. It will delete all sub-derivatives.');

  $form = confirm_form($form,
    t('Are you sure you want to delete derivative %derivative?', array('%derivative' => _mapi_derivatives_title($derivative))),
    isset($_GET['destination']) ? $_GET['destination'] : 'admin/mapi/derivatives/'. $derivative->did,
    $notice,
    t('Delete'), t('Cancel'));

  return $form;
}

/**
 *  Submission for derivative_delete form
 */
function mapi_derivatives_delete_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  if ($values['op'] == t('Delete')) {
    mapi_derivative_delete($values['derivative']);
  }

  $form_state['redirect'] = 'admin/mapi/derivatives';
}

/**
 * Prints out the settings as a recursive function.
 */
function _mapi_derivatives_transform_print_settings($settings, $title = '') {
  drupal_add_css(drupal_get_path('module', 'mapi_derivatives') .'/mapi_derivatives.css');

  $output = '';
  if (is_array($settings)) {
    $output = '<ul class="mapi-derivatives-setting-array">';
    if ($title) {
      $output .= check_plain($title);
    }
    foreach ($settings as $key => $value) {
      if (is_array($value)) {
        $output .= _mapi_derivatives_transform_print_settings($value, $key);
      }
      else {
        $output .= '<li class="mapi-derivatives-setting">'. check_plain($key) .' = '. check_plain($value) .'</li>';
      }
    }
    $output .= '</ul>';
  }

  return $output;
}
