<?php

/**
 * @file
 * Contains the mapi_derivative hooks for mapi_admin, and supporting
 * form files.
 */

/**
 * Implementation of hook_mapi_admin()
 */
function mapi_derivatives_mapi_admin($op) {
  switch ($op) {
    case 'menu':
      $items['admin/mapi/derivatives'] = array(
        'title' => 'Derivatives',
        'description' => 'Manage automatically generated derivatives.',
        'page callback' => 'mapi_derivatives_admin_page',
        'page arguments' => array(),
        'file' => 'admin.derivatives.inc',
        'file path' => drupal_get_path('module', 'mapi_derivatives'),
        'access arguments' => array('administer mapi'),
      );

      $items['admin/mapi/derivatives/add'] = array(
        'title' => 'Add',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('mapi_derivatives_edit_form', null, null),
        'type' => MENU_CALLBACK,
        'file' => 'admin.derivatives.inc',
        'access arguments' => array('administer mapi'),
      );

      $items['admin/mapi/derivatives/%mapi_derivative'] = array(
        'title callback' => '_mapi_derivatives_title',
        'title arguments' => array(3),
        'page callback' => 'mapi_derivatives_derivative_page',
        'page arguments' => array(3),
        'file' => 'admin.derivatives.inc',
        'access arguments' => array('administer mapi'),
      );

      $items['admin/mapi/derivatives/%mapi_derivative/add'] = array(
        'title' => 'Add subderivative',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('mapi_derivatives_edit_form', null, 3),
        'type' => MENU_CALLBACK,
        'file' => 'admin.derivatives.inc',
        'access arguments' => array('administer mapi'),
      );

      $items['admin/mapi/derivatives/%mapi_derivative/edit'] = array(
        'title' => 'Edit',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('mapi_derivatives_edit_form', 3, null),
        'type' => MENU_LOCAL_TASK,
        'file' => 'admin.derivatives.inc',
        'access arguments' => array('administer mapi'),
      );

      $items['admin/mapi/derivatives/%mapi_derivative/delete'] = array(
        'title' => 'Delete',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('mapi_derivatives_delete_form', 3),
        'type' => MENU_CALLBACK,
        'file' => 'admin.derivatives.inc',
        'access arguments' => array('administer mapi'),
      );

      $items['admin/mapi/derivatives/%mapi_derivative/transforms'] = array(
        'title' => 'Transforms',
        'type' => MENU_DEFAULT_LOCAL_TASK,
      );

      $items['admin/mapi/derivatives/%mapi_derivative/transforms/add'] = array(
        'title' => 'Add',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('mapi_derivatives_transform_edit_form', 3, null),
        'type' => MENU_CALLBACK,
        'file' => 'admin.transforms.inc',
        'access arguments' => array('administer mapi'),
      );

      $items['admin/mapi/derivatives/%mapi_derivative/transforms/%mapi_transform'] = array(
        'title callback' => '_mapi_derivatives_transform_title',
        'title arguments' => array(5),
        'page callback' => 'drupal_get_form',
        'page arguments' => array('mapi_derivatives_transform_edit_form', 3, 5),
        'file' => 'admin.transforms.inc',
        'access arguments' => array('administer mapi'),
      );

      $items['admin/mapi/derivatives/%mapi_derivative/transforms/%mapi_transform/delete'] = array(
        'title' => 'Delete',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('mapi_derivatives_transform_delete_form', 3, 5),
        'type' => MENU_CALLBACK,
        'file' => 'admin.transforms.inc',
        'access arguments' => array('administer mapi'),
      );

      return $items;
  }
}

/**
 *  Helper functions for menu titles
 */
function _mapi_derivatives_title($derivative) {
  return $derivative->name .' - '. $derivative->ext;
}

function _mapi_derivatives_transform_title($transform) {
  return $transform->name;
}

/**
 *  Gets the base derivative breadcrumb
 */
function _mapi_derivatives_breadcrumb($did, $tid = null) {
  $list = array(
    l(t('Home'), NULL),
    l(t('Administer'), 'admin'),
    l(t('Media management'), 'admin/mapi'),
    l(t('Derivatives'), 'admin/mapi/derivatives'),
  );
  $derivatives = array();

  $derivative = mapi_derivative_load($did);
  while ($derivative->did) {
    $derivatives[] = l(_mapi_derivatives_title($derivative), 'admin/mapi/derivatives/'. $derivative->did);
    $derivative = mapi_derivative_load($derivative->parent);
  }
  array_shift($derivatives);
  $list = array_merge($list, array_reverse($derivatives));

  // handle the transforms also
  if (!is_null($tid)) {
    $derivative = mapi_derivative_load($did);
    $list[] = l(_mapi_derivatives_title($derivative), 'admin/mapi/derivatives/'. $derivative->did);
  }

  return $list;
}
