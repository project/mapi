<?php

/**
 * @file
 * Handles the Administrative forms for transforms.
 */

/**
 * Administrative form for Tranform Addition/Modification
 */
function mapi_derivatives_transform_edit_form(&$form_state, $derivative, $transform) {
  // set the correct breadcrumb
  $breadcrumbs = _mapi_derivatives_breadcrumb($derivative->did, $transform->tid ? $transform->tid : true);
  drupal_set_breadcrumb($breadcrumbs);

  // transform must either match the derivative, or be a new transform
  if ($transform->did != $derivative->did && !is_null($transform)) {
    drupal_not_found();
    exit();
  }

  $form['did'] = array('#type' => 'value', '#value' => $derivative->did);
  $form['tid'] = array('#type' => 'value', '#value' => $transform->tid);

  // adding a transform
  if (!$transform->name) {
    // get the list of transform for the derivative
    $transforms = mapi_transform_list_by_derivative($derivative->did);

    // get the extensions are this point of the transform flow
    $ext = mapi_derivative_transform_validate($derivative->ext, $transforms);

    // construct the validate transforms that apply for ALL exts
    $options = mapi_transform_list_by_extension($ext);

    // check and reply to no available options for adding.
    if (!count($options)) {
      return array('info' => array('#value' => t('There are no available transforms that can be added here.')));
    }

    // construct the weights
    $weight = 0;
    foreach ($transforms as $transform) {
      $weight = $transform->weight + 1;
    }

    $form['weight'] = array('#type' => 'value', '#value' => $weight);

    sort($options);

    // create the list as radio buttons
    foreach ($options as $key => $option) {
      $description = mapi_transform_info($option);
      $form['transforms'][$option] = array(
        '#type' => 'radio',
        '#title' => $option,
        '#parents' => array('name'),
        '#default_value' => ($key === 0 ? $option[0] : NULL),
        '#return_value' => $option,
        '#description' => $description,
      );
      if (!$key) {
        $form['transforms'][$option]['#default_value'] = $option;
      }
    }

    $form['submit'] = array('#type' => 'submit', '#value' => t('Submit'));
  }
  // editing a transform
  else {
    // this should contain all the details for the transform settings.
    $items = mapi_transform_configure($transform->name, $transform->settings);
    if (is_array($items)) {
      $form['settings'] = $items;
      $form['settings']['#tree'] = true;
      $form['submit'] = array('#type' => 'submit', '#value' => t('Submit'));
      if ($transform->tid) {
        $form['delete'] = array('#type' => 'submit', '#value' => t('Delete'));
      }
    }
  }

  return $form;
}

/**
 * Validation for mapi_derivatives_transform_edit_form.
 */
function mapi_derivatives_transform_edit_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  $tid = $values['tid'];

  // editing requires checks.
  if ($tid) {
    $derivative = mapi_derivative_load($values['did']);

    // get the list of transform for the derivative
    $transforms = mapi_transform_list_by_derivative($derivative->did);
    $transforms[$tid]->settings = $values['settings'];

    // get the extensions are this point of the transform flow
    $ext = mapi_derivative_transform_validate($derivative->ext, $transforms);
    if (!$ext) {
      form_set_error('', t('Current settings disrupt the transformation process.'));
      return;
    }

    $transform = mapi_transform_load($tid);
    mapi_transform_validate($transform->name, $values['settings']);

    // check if dependent derivatives are affected by the extension change.
    $results = db_query("SELECT ext FROM {mapi_derivatives} WHERE parent = %d", $derivative->did);
    while ($object = db_fetch_object($results)) {
      if ($object->ext != $ext) {
        form_set_error('', t('Current settings disrupt the dependent derivatives.'));
        return;
      }
    }
  }
}

/**
 * Submission for mapi_derivatives_transform_edit_form.
 */
function mapi_derivatives_transform_edit_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $did = $values['did'];
  $tid = $values['tid'];
  switch ($values['op']) {
    case t('Delete'):
      drupal_goto('admin/mapi/derivatives/'. $did .'/transforms/'. $tid .'/delete');
      break;
    default:
      // save changes to edited
      if ($tid) {
        $transform = mapi_transform_load($tid);
        $settings = $values['settings'];
        mapi_transform_submit($transform->name, $settings);
        mapi_transform_update($tid, $settings);
        mapi_derivative_flush($did);
        drupal_set_message(t('Transform has been updated.'));
      }
      // add new transform, get defaults
      elseif ($did) {
        $derivative = mapi_derivative_load($values['did']);
        $name = $values['name'];
        $settings = mapi_transform_defaults($name, $derivative->output);
        if ($transform = mapi_transform_create($did, $name, $settings)) {
          $form_state['redirect'] = 'admin/mapi/derivatives/'. $did .'/transforms/'. $transform->tid;
          $form_state['tid'] = $transform->tid;
          drupal_set_message(t('Transform has been created.'));
        }
      }
      break;
  }
}

/**
 * Administrative Form for Tranform Deletion.
 */
function mapi_derivatives_transform_delete_form(&$form_state, $derivative, $transform) {
  if ($transform->did != $derivative->did) {
    drupal_not_found();
    exit();
  }

  $breadcrumbs = _mapi_derivatives_breadcrumb($derivative->did, $transform->tid);
  drupal_set_breadcrumb($breadcrumbs);

  // check that deleting the transform
  $transforms = mapi_transform_list_by_derivative($derivative->did);
  unset($transforms[$transform->tid]);
  if (mapi_derivative_transform_validate($derivative->ext, $transforms)) {
    $form['did'] = array('#type' => 'value', '#value' => $derivative->did);
    $form['tid'] = array('#type' => 'value', '#value' => $transform->tid);

    // provide user interaction
    $notice = t('This action cannot be undone. It will force all similarly named transforms to be erased from the system.');

    $form = confirm_form($form,
      t('Are you sure you want to delete transform %transform?', array('%transform' => $derivative->name)),
      isset($_GET['destination']) ? $_GET['destination'] : 'admin/mapi/derivatives/'. $derivative->did,
      $notice,
      t('Delete'), t('Cancel'));
  }
  else {
    $form['notification'] = array('#value' => t('This transform cannot not be deleted without destroying the flow of transforms for the derivative.'));
  }

  return $form;
}

/**
 * Submission for mapi_derivatives_transform_delete_form.
 */
function mapi_derivatives_transform_delete_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  if ($values['op'] == t('Delete')) {
    mapi_transform_delete($values['tid']);
  }

  $form_state['redirect'] = 'admin/mapi/derivatives/'. $values['did'] .'/transforms';
}


