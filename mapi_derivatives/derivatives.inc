<?php

/**
 * @file
 * API functions for derivatives handling.
 */

/**
 * Loads a derivative item.
 */
function mapi_derivative_load($did, $refresh = false, $allow_string = false) {
  static $derivatives = array();

  // only allow names if allow_string is specified
  if (!is_numeric($did) && (!is_string($did) || !$allow_string)) {
    return FALSE;
  }

  // if derivative cached, or needs refreshing
  if (!array_key_exists($did, $derivatives) || $refresh) {
    // individual derivative
    if (is_numeric($did)) {
      $derivatives[$did] = db_fetch_object(db_query("SELECT * FROM {mapi_derivatives} WHERE did=%d", $did));
    }
    // by name
    else {
      $derivatives[$did] = array();
      $results = db_query("SELECT * FROM {mapi_derivatives} WHERE name = '%s' ORDER BY ext ASC", $did);
      while ($object = db_fetch_object($results)) {
        $derivatives[$did][$object->ext] = $object;
      }
    }
  }

  return $derivatives[$did];
}

/**
 * Clears all derivatives generated for a particular identifier.
 */
function mapi_derivative_flush($did, $mid = 0, $override = 0) {
  // destroys all generated media by the derivative
  if (!is_numeric($did)  || !($derivative = mapi_derivative_load($did))) {
    return;
  }

  // just get all the files of the derivative from generated and delete them.
  $results = db_query("SELECT dest FROM {mapi_generated} WHERE did = %d AND override = %d", $derivative->did, $override);
  while ($object = db_fetch_object($results)) {
    if (is_file($object->dest)) {
      @unlink($object->dest);
    }
  }

  db_query("DELETE FROM {mapi_generated} WHERE did = %d AND override = %d", $derivative->did, $override);
}

/**
 * Returns the derivatives which can apply for a given extension.
 */
function mapi_derivative_list_by_extension($ext) {
  $derivatives = array();
  $results = db_query("SELECT did, name FROM {mapi_derivatives} WHERE LOWER(ext) = '%s' ORDER BY name ASC", drupal_strtolower($ext));
  while ($obj = db_fetch_object($results)) {
    $derivatives[$obj->did] = $obj->name;
  }
  return $derivatives;
}

/**
 * Returns the derivatives which can apply for a given extension, based on output extension.
 */
function mapi_derivative_list_by_output($ext) {
  $derivatives = array();
  $results = db_query("SELECT did, name FROM {mapi_derivatives} WHERE LOWER(output) = '%s' ORDER BY name ASC", drupal_strtolower($ext));
  while ($obj = db_fetch_object($results)) {
    $derivatives[$obj->did] = $obj->name;
  }
  return $derivatives;
}

/**
 * Returns the extensions which a derivative handles.
 */
function mapi_extension_list_by_derivative($derivative) {
  static $cache = array();

  // make sure that the result is cached, this could be called numerous times.
  if (!array_key_exists($derivative, $cache)) {
    $derivatives = array();
    $results = db_query("SELECT did, ext FROM {mapi_derivatives} WHERE name = '%s' ORDER BY ext ASC", drupal_strtolower($derivative));
    while ($object = db_fetch_object($results)) {
      $derivatives[$object->ext] = $object->did;
    }

    $cache[$derivative] = $derivatives;
  }

  return $cache[$derivative];
}

/**
 * Returns the derivatives which can apply for a given type.
 */
function mapi_derivatives_by_type($type) {
  $derivatives = array();
  foreach (mapi_type_extensions($type) as $ext) {
    $derivatives = array_merge($derivatives, mapi_derivative_list_by_extension($ext));
  }
  return array_unique($derivatives);
}

/**
 * Gives the list of the child derivatives, for a specific derivative.
 */
function mapi_derivatives_children($did) {
  $derivatives = array();
  $results = db_query("SELECT did FROM {mapi_derivatives} WHERE parent = %d", $did);
  while ($obj = db_fetch_object($results)) {
    $derivatives[] = $obj->did;
  }
  return $derivatives;
}

/**
 * Creates a derivative.
 */
function mapi_derivative_create($name, $ext, $parent = 0) {
  if (!($derivative = mapi_derivative_load($parent)) && $parent != 0) {
    return FALSE;
  }

  // guarantee that the name is properly formatted before going into the system
  $name = preg_replace('/[^\w\d\-\_ ]+/', '-', $name);

  // must be an extension that is handled by the extension lists.
  if (!$ext || !in_array($ext, mapi_extension_list())) {
    return false;
  }

  // check quickly that another derivative of that name doesn't already exist
  $taken = mapi_extension_list_by_derivative($name);
  if (array_key_exists($ext, $taken)) {
    return false;
  }

  // values for the record about to be entered
  $values = array(
    'name' => $name,
    'parent' => $parent,
    'ext' => $ext,
    'output' => $ext,
    'override' => 0,
  );

  if (drupal_write_record('mapi_derivatives', $values)) {
    return mapi_derivative_load($values['did']);
  }

  return false;
}

/**
 * Updates a derivative name.
 */
function mapi_derivative_update($did, $name) {
  if (!($derivative = mapi_derivative_load($did))) {
    return false;
  }

  // guarantee that the name is properly formatted before going into the system
  $name = preg_replace('/[^\w\d\-\_ ]+/', '-', $name);

  // update the database
  db_query("UPDATE {mapi_derivatives} SET name = '%s' WHERE did = %d", $name, $derivative->did);

  return mapi_derivative_load($derivative->did, true);
}

/**
 * Deletes a derivative and it's children.
 */
function mapi_derivative_delete($did) {
  if (!($derivative = mapi_derivative_load($did))) {
    return;
  }

  // have to kill all the derivatives which rely on this one.
  $results = db_query("SELECT did FROM {mapi_derivatives} WHERE parent=%d", $did);
  while ($obj = db_fetch_object($results)) {
    mapi_derivative_delete($obj->did);
  }

  // remove all generated derivatives
  mapi_derivative_flush($did);

  // delete from database
  db_query("DELETE FROM {mapi_derivatives} WHERE did=%d", $did);
  db_query("DELETE FROM {mapi_transforms} WHERE did=%d", $did);

  // clear cache
  mapi_derivative_load($did, true);
}

/**
 * Gives the derivative name for a given derivative identifier.
 */
function mapi_derivative_name($did) {
  static $cache = array();
  if (!array_key_exists($did, $cache)) {
    $cache[$did] = db_result(db_query("SELECT name FROM {mapi_derivatives} WHERE did = %d", $did));
  }
  return $cache[$did];
}

/**
 * Creates the derivative for a media file, with possibility to force a new
 * generation.
 */
function mapi_derive($source, $name, $options = array()) {
  static $filenames = array();
  static $derivatives = array();

  // necessary options used throughout the function
  $exists = (isset($options['exists']) ? $options['exists'] : FILE_EXISTS_RENAME);
  $workspace = (isset($options['workspace']) ? $options['workspace'] : null);
  $create = (isset($options['directory create']) ? $options['directory create'] : false);
  $dest = (isset($options['filename']) ? $options['filename'] : null);

  // source file must exist
  if (!mapi_file_exists($source) || !is_string($name)) {
    return FALSE;
  }

  // refresh cache
  if (isset($options['refresh']) && $options['refresh']) {
    $filenames = array();
  }

  // check the cache for already completed derivatives, doing requested copy to that location
  if (array_key_exists($source, $filenames) && ($exists != FILE_EXIST_REPLACE)) {
    if (array_key_exists($name, $filenames[$source])) {
      return $filenames[$source][$name];
    }
  }

  // get the extension for the source
  $ext = mapi_file_ext($source);
  if (!array_key_exists($name, $derivatives)) {
    $derivatives[$name] = mapi_derivative_load($name, false, true);
  }

  // find the appropriate derivative in the list
  if (!array_key_exists($ext, $derivatives[$name])) {
    return false;
  }
  $derivative = $derivatives[$name][$ext];

  // check for possibly already generated versions, copying to location specified
  // ignoring if generated already
  $alternatives = mapi_generated_load($source);
  if (!empty($alternatives)) {
    // store all the alternatives for this filename in the filesnames section
    foreach ($alternatives as $key => $object) {
      $filenames[$source][$key] = $object->dest;
    }

    // check the indicator for this
    if (array_key_exists($derivative->did, $alternatives)) {
      if ($exists != FILE_EXISTS_REPLACE) {
        return $filenames[$source][$derivative->did];
      }
      // overwrite this location
      else {
        $dest = $alternatives[$derivative->did]->dest;
      }
    }
  }


  // make sure that destination is actually set
  if (!$dest) {
    // handle directory name if the file is local
    if (mapi_file_is_local($source)) {
      $base = dirname($source);
    }
    // otherwise specify some local for it.
    else {
      $base = mapi_workspace_path($workspace, 'public');
    }

    // make sure there is always a valid name that is free for a generated file.
    $count = 1;
    $dest = $base .'/'. mapi_file_name($source) .'-'. $derivative->did .'.'. $derivative->output;

    // make sure this is really unique consider the already generated files
    while (db_result(db_query("SELECT genid FROM {mapi_generated} WHERE dest = '%s' AND source != '%s'", $dest, $source))) {
      $dest = $base .'/'. mapi_file_name($source) .'-'. $derivative->did .'-'. $count++ .'.'. $derivative->output;
    }
  }

  // check for path existance for destination (create if necessary)
  if (!mapi_directory_check(dirname($dest), $create, false, $workspace)) {
    mapi_error('Directory (%dest) does not exist and could not be created.', array('%dest' => dirname($dest)));
    return FALSE;
  }

  // need to check if it already exists, if not don't worry about generating it
  // or we are forcing a replace of the filename
  if (!is_file($dest) || ($exists == FILE_EXISTS_REPLACE)) {
    // original filename
    $fname = $source;

    // first, make sure we generate all the parents which are required for this one
    if ($derivative->parent) {
      $d = mapi_derivative_load($derivative->parent);

      // we need to generate the appropriate parent media
      if (!($fname = mapi_derive($source, $d->name))) {
        return FALSE;
      }
    }

    // use a temporary variable for the transforming filename
    $temp = $fname;

    // process according to transformation listing
    $transforms = mapi_transform_list_by_derivative($derivative->did);
    foreach ($transforms as $tid => $transform) {
      // operating on this temporary file
      $filename = mapi_transform($temp, $transform->name, $transform->settings);

      // remove those files which have changed name, possibly extension
      if ($filename != $fname && $temp != $fname) {
        @unlink($temp);
      }
      // don't keep trying to process on transformation failure, including zero-sized files.
      $size = filesize($filename);
      if (!$filename || !$size) {
        // and delete the temporary file created
        if ($temp && $temp != $fname) {
          @unlink($temp);
        }
        return FALSE;
      }
      $temp = $filename;
    }

    // now copy out of tmp area to actual path area
    if (!($dest = mapi_file_move($temp, $dest, array('workspace' => $workspace, 'replace' => $exists)))) {
      mapi_error("Unable to create derivative filename [%filename].", array('%filename' => $dest));
      return FALSE;
    }

    // get the details for the newly generated filename
    $ext = mapi_file_ext($dest);
    $dimensions = mapi_extension_dimensions($dest);

    $values = array(
      'did' => $derivative->did,
      'source' => $source,
      'dest' => $dest,
      'dimensions' => $dimensions,
    );

    // if already exists,
    if (array_key_exists($derivative->did, $alternatives)) {
      // remove filename, it's a different one
      if ($alternatives[$derivative->did]->dest != $dest) {
        @unlink($alternatives[$derivative->did]->dest);
      }

      $values['genid'] =  $alternatives[$derivative->did]->genid;
    }

    drupal_write_record('mapi_generated', $values, ($values['genid'] ? array('genid') : array()));

    // reload the cache for this source.
    $alternatives = mapi_generated_load($source, true);
  }

  // save into cache
  $filenames[$source][$name] = $dest;

  // now return the modified filename
  return $dest;
}

/**
 * Determines whether a derivative name already applies to an extension.
 */
function mapi_derivative_unique($name, $ext) {
  $exts = mapi_derivative_load($name, false, true);
  return array_key_exists($ext, $exts);
}

/**
 * Returns a list of the derivatives available for an extension.
 */
function mapi_derivative_list() {
  static $cache;

  // fill cache if not already filled
  if (!isset($cache)) {
    $cache = array();
    $results = db_query("SELECT DISTINCT(name) FROM {mapi_derivatives} ORDER BY name ASC");
    while ($object = db_fetch_object($results)) {
      $cache[] = $object->name;
    }
  }

  return $cache;
}

/**
 * Gets list in order, generating the parentage for each derivative.
 */
function mapi_derivatives_list_by_parentage() {
  static $list;
  if (!isset($list)) {
    $list = array();
    $results = db_query("SELECT did, parent FROM {mapi_derivatives} ORDER BY parent ASC, ext ASC, name ASC");
    while ($object = db_fetch_object($results)) {
      $list[$object->did] = (isset($list[$object->parent]) ? ($list[$object->parent] . '/') : '') . $object->did;
    }
    asort($list);
  }
  return $list;
}

/**
 * Loads a transform.
 */
function mapi_transform_load($tid) {
  static $cache = array();

  if (!is_numeric($tid)) {
    return false;
  }

  // store in cache whatever the result
  if (!array_key_exists($tid, $cache)) {
    if ($object = db_fetch_object(db_query("SELECT * FROM {mapi_transforms} WHERE tid=%d", $tid))) {
      $object->settings = unserialize($object->settings);
    }
    $cache[$tid] = $object;
  }

  return $cache[$tid];
}

/**
 * Deletes a transform. Forces flushing of all derivatives.
 */
function mapi_transform_delete($tid) {
  // remove transform from database
  if (!($transform = mapi_transform_load($tid))) {
    return;
  }

  // remove all generated media which have the transform as part of it.
  mapi_transform_flush($transform->name);

  // remove from the transformation database
  db_query("DELETE FROM {mapi_transforms} WHERE tid = %d", $transform->tid);
}

/**
 * Loads a transform list for a given derivative.
 */
function mapi_transform_list_by_derivative($did, $refresh = false) {
  static $cache = array();

  if (!array_key_exists($did, $cache) || $refresh) {
    $cache[$did] = array();
    $results = db_query("SELECT tid FROM {mapi_transforms} WHERE did=%d ORDER BY weight ASC", $did);
    while ($object = db_fetch_object($results)) {
      $cache[$did][$object->tid] = mapi_transform_load($object->tid);
    }
  }

  return $cache[$did];
}

/**
 * Returns a list of the extensions which the transform allows, given it's settings.
 */
function mapi_transform_allowable($tid, $exts) {
  $new = array();
  $transform = mapi_transform_load($tid);
  foreach ($exts as $ext) {
    $value = mapi_transform_test($transform->name, 'test.'. $ext, $transform->settings);
    $new = array_merge($new, array($value));
  }
  $new = array_unique(array_filter($new));
  return $new;
}

/**
 * Create a transform.
 */
function mapi_transform_create($did, $name, $settings) {
  if (!($derivative = mapi_derivative_load($did)) || !in_array($name, mapi_transform_list()) || !is_array($settings)) {
    return FALSE;
  }

  $weight = 0;

  // get the highest weighted object within this transform
  $transforms = mapi_transform_list_by_derivative($derivative->did);
  foreach ($transforms as $transform) {
    $weight = $transform->weight + 1;
  }

  $values = array(
    'did' => $derivative->did,
    'name' => $name,
    'weight' => $weight,
    'settings' => $settings,
  );

  // check the validation of adding an extra transform
  if (!($ext = mapi_derivative_transform_validate($derivative->ext, array_merge($transforms, array($values))))) {
    return false;
  }

  if (drupal_write_record('mapi_transforms', $values)) {
    // update the output extension of the derivative
    db_query("UPDATE {mapi_derivatives} SET output = '%s' WHERE did = %d", $ext, $derivative->did);

    // refresh the cache
    mapi_transform_list_by_derivative($derivative->did, true);

    return mapi_transform_load($values['tid']);
  }

  return false;
}

/**
 * Updates a transform.
 */
function mapi_transform_update($tid, $settings) {
  if (!($transform = mapi_transform_load($tid)) || !is_array($settings)) {
    return FALSE;
  }
  db_query("UPDATE {mapi_transforms} SET settings='%s' WHERE tid = %d", serialize($settings), $transform->tid);

  // update the output extension of the derivative, since the change of settings for a transform may affect it.
  $derivative = mapi_derivative_load($transform->did);
  $transforms = mapi_transform_list_by_derivative($derivative->did);
  $ext = mapi_derivative_transform_validate($derivative->ext, $transforms);
  db_query("UPDATE {mapi_derivatives} SET output = '%s' WHERE did = %d", $ext, $derivative->did);

  $transform->settings = $settings;

  return $transform;
}

/**
 *  Loads the generated details from the database.
 */
function mapi_generated_load($source, $refresh = false) {
  static $cache = array();

  if (!array_key_exists($source, $cache) || ($refresh)) {
    $cache[$source] = array();
    $results = db_query("SELECT * FROM {mapi_generated} WHERE source = '%s'", $source);
    while ($object = db_fetch_object($results)) {
      $object->dimensions = unserialize($object->dimensions);
      $cache[$source][$object->did] = $object;
    }
  }

  return $cache[$source];
}

/**
 *  Returns the specific details for the generated files
 *  as an array.
 */
function mapi_generated_details($source, $did) {
  $details = mapi_generated_load($source);
  if (array_key_exists($did, $details)) {
    return array(
      'filename' => $details[$did]->dest,
      'dimensions' => $details[$did]->dimensions,
    );
  }

  return false;
}

/**
 * Override a generated file.
 */
function mapi_generated_override($source, $did, $filename) {
  if (!is_file($filename)) {
    return false;
  }

  $values = array(
    'did' => $did,
    'source' => $source,
    'dest' => $filename,
    'dimensions' => mapi_extension_dimensions($filename),
    'override' => 1,
  );
  $details = mapi_generated_load($source);
  if (array_key_exists($did, $details)) {
    // remove filename, it's a different one
    if ($details[$did]->dest != $filename) {
      @unlink($details[$did]->dest);
    }
    $values['genid'] = $details[$did]->genid;
  }
  drupal_write_record('mapi_generated', $values, ($values['genid'] ? array('genid') : array()));

  mapi_generated_load($source, true);

  return true;
}

/**
 *  Deletes a generated derivative
 */
function mapi_generated_delete($source, $did) {
  $details = mapi_generated_load($source, true);
  if (array_key_exists($did, $details)) {
    // remove the file if it exists
    if (is_file($details[$did]->dest)) {
      @unlink($details[$did]->dest);
    }
    db_query("DELETE FROM {mapi_generated} WHERE source='%s' AND did = %d", $source, $did);

    // refresh cache of generated
    mapi_generated_load($source, true);
  }
}

/**
 *  Deletes a generated derivative
 */
function mapi_generated_flush($source, $override = 0) {
  $details = mapi_generated_load($source);
  foreach ($details as $did => $object) {
    if (is_file($object->dest) && $object->override == $override) {
      @unlink($object->dest);
    }
  }
  db_query("DELETE FROM {mapi_generated} WHERE source='%s' AND override = %d", $source, $override);

  mapi_generated_load($source, true);
}

/**
 *  Validate the flow of transforms for a given set of extensions.
 */
function mapi_derivative_transform_validate($ext, $transforms) {
  if (!in_array($ext, mapi_extension_list())) {
    return false;
  }

  // goes through the entire list of transforms, with settings and checks what happens to the extension
  if (is_array($transforms)) {
    foreach ($transforms as $key => $transform) {
      $transform = (object)$transform;
      $value = mapi_transform_test($transform->name, 'test.'. $ext, $transform->settings);
      // error in transformation process
      if (!$value) {
        return false;
      }
      $ext = mapi_file_ext($value);
    }
  }

  return $ext;
}

/**
 *  Will move the generated derivative to location, either into the directory or as a file.
 *  Only will move the file if it is generated
 *
 *  Returns false.
 */
function mapi_generated_move($original, $new, $did, $dest, $workspace = null) {
  $details = mapi_generated_load($original);
  if (array_key_exists($name, $details)) {
    // remove the file if it exists
    if (is_file($details[$name]->dest)) {
      $filename = (is_dir($dest) ? rtrim($dest, '/') .'/'. mapi_file_basename($details[$name]->dest) : $dest);
      if ($temp = mapi_file_move($details[$name]->dest, $filename, array('replace' => FILE_EXISTS_RENAME, 'workspace' => $workspace))) {
        // update the database
        db_query("UPDATE {mapi_generated} SET dest='%s', source='%s' WHERE source='%s' AND did='%d'", $temp, $new, $original, $did);
        // refresh cache of old generated media
        mapi_generated_load($original, true);
      }
    }
  }
}

/**
 *  A more efficient way to move all the generated files for a source
 */
function mapi_generated_moveall($original, $new, $dest, $workspace = null) {
  if (is_dir($dest)) {
    $details = mapi_generated_load($original);
    foreach ($details as $did => $object) {
      $filename = rtrim($dest, '/') .'/'. mapi_file_basename($object->dest);
      if ($temp = mapi_file_move($object->dest, $filename, array('replace' => FILE_EXISTS_RENAME, 'workspace' => $workspace))) {
        // update the database
        db_query("UPDATE {mapi_generated} SET dest='%s', source='%s' WHERE source='%s' AND did='%d'", $temp, $new, $original, $did);
      }
    }
    // update the cache
    mapi_generated_load($original, true);
  }
}
