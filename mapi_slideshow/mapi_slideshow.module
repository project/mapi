<?php

/**
 *  Implementation of hook_theme().
 */
function mapi_slideshow_theme() {
  return array(
    'slideshow' => array('arguments' => array('reference' => '', 'list' => array(), 'current' => null, 'settings' => null))
  );
}

/**
 *  This allows for the generation of a slideshow.
 */
function mapi_slideshow_generate($reference, $items, $settings, $profile) {
  // safety checks on the incoming information
  $reference = 'slideshow-'. preg_replace('/[^\w\d\-]+/', '-', $reference);
  $items = (!is_array($items) ? array() : $items);
  $settings = (!is_array($settings) ? array() : $settings);

  // rebuilt item array to have simply key indexs
  $list = array();


  $options = array('settings' => $settings);
  if ($profile) {
    $options['profile'] = $profile;
  }

  $layout = module_exists('mapi_layouts') && isset($settings['layout']) && in_array($settings['layout'], mapi_layout_list()) ? $settings['layout'] : null;

  $height = 0;
  $width = 0;

  // switch filename for the display characteristics
  foreach ($items as $value) {
    $filename = isset($value['filename']) && is_string($value['filename']) ? $value['filename'] : '';
    $options['metadata'] = isset($value['metadata']) && is_array($value['metadata']) ? $value['metadata'] : array();

    if (mapi_file_exists($filename)) {
      $dims = mapi_extension_dimensions($filename);
      $height = $dims['height'] > $height ? $dims['height'] : $height;
      $width = $dims['width'] > $width ? $dims['width'] : $width;
      $list[] = mapi_display($filename, $options);
    }
  }

  $settings['width'] = $settings['width'] ? $settings['width'] : $width;
  $settings['height'] = $settings['height'] ? $settings['height'] : $height;

  // figure out the current reference in the slideshow (should always exist)
  $current = (isset($_GET[$reference]) && array_key_exists($_GET[$reference], $list)) ? $_GET[$reference] : 0;

  return count($list) ? theme('mapi_slideshow', $reference, $list, $current, $settings) : '';
}

/**
 * General theming of the various items.
 */
function theme_mapi_slideshow($reference, $items, $current, $settings) {
  $indices = array_keys($items);
  $total = count($items);

  $float = '';
  $style = 'width:'. $settings['width'] .'px;';
  if (isset($settings['float'])) {
    $float = ' mapi-slideshow-'. $settings['float'];
    $style .= 'float:'. $settings['float'] .';clear:both;';
  }

  $output = '<div class="mapi-slideshow'. $float .'" id="'. $reference .'" style="width:'. $settings['width'] .'px;">';
  foreach ($items as $key => $item) {
    $output .='<div id="'. $reference .'-'. $key .'" class="mapi-slideshow-content" style="display:'. ($key == $current ? 'block' : 'none') .'">';
    $output .= $item;
    $output .= '<div>';
  }

  // movement band
  $output .= '<div class="mapi-slideshow-prev">';
  $output .= l('<<', $_GET['q'], array('class' => 'mapi-slideshow-prev-link '. $reference), $reference .'='. ($current ? ($current-1) : ($total-1)));
  $output .= '</div>';
  $output .= '<div class="mapi-slideshow-next">';
  $output .= l('>>', $_GET['q'], array('class' => 'mapi-slideshow-next-link '. $reference), $reference .'='. ($current+1 < $total ? ($current+1) : 0));
  $output .= '</div>';
  $output .= '<div class="mapi-slideshow-listing">';
  $output .= t('<span class="mapi-slideshow-current">!current</span> of !amount', array('!current' => $current + 1, '!amount' => count($indices)));
  $output .= '</div>';

  $output .= '</div>';

  $path = drupal_get_path('module', 'mapi_slideshow');
  drupal_add_js($path .'/slideshow.js');
  drupal_add_css($path .'/slideshow.css');
  drupal_add_js(array('mapi_slideshow' => array($reference => array('current' => $current, 'total' => count($items)))), 'setting');

  return $output;
}
