/**
 */

$(document).ready(function() {
  if (!Drupal.settings.mapi_slideshow) return;
  $('.mapi-slideshow').each(function() {
    var container = $(this).find('.mapi-slideshow-content'),
        indicator = $(this).find('.mapi-slideshow-current'),
        slideshow = Drupal.settings.mapi_slideshow[this.id];

    if (!slideshow) return;

    $(this).
      find('.mapi-slideshow-prev-link').href('javascript:void();').
        click(function() {
          if (slideshow.current <= 0) {
            slideshow.current = slideshow.total;
          }
          container.html(slideshow.items[--slideshow.current]);
          if (indicator) {
            $(indicator).html(slideshow.current + 1);
          }
        }).end().
      find('.mapi-slideshow-next-link').href('javascript:void();').
        click(function() {
          if (slideshow.current + 1 >= slideshow.total) {
            slideshow.current = -1;
          }
          container.html(slideshow.items[++slideshow.current]);
          if (indicator) {
            $(indicator).html(slideshow.current + 1);
          }
        }).end();
  });
});