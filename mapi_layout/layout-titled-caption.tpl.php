<?php

$float = in_array($settings['float'], array('left', 'right', 'center')) ? $settings['float'] : '';
if ($float) {
  $style = ' style="min-height:'. $settings['height'] .'px;width:'. ($settings['width'] + 10) .'px"';
}

?>
<div class="layout layout-titled-caption layout-titled-caption-<?php print $float; ?>"<?php print $style ?>>
  <?php if ($metadata['title']): ?>
    <div class="title"><?php print $metadata['title']; ?></div>
  <?php endif; ?>
  <?php print $content; ?>
  <?php if ($metadata['caption']): ?>
    <div class="caption"><?php print $metadata['caption']; ?></div>
  <?php endif; ?>
</div>

