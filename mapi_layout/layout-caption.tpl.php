<?php

$float = in_array($settings['float'], array('left', 'right', 'center')) ? $settings['float'] : '';
if ($float) {
  $style = ' style="min-height:'. $settings['height'] .'px;width:'. ($settings['width'] + 10) .'px"';
}

?>
<span class="layout layout-caption layout-caption-<?php print $float; ?>"<?php print $style ?>>
  <?php print $content; ?>
  <?php if ($metadata['title']): ?>
    <div class="caption"><?php print $metadata['title']; ?></div>
  <?php endif; ?>
</span>

