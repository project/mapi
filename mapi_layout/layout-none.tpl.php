<?php

$float = in_array($settings['float'], array('left', 'right', 'center')) ? $settings['float'] : '';
if ($float) {
  $style = ' style="min-height:'. $settings['height'] .'px;width:'. ($settings['width'] + 10) .'px"';
}

?>
<?php if ($float): ?>
<span class="layout layout-none layout-none-<?php print $float; ?>"<?php print $style ?>>
<?php endif; ?>	
  <?php print $content; ?>
<?php if ($float): ?>
</span>
<?php endif; ?>
