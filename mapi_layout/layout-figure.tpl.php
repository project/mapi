<?php
static $figure = 1;

$float = in_array($settings['float'], array('left', 'right', 'center')) ? $settings['float'] : '';
if ($float) {
  $style = ' style="min-height:'. $settings['height'] .'px;width:'. ($settings['width'] + 10) .'px"';
}

?>
<span class="layout layout-figure layout-figure-<?php print $float; ?>"<?php print $style ?>>
  <div class="figure"><?php print t('Figure') .' '. $figure++; ?></div>
  <?php print $display; ?>
  <?php if ($metadata['title']): ?>
    <div class="caption"><?php print $metadata['title']; ?>
  <?php endif; ?>
</span>

