Layouts
-------

Layouts provide a way to visual represent an object with extra metadata, to be presented in the correct manner.
Since we need the final file that is being produced, as well as the metadata associated with it, it must go into
the mapi_display section.

The final filename, the final display and the metadata are all that can be passed to a layout. It has to determine
from there exactly it wants to produce the HTML. As such it involves theming functions.


