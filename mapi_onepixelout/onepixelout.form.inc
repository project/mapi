<?php

switch ($op) {
  case 'defaults':
    return array(
      'width'          => 250,
      'height'         => 20,
      'bg'             => '0xf8f8f8',
      'text'           => '0x666666',
      'leftbg'         => '0xeeeeee',
      'lefticon'       => '0x666666',
      'rightbg'        => '0xcccccc',
      'rightbghover'   => '0xcccccc',
      'righticon'      => '0x666666',
      'righticonhover' => '0x999999',
      'slider'         => '0x666666',
      'track'          => '0xffffff',
      'loader'         => '0x9fffb8',
      'border'         => '0x666666',
    );
  case 'configure':
    $img = drupal_get_path('module', 'mapi_onepixelout') .'/map.gif';
    if (is_file($img)) {
      $form['map'] = array('#value' => '<img src="'. $img .'" />');
    }

    // player size
    $form['width'] = array(
      '#type' => 'textfield',
      '#title' => t('Width'),
      '#default_value' => $settings['width'],
      '#description' => t('Player Width'),
    );
    $form['height'] = array(
      '#type' => 'textfield',
      '#title' => t('Height'),
      '#default_value' => $settings['height'],
      '#description' => t('Player Height'),
    );

    // configure the forms (allow for color changes, etc).
    $form['bg'] = array(
      '#type' => 'textfield',
      '#title' => t('Background Color'),
      '#default_value' => $settings['bg'],
    );

    $form['text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text Color'),
      '#default_value' => $settings['text'],
    );

    $form['leftbg'] = array(
      '#type' => 'textfield',
      '#title' => t('Left Background Color'),
      '#default_value' => $settings['leftbg'],
    );

    $form['lefticon'] = array(
      '#type' => 'textfield',
      '#title' => t('Left Icon Color'),
      '#default_value' => $settings['lefticon'],
    );

    $form['rightbg'] = array(
      '#type' => 'textfield',
      '#title' => t('Right Background Color'),
      '#default_value' => $settings['rightbg'],
    );

    $form['rightbghover'] = array(
      '#type' => 'textfield',
      '#title' => t('Right Background Hover Color'),
      '#default_value' => $settings['rightbghover'],
    );

    $form['righticon'] = array(
      '#type' => 'textfield',
      '#title' => t('Right Icon Color'),
      '#default_value' => $settings['righticon'],
    );

    $form['righticonhover'] = array(
      '#type' => 'textfield',
      '#title' => t('Right Icon Hover Color'),
      '#default_value' => $settings['righticonhover'],
    );

    $form['slider'] = array(
      '#type' => 'textfield',
      '#title' => t('Progress Slider Color'),
      '#default_value' => $settings['slider'],
    );

    $form['track'] = array(
      '#type' => 'textfield',
      '#title' => t('Progress Track Color'),
      '#default_value' => $settings['track'],
    );

    $form['loader'] = array(
      '#type' => 'textfield',
      '#title' => t('Loader Bar Color'),
      '#default_value' => $settings['loader'],
    );

    $form['border'] = array(
      '#type' => 'textfield',
      '#title' => t('Border Color'),
      '#default_value' => $settings['border'],
    );
    break;
  case 'validate':
    // validate the configuration form.
    foreach ($settings as $key => $value) {
      // everything here should be in the color format.
      if ($key != 'width' && $key != 'height') {
        if (!preg_match('/0x[\da-z]{1,6}/i', $value)) {
          form_set_error($key, t('!key needs to be a hexadecimal number in the format 0x??????', array('!key' => $key)));
        }
      }
      // except width and height
      else {
        if (!is_numeric($value) || $value <= 0) {
          form_set_error($key, t('!key need to be a positive number.', array('!key' => $key)));
        }
      }
    }
    break;
}
