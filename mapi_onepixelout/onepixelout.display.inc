<?php

$variables = array('bg', 'text', 'spacer', 'leftbg', 'lefticon', 'rightbg',
'rightbghover', 'righticon', 'righticonhover', 'track', 'loader', 'border');

$height = (!empty($settings['height']) ? $settings['height'] : 20);
$width  = (!empty($settings['width']) ? $settings['width'] : 250);

$filename = mapi_url($filename);
$flashvar = 'soundfile='. $filename;
foreach ($variables as $key) {
  if (isset($settings[$key])) {
    $flashvar .= "&{$key}=". $settings[$key];
  }
}

$general = array(
  'param' => array('movie' => $presenter, 'wmode' => 'opaque', 'flashvars' => $flashvar),
  'var' => array('type' => 'application/x-shockwave-flash'),
);
$microsoft = array(
  'param' => array('src' => $presenter, 'wmode' => 'opaque', 'flashvars' => $flashvar),
  'var' => array('classid' => 'clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'),
);

$output = mapi_render_object($presenter, $width, $height, $general, $microsoft);
