<?php

/**
 * Prepare HTML text for being used through the filtering process
 */
function mapi_filter_prepare_text($text) {
  $list = array();

  // check to replace images
  if (variable_get('mapi_filter_images', true)) {
    $list[] = '|<img[^>]+>|is';
  }
  // check to replace embeds
  if (variable_get('mapi_filter_embed', true)) {
    $list[] = '|<embed([^>]+)>(.*?)</embed>|is';
    $list[] = '|<embed([^>]+)/>|is';
  }

  // converts images and embeds
  if (count($list)) {
    $text = preg_replace_callback($list, '_mapi_filter_replace', $text);
  }

  if (variable_get('mapi_filter_objects', true)) {
    // deal with nested objects
    while (preg_match('|<object\b[^>]*>(.*?<object\b[^>]*>)+</object>|', $text)) {
      // gets the innermost objects and replaces that
      $text = preg_replace_callback('|<object\b[^>]*>(?!<object\b[^>]*>)(?:[\S\s](?!<object\b[^>]*>))*?</object>|i', '_mapi_filter_replace_object', $text);
    }
    // replace the details from what are effectively singlely left objects
    $text = preg_replace_callback('|<object\b[^>]*>.*?</object>|', '_mapi_filter_replace_object_final', $text);
  }

  return $text;
}

/**
 * Special functions for filtering out media related HTML tags, and converting them into something that
 * can be reprocessed back later, in a clean, XHTML safe way into sites, hopefully with less cross-scripting
 * involved.
 */
function _mapi_filter_replace($matches) {
  // all images or embed tags must have an attribute called "src"
  $params = _mapi_filter_construct_params($matches[0]);
  if (empty($params['src'])) {
    return '';
  }

  // make this into a special string url
  $src = $params['src'];
  unset($params['src']);

  $attrs = '';
  foreach ($params as $k => $v) {
    $attrs .= "{$k}=\"{$v}\";";
  }

  // return a standard filter tag
  return '[embed:uri="'. $src .'"'. ($attrs ? ';'. $attrs : '') .']';
}

function _mapi_filter_replace_object_final($matches) {
  return _mapi_filter_replace_object($matches, true);
}

function _mapi_filter_replace_object($matches, $finish = false) {
  // get the general attributes of the object
  preg_match('|<object\b(.*?)>|i', $matches[0], $params);
  $object = _mapi_filter_settings($params[1]);

  // determine the parameters of an object, as well as the ones specified as attributes.
  if (preg_match_all('|<param\b(.*?)>|i', $matches[0], $params, PREG_SET_ORDER)) {
    foreach ($params as $param) {
      $p = _mapi_filter_construct_params($param[1]);
      if (!empty($p['name']) && !empty($p['value'])) {
        $object[$p['name']] = $p['value'];
      }
    }
  }

  // only if this is the last object to handle in nested objects
  if ($finish) {
    // must have some kind of url specifier set
    $src = '';
    foreach (array('movie', 'src', 'url', 'data') as $type) {
      if (!empty($object[$type])) {
        $src = $object[$type];
      }
      unset($object[$type]);
    }
    if (!$src) {
      return '';
    }

    $attrs = '';
    foreach ($object as $k => $v) {
      $attrs .= "${k}=\"{$v}\";";
    }

    $output = "[embed:uri=\"". $src .'"'. ($attrs ? ";". $attrs : '') .']';
  }
  else {
    // simply construct the output as more parameters for the ultimate object
    $output = '';
    foreach ($object as $k => $v) {
      $output .= "<param name=\"{$k}\" value=\"{$v}\" />";
    }
  }

  return $output;
}

/**
 * Get tag attributes as per the HTML 4 specifications.
 */
function _mapi_filter_settings($details) {
  // matches definition of values according to the HTML4 settings
  preg_match_all("/(\w+)=((?:[a-zA-Z0-9_:.\-]+)|(?:\"(?:.*?|)\")|(?:'(?:.*?|)'))/", $details, $list, PREG_SET_ORDER);
  $attr = array();
  foreach ($list as $item) {
    // remove quotation marks
    if (substr($item[2], 0, 1) == '"' || substr($item[2], 0, 1) == "'") {
      $value = substr($item[2], 1, strlen($item[2])-2);
    }
    // value is standard unquoted
    else {
      $value = $item[2];
    }

    $attr[$item[1]] = check_plain($value);
  }
  return $attr;
}

function _mapi_filter_construct_params($details) {
  // matches definition of values according to the HTML4 settings
  preg_match_all("/(\w+)=((?:[a-zA-Z0-9_:.\-]+)|(?:\"(?:.*?|)\")|(?:'(?:.*?|)'))/", $details, $list, PREG_SET_ORDER);
  $attr = array();
  foreach ($list as $item) {
    // remove quotation marks
    if (substr($item[2], 0, 1) == '"' || substr($item[2], 0, 1) == "'") {
      $value = substr($item[2], 1, strlen($item[2])-2);
    }
    // value is standard unquoted
    else {
      $value = $item[2];
    }

    $attr[$item[1]] = check_plain($value);
  }
  return $attr;
}

/**
 * This provides a details array which can be referenced by the appropriate
 * function to generate the output.
 */
function _mapi_filter_build_details($list) {
  $details = array();

  $first = true;
  $last = false;
  $current = array();

  // cycle through the entire list of details
  while ($value = array_shift($list)) {
    if ($value == ':') {
      if (!$first) {
        // special index into the details
        if ($last) {
          $details[$last] = $current;
          $last = false;
        }
        // we have an index specified
        elseif (count($current) == 1 && !count(array_filter(array_keys($current)))&& !is_numeric($current[0])) {
          $last = $current[0];
        }
        // otherwise it's a simple list.
        else {
          $details[] = $current;
        }
      }
      $current = array();
    }
    // simply add the numeric value or special word to the current list
    elseif (is_numeric($value) || preg_match('/^[\w-]+$/', $value)) {
      $current[] = $value;
    }
    // it's an index of an array
    elseif (preg_match('/([\w-]+)="((?:\\\\"|[^"]|)+)"/', $value, $settings)) {
      $current[$settings[1]] = $settings[2];
    }
    elseif (preg_match('/^"((?:\\\\"|[^"]|)+)"$/', $value, $settings)) {
      $current[] = $settings[1];
    }
    $first = false;
  }

  // there will always be something to add at the end
  if ($last) {
    $details[$last] = $current;
  }
  else {
    $details[] = $current;
  }
  return $details;
}
