<?php

if (function_exists('mapi_derive')) {
  if (!empty($settings['normal.file']) && !empty($settings['hd.file'])) {
    switch ($settings['hd.file']) {
      case 'none': unset($settings['hd.file']); break;
      case 'original': $settings['hd.file'] = mapi_url($filename); break;
      default; $settings['hd.file'] = mapi_url(mapi_derive($filename, mapi_derivative_name($settings['hd.file']))); break;
    }
    $filename = ($settings['normal.file'] == 'original' ? $filename : mapi_derive($filename, mapi_derivative_name($settings['normal.file'])));
    if (isset($settings['hd.file'])) {
      $settings['plugins'][] = 'hd-1';
    }
    if (!mapi_file_exists($filename) && $settings['hd.file']) {
      $filename = $settings['hd.file'];
      unset($settings['hd.file']);
      unset($settings['plugins'][array_search('hd-1', $settings['plugins'])]);
    }
  }
  // checking the settings, figure out if there is a possible display_image to go with it.
  if (!empty($settings['display_image'])) {
    $settings['image'] = mapi_url(mapi_derive($filename, mapi_derivative_name($settings['display_image'])));
  }
}

if (empty($settings['width']) && empty($settings['height'])) {
  $dims = mapi_extension_dimensions($filename);
  $settings['width'] = $dims['width'];
  $settings['height'] = $dims['height'];
}
elseif (empty($settings['width'])) {
  $dims = mapi_extension_dimensions($filename);
  $settings['width'] = floor($dims['width'] / $dims['height'] * $settings['height']);
}
elseif (empty($settings['height'])) {
  $dims = mapi_extension_dimensions($filename);
  $settings['height'] = floor($dims['height'] / $dims['width'] * $settings['width']);
}

// allow for playbar
$width = $settings['width'];
$height = $settings['height'] + 20;

// the extra settings which are valid for the jw_player
$variables = array('playlistsize', 'playlist', 'width', 'height', 'stretching', 'controlbar', 'autostart', 'image', 'showdownload', 'link', 'plugins', 'hd.file');
if (!empty($settings['showdownload']) && $settings['showdownload'] == 'true' && empty($settings['link'])) {
  $settings['link'] = mapi_url($filename);
}

if (!empty($settings['plugins'])) {
  $settings['plugins'] = implode(',', $settings['plugins']);
}

$filename = mapi_url($filename);
$param = array('wmode' => 'opaque');
$var = array();
$flashvar = 'file='. $filename;
foreach ($variables as $key) {
  if (isset($settings[$key])) {
    $flashvar .= "&{$key}=". $settings[$key];
  }
}
$param['flashvars'] = $flashvar;
if (!empty($settings['fullscreen']) && $settings['fullscreen'] == 'true') {
  $param['allowfullscreen'] = 'true';
  $param['allowscriptaccess'] = 'always';
}

$general = array(
  'param' => $param,
  'var' => array('type' => 'application/x-shockwave-flash'),
);
$microsoft = array(
  'param' => array_merge($param, array('movie' => $presenter)),
  'var' => array('classid' => 'clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'),
);

$output = mapi_render_object($presenter, $width, $height, $general, $microsoft);

