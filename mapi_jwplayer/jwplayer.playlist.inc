<?php

// XSPF playlist
$items = array();

// handle the fact that a playlist requires an extended height/width
$set = is_array($settings) ? $settings : array();

$width = (!empty($set['height']) ? $set['height'] : 0);
$height = (!empty($set['height']) ? $set['height'] : 0);

foreach ($list as $values) {
  $profile = $values['details']['settings'];

  $filename = $values['filename'];
  $caption = $values['metadata']['title'];
  if ($profile['display_image'] && module_exists('mapi_derivatives')) {
    $image = mapi_derive($filename, mapi_derivative_name($profile['display_image']));
  }

  // check the filename, and the existance of the file
  if (!$filename || !in_array(mapi_file_ext($filename), mapi_presenter_extension_list('jw_player')) || !mapi_file_exists($filename)) {
    continue;
  }

  // get a height since nothing else is defined.
  if (!$height || !$width) {
    $dims = mapi_extension_dimensions($filename);
    $height = !$height ? $dims['height'] : $height;
    $width = !$width ? $dims['width'] : $width;
  }

  // generate the link the player will use
  $link = mapi_url($filename);

  $item = array(
    'key' => 'track',
    'value' => array(
      'title' => $caption,
      'location' => $link,
    ),
  );

  // add thumbnail image for playlist if it exists.
  if ($image && mapi_file_exists($image)) {
    $item['value']['image'] = mapi_url($image);
  }

  $items[] = $item;
}

$playlist[] = array(
  'key' => 'playlist',
  'attributes' => array('version' => 1, 'xmlns' => 'http://xspf.org/ns/0/'),
  'value' => array('trackList' => $items)
);

$output = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
$output .= format_xml_elements($playlist);

// check that height is set from the height given above
$set['height'] = isset($set['height']) ? $set['height'] : $height;
$set['width'] = isset($set['width']) ? $set['width'] : $width;

// make sure that displayheight is set
if (!isset($set['playlistsize'])) {
  $set['playlistsize'] = 65;
  $set['height'] = $height + 65;
}
// otherwise add the playlist height to the general height.
else {
  switch ($set['playlist']) {
    case 'bottom':  $set['height'] += $set['playlistsize']; break;
    case 'right': $set['width'] += $set['playlistsize']; break;
  }
}

// make sure to incorporate display bar in the height given
$set['height'] = $set['height'] + 20;

$headers = "Content-Type: application/xml; charset=utf-8\n\r";
$cache = $output;
$html = _mapi_jwplayer_display($details['playlist_url'] .'.xml', $set);
