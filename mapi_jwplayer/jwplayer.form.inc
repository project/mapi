<?php
switch ($op) {
  case 'defaults':
    drupal_set_message('ext: '. $ext);
    if (in_array($ext, array('flv', 'mp4'))) {
      return array(
        'height' => 300,
        'width' => 250,
        'playlistsize' => 100,
        'playlist' => 'none',
        'autostart' => 'false',
        'fullscreen' => 'false',
        'controlbar' => 'bottom',
        'stretching' => 'uniform',
        'display_image' => '',
        'playlist_image' => '',
      );
    }
    return array('width' => 250);

  case 'configure':
    $form['ext'] = array('#type' => 'value', '#value' => $ext);

    // configure the forms (allow for color changes, etc).
    $form['width'] = array(
      '#type' => 'textfield',
      '#title' => t('Width'),
      '#default_value' => $settings['width'],
      '#description' => t('Video display width'),
    );

    if (in_array($ext, array('flv', 'mp4'))) {
      $form['height'] = array(
        '#type' => 'textfield',
        '#title' => t('Height'),
        '#default_value' => $settings['height'],
        '#description' => t('Video display height'),
      );

      // playlistsize
      $form['playlistsize'] = array(
        '#type' => 'textfield',
        '#title' => t('Playlist Size'),
        '#default_value' => $settings['playlistsize'],
        '#description' => t('Size of playlist when attached. Automatically added to the above height.'),
      );

      $form['playlist'] = array(
        '#type' => 'select',
        '#title' => t('Playlist location'),
        '#options' => drupal_map_assoc(array('bottom', 'over', 'right', 'none')),
        '#default_value' => $settings['playlist'],
        '#description' => t('Defines where the playlist is located.'),
      );

      // define autostart capabilities.
      $form['autostart'] = array(
        '#type' => 'select',
        '#title' => t('Autostart'),
        '#options' => array('false' => 'false', 'true' => 'true'),
        '#default_value' => $settings['autostart'],
        '#description' => t('Automatically start the player on load.'),
      );

      $form['fullscreen'] = array(
        '#type' => 'select',
        '#title' => t('Fullscreen'),
        '#options' => array('false' => 'false', 'true' => 'true'),
        '#default_value' => $settings['fullscreen'],
        '#description' => t('Enable fullscreen capability.'),
      );

      $form['showdownload'] = array(
        '#type' => 'select',
        '#title' => t('Show Download'),
        '#options' => array('false' => 'false', 'true' => 'true'),
        '#default_value' => $settings['showdownload'],
        '#description' => t('Provides a download link for downloading videos.'),
      );

      $form['controlbar'] = array(
        '#type' => 'select',
        '#title' => t('Controlbar'),
        '#options' => drupal_map_assoc(array('bottom', 'over', 'right', 'none')),
        '#default_value' => $settings['controlbar'],
        '#description' => t('Location of the controlbar.'),
      );

      $form['stretching'] = array(
        '#type' => 'select',
        '#title' => t('Stretching'),
        '#options' => drupal_map_assoc(array('uniform', 'none', 'exactfit', 'fill')),
        '#default_value' => $settings['stretching'],
        '#description' => t('Resizing of the image to the display.'),
      );

      // display images
      if (module_exists('mapi_derivatives')) {
        // handle image types
        $list = array();
        // calculate the complete numerical union of all the possibilities
        foreach (mapi_type_extensions('image') as $extension) {
          foreach (mapi_derivative_list_by_output($extension) as $key => $value) {
            $list[$key] = $value;
          }
        }
        if (count($list)) {
          $list = array_intersect_key($list, mapi_derivative_list_by_extension($ext));
        }

        // only display this if there are some derivatives that can apply
        if (count($list)) {
          // make sure that there is a way for nothing to be displayed.
          $list[''] = t('none');

          $form['display_image'] = array(
            '#type' => 'select',
            '#title' => t('Display image.'),
            '#options' => $list,
            '#default_value' => $settings['display_image'],
            '#description' => t('The derivative image to be initially displayed'),
          );

          $form['playlist_image'] = array(
            '#type' => 'select',
            '#title' => t('Playlist image.'),
            '#options' => $list,
            '#default_value' => $settings['playlist_image'],
            '#description' => t('The derivative image to be initially displayed'),
          );
        }

        $list = array();
        // calculate the complete numerical union of all the possibilities
        foreach (mapi_type_extensions('video') as $extension) {
          foreach (mapi_derivative_list_by_output($extension) as $key => $value) {
            $list[$key] = $value;
          }
        }
        $list = array_intersect_key($list, mapi_derivative_list_by_extension($ext));
        if (count($list)) {
          $list = array('original' => t('Original')) + $list;
          $form['normal.file'] = array(
            '#type' => 'select',
            '#title' => t('Original File'),
            '#options' => $list,
            '#default_value' => $settings['normal.file'],
            '#description' => t('Derivative for play file.'),
          );
          $list = array('none' => t('None')) + $list;
          $form['hd.file'] = array(
            '#type' => 'select',
            '#title' => t('HD File'),
            '#options' => $list,
            '#default_value' => $settings['hd.file'],
            '#description' => t('Derivative for HD file.'),
          );
        }
      }
    }

    break;
  case 'validate':
    // validate the configuration form.
    if ($settings['width'] && (!is_numeric($settings['width']) || $settings['width'] < 0)) {
      form_set_error('width', t('Width should be positive integer.'));
    }
    if ($settings['height'] && (!is_numeric($settings['height']) || $settings['height'] < 0)) {
      form_set_error('height', t('Height should be positive integer.'));
    }
    if ($settings['displayheight'] && (!is_numeric($settings['displayheight']) || $settings['displayheight'] < 0)) {
      form_set_error('displayheight', t('DisplayHeight should be positive integer.'));
    }
    if ($settings['height'] && $settings['displayheight'] && $settings['height'] > $settings['displayheight']) {
      form_set_error('height', t('DisplayHeight should be greater than the Height.'));
    }
    break;
}
