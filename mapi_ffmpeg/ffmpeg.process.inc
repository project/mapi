<?php

// the program needs to be installed
if (!mapi_program_installed('ffmpeg')) {
  return;
}

// what's the output file
$ofile = mapi_file_temp($settings['output']);

// initial commandline
$cmdline = array('-y', '-i', realpath($filename));

switch ($transform) {
  case 'transcode':
    // additional conversion materials
    if ($settings['audio']['frequency']) {
      $cmdline[] = '-ar';
      $cmdline[] = $settings['audio']['frequency'];
    }
    break;

  case 'snapshot':
    // choose format for snapshot
    switch ($settings['output']) {
      case 'jpg':
        $cmdline[] = '-f';
        $cmdline[] = 'mjpeg';
        break;
      case 'gif':
        $cmdline[] = '-f';
        $cmdline[] = 'gif';
        $cmdline[] = '-pix_fmt';
        $cmdline[] = 'rgb24';
        break;
      case 'png':
        $cmdline[] = '-vcodec';
        $cmdline[] = 'png';
        $cmdline[] = '-f';
        $cmdline[] = 'rawvideo';
        break;
    }

    // add timing
    $cmdline[] = '-ss';
    $cmdline[] = $settings['timing'];

    // only one frame
    $cmdline[] = '-vframes';
    $cmdline[] = 1;

    // and no video recording
    $cmdline[] = '-an';
    break;
}

// output file
$cmdline[] = realpath($ofile);

// execute the program with the cmdline given
$result = mapi_program_execute('ffmpeg', $cmdline);

// make sure we return the actual file that we've created assuming it exists
$filename = $ofile;
