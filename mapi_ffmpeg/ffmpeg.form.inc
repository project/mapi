<?php

switch ($type) {
  // handle defaults
  case 'defaults':
    switch ($transform) {
      case 'transcode':
        $form = array(
          'output' => array_shift(mapi_program_extensions('ffmpeg')),
          'audio' => array(
            'frequency' => 0,
            'bitrate' => 0,
            'channels' => 0,
          ),
          'video' => array(
            'bitrate' => 0,
            'channels' => 0,
          )
        );
        break;
      case 'snapshot':
        $form = array(
          'output' => 'jpg',
          'timing' => 3
        );
        break;
    }
    break;

  // handle form
  case 'configure':
      $exts = mapi_program_extensions('ffmpeg');
      $extensions = array();
      foreach ($exts as $ext) {
        $extensions[$ext] = $ext;
      }

      if ($transform == 'transcode') {
        $form['output'] = array(
          '#type' => 'select',
          '#title' => t('Output extension'),
          '#description' => t('Select the output extension for transcoding.'),
          '#options' => $extensions,
          '#default_value' => !empty($settings['output']) ? $settings['output'] : $settings[0],
        );

        $form['audio'] = array(
          '#type' => 'fieldset',
          '#tree' => true,
          '#title' => t('Audio'),
          '#description' => t('Audio settings'),
        );

        $form['audio']['frequency'] = array(
          '#type' => 'select',
          '#title' => t('Frequency'),
          '#description' => t('Selects the frequency for the audio output.'),
          '#options' => array(t('No Change'), '11025' => '11.025 kHz', '22050' => '22.050 kHz', '44100' => '44.100 kHz'),
          '#default_value' => $settings['audio']['frequency'],
        );

        $form['audio']['bitrate'] = array(
          '#type' => 'select',
          '#title' => t('Bit Rate'),
          '#description' => t('Selects the bit rate for the audio output.'),
          '#options' => array(t('No Change'), '11k' => '11khz (FBR)', '22k' => '22khz (FBR)', '44k' => '44khz (FBR)'),
          '#default_value' => $settings['audio']['bitrate'],
        );

        $form['audio']['channels'] = array(
          '#type' => 'select',
          '#title' => t('Channels'),
          '#description' => t('Select the output channels for the audio.'),
          '#options' => array('' => t('No Change'), 'stereo' => t('stereo'), 'mono' => t('mono')),
          '#default_value' => $settings['audio']['channels'],
        );

        $form['video'] = array(
          '#type' => 'fieldset',
          '#tree' => true,
          '#title' => t('Video'),
          '#description' => t('Video settings'),
        );
      }
      elseif ($transform == 'snapshot') {
        $form['output'] = array(
          '#type' => 'select',
          '#title' => t('Output image extension'),
          '#description' => t('Select the output extension for transcoding'),
          '#options' => array('jpg' => 'jpg', 'png' => 'png', 'gif' => 'gif'),
          '#default_value' => $settings['output'],
        );

        $form['timing'] = array(
          '#type' => 'textfield',
          '#title' => t('Snapshot time'),
          '#description' => t('Determines in seconds when the snapshot is taken.'),
          '#default_value' => $settings['timing'],
        );
      }
    break;

  // handle validate
  case 'validate':
    if ($transform == 'snapshot' && (!is_numeric($settings['timing']) || $settings['timing'] < 0)) {
      form_set_error('timing', t('Timing must be in positive seconds.'));
    }
    break;
}